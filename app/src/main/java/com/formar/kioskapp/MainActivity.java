package com.formar.kioskapp;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.print.PrintManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.database.DBHandler;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.MySharePreference;
import com.formar.kioskapp.utils.VolleySingleton;
import com.formar.kioskapp.utils.VolleyUtils;
import com.formar.kioskapp.view.LoginActivity;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.sachinvarma.easypermission.EasyPermissionConstants;
import com.sachinvarma.easypermission.EasyPermissionInit;
import com.sachinvarma.easypermission.EasyPermissionList;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;
import static com.sachinvarma.easypermission.EasyPermissionConstants.PERMISSION_REQUEST_CODE;

public class MainActivity extends AppCompatActivity {
    public String demo;

    PlayerView videoFullScreenPlayer;
    ProgressBar progressBar;
    private String videoUrl="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4";
    SimpleExoPlayer player;
    DBHandler db;
    ProgressDialog progress;
    ProgressDialog pdialog;
    SQLiteDatabase dbs;
    String villageid = "134";
    String date = "2021-03-08";
    JSONObject userd;
    String fname;
    JSONObject userd2;
    JSONArray userd3;

    int PERMISSION_ALL = 1;
    private ArrayList<String> deniedPermissions;
    String[] PERMISSIONS = {

            Manifest.permission.READ_EXTERNAL_STORAGE,
            WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MANAGE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION

    };
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    ArrayList<String> ar = new ArrayList<String>();
    //ArrayList<WeatherListModel> weatherListModels=new ArrayList<>();
    //ArrayList<WeatherListModel> weatherListModels1=new ArrayList<>();
    ImageView master;
    Button loginTxt;
    ArrayList<String> UrlList=new ArrayList<>();
    int pos=0;
    boolean isVideoEnded=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.black));
        }
        master = findViewById(R.id.master);
        db = new DBHandler(this);
        dbs = db.getWritableDatabase();
        MySharePreference  mySharePreference = MySharePreference.getInstance(MainActivity.this);
        mySharePreference.setLanguage("en");

       // CheckPermissions();
        requestPermission();
        if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        }
        initView();
        getAdDetails();

         progress = new ProgressDialog(this);



        master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    try {


                        userd = new JSONObject();
                        userd.put("orgnId", "");
                        userd.put("locnId", "TA");
                        userd.put("userId", "");
                        userd.put("localeId", "TA");
                        userd.put("screen_Id", "");
                        userd.put("instance", "TA");

                        Log.e("OUTPUT", "" + userd.toString());

                    } catch (Exception e) {
                        Log.e("OUTPUT", "" + e.getMessage());
                    }


                    progress.setMessage("Master Synchronising.......");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.show();

                    //pdialog.setCanceledOnTouchOutside(false);
                    //pdialog.setTitle("Uploading Please Wait.......");
                    //pdialog.show();

                    //169.38.77.190:101/api/Mobile_FDR/Farmermaster
                    //15.206.21.248:27/Farmermaster
                    //  dbs.execSQL("delete from user_details");

                    JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, Constants.Url + "/api/OfflineAttachment/KioskList", userd,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("CCCC", "" + response);
                                    try {
                                        JSONObject obj = response.getJSONObject("context");
                                        //User Insert
                                       /* JSONArray cast = obj.getJSONArray("user");
                                        for (int i = 0; i < cast.length(); i++) {


                                            JSONObject actor = cast.getJSONObject(i);


                                            String n1 = actor.getString("user_name");
                                            String n2 = actor.getString("user_code");
                                            String n3 = actor.getString("user_pwd");
//                                            String n4 = actor.getString("out_master_description");
//                                            String n5 = actor.getString("out_depend_code");
//                                            String n6 = actor.getString("out_depend_desc");
//                                            String n7 = actor.getString("out_locallang_flag");
//                                            String n8 = actor.getString("out_status_code");




                                            db.onInsert(n1, n2, n3, "", "", "", "", "");


                                        }*/

                                        // User Insert
                                        JSONArray user = obj.getJSONArray("user");
                                        for (int i = 0; i < user.length(); i++) {


                                            JSONObject actor = user.getJSONObject(i);


                                            String n1 = actor.getString("user_rowid");
                                            String n2 = actor.getString("orgn_code");
                                            String n3 = actor.getString("user_code");
                                            String n4 = actor.getString("user_name");
                                            String n5 = actor.getString("status_code");
                                            String n6 = actor.getString("role_code");
                                            String n7 = actor.getString("user_pwd");
                                            String n8 = actor.getString("email_id");
                                            String n9 = actor.getString("contact_no");
                                            String n10 = actor.getString("photo_user");
                                            String n11 = actor.getString("valid_till");
                                            String n12 = actor.getString("secquestion_code");
                                            String n13 = actor.getString("secquestion_answer");
                                            String n14 = actor.getString("created_datetime");
                                            String n15 = actor.getString("password");
                                            String n16 = actor.getString("password_reset");

                                            db.onUserInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9,
                                                    n10, n11, n12, n13, n14, n15, n16);
                                        }
                                        Log.e("Insert", "User Insert Completed" + response);


                                        // Master Insert
                                        JSONArray master = obj.getJSONArray("masterheader");
                                        for (int i = 0; i < master.length(); i++) {


                                            JSONObject actor = master.getJSONObject(i);


                                            String n1 = actor.getString("master_rowid");
                                            String n2 = actor.getString("parent_code");
                                            // String n3 = actor.getString("parent_desc");
                                            String n4 = actor.getString("master_code");
                                            // String n5= actor.getString("master_desc");
                                            String n6 = actor.getString("depend_code");
                                            // String n7 = actor.getString("depend_desc");
                                            String n8 = actor.getString("locallang_flag");
                                            String n9 = actor.getString("master_row_slno");
                                            String n10 = actor.getString("status_code");

                                            db.onMasterInsert(n1, n2, n4, n6, n8, n9, n10);
                                        }
                                        Log.e("Insert", "Master Insert Completed" + response);

                                        // Sync Insert

                                        JSONArray syncmaster = obj.getJSONArray("synmas");
                                        for (int i = 0; i < syncmaster.length(); i++) {


                                            JSONObject actor = syncmaster.getJSONObject(i);


                                            String n1 = actor.getString("syncmaster_rowid");
                                            String n3 = actor.getString("village_id");
                                            String n4 = actor.getString("district_id");
                                            String n5= actor.getString("kiosk_village_gid");
                                            String n6 = actor.getString("kiosk_Master_type");
                                            String n7 = actor.getString("village_name");
                                            String n8 = actor.getString("district_name");
                                            String n9 = actor.getString("taluk_id");
                                            String n10 = actor.getString("taluk_name");
                                            String n11 = actor.getString("kiosk_taluk_gid");
                                            String n12 = actor.getString("kiosk_panchayat_gid");
                                            String n13 = actor.getString("source");
                                            String n14 = actor.getString("kiosk_district_gid");

                                            db.onSyncMasterInsert(n1,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14);
                                        }
                                        Log.e("Insert", "Sync Master Insert Completed" + response);

                                        //Master Data Insert


                                        // Faq Insert
                                        JSONArray faq = obj.getJSONArray("faq");
                                        for (int i = 0; i < faq.length(); i++) {


                                            JSONObject actor = faq.getJSONObject(i);


                                            String n1 = actor.getString("faq_gid");
                                            String n2 = actor.getString("faq_category");
                                            String n3 = actor.getString("faq_date");
                                            String n4 = actor.getString("faq_question");
                                            String n5 = actor.getString("faq_answer");
                                            String n6 = actor.getString("faq_keywords");
                                            String n7 = actor.getString("faq_ques_locallang");
                                            String n8 = actor.getString("faq_answer_locallang");
                                            String n9 = actor.getString("faq_keywords_locallang");
                                            String n10 = actor.getString("faq_ans_upload");
                                            String n11 = actor.getString("faq_url");
                                            String n12 = actor.getString("faq_video");
                                            String n13 = actor.getString("faq_videopath");

                                            //db.onFaqInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13);
                                            db.onFaqInsert(n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13);
                                        }
                                        Log.e("Insert", "FAQ Insert Completed" + response);


                                        // Land Insert
                                        JSONArray land = obj.getJSONArray("land");
                                        for (int i = 0; i < land.length(); i++) {


                                            JSONObject actor = land.getJSONObject(i);


                                            String n1 = actor.getString("land_gid");
                                            String n2 = actor.getString("farmer_gid");
                                            String n3 = actor.getString("land_type");
                                            String n4 = actor.getString("land_ownership");
                                            String n5 = actor.getString("land_soiltype");
                                            String n6 = actor.getString("land_irrsou");
                                            String n7 = actor.getString("land_noofacres");
                                            String n8 = actor.getString("land_longtitude");
                                            String n9 = actor.getString("land_latitude");
                                            String n10 = actor.getString("land_insertedby");
                                            String n11 = actor.getString("land_inserteddate");
                                            String n12 = actor.getString("land_modifyby");
                                            String n13 = actor.getString("land_modifydate");
                                            String n14 = actor.getString("land_isremoved");

                                            db.onLandInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14);

                                        }
                                        Log.e("Insert", "Land Insert Completed" + response);


                                        // Header Insert
                                        JSONArray header = obj.getJSONArray("kioskheader");
                                        for (int i = 0; i < header.length(); i++) {


                                            JSONObject actor = header.getJSONObject(i);


                                            String n1 = actor.getString("kiosk_gid");
                                            String n2 = actor.getString("kiosk_code");
                                            String n3 = actor.getString("kiosk_name");
                                            String n4 = actor.getString("kiosk_billingual");
                                            String n5 = actor.getString("kiosk_village");
                                            String n6 = actor.getString("kiosk_taluk");
                                            String n7 = actor.getString("kiosk_district");
                                            String n8 = actor.getString("kiosk_state");
                                            String n9 = actor.getString("kiosk_pincode");
                                            String n10 = actor.getString("kiosk_fpo");
                                            String n11 = actor.getString("kisok_address");
                                            String n12 = actor.getString("kiosk_notes");
                                            String n13 = actor.getString("kiosk_status");

                                            db.onHeaderInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13);

                                        }
                                        Log.e("Insert", "Header Insert Completed" + response);


                                        // Contact Insert
                                        JSONArray contact = obj.getJSONArray("contactdetail");
                                        for (int i = 0; i < contact.length(); i++) {


                                            JSONObject actor = contact.getJSONObject(i);


                                            String n1 = actor.getString("contact_gid");
                                            String n2 = actor.getString("contact_kiosk_gid");
                                            String n3 = actor.getString("contact_name");
                                            String n4 = actor.getString("contact_designation");
                                            String n5 = actor.getString("contact_email");
                                            String n6 = actor.getString("contact_mobile");
                                            String n7 = actor.getString("contact_landline");

                                            db.onContactInsert(n1, n2, n3, n4, n5, n6, n7);
                                        }
                                        Log.e("Insert", "Contact Insert Completed" + response);


                                        // Attchment Insert
                                        JSONArray attach = obj.getJSONArray("attachment");
                                        for (int i = 0; i < attach.length(); i++) {


                                            JSONObject actor = attach.getJSONObject(i);


                                            String n1 = actor.getString("attach_gid");
                                            String n2 = actor.getString("attach_ref_gid");
                                            String n3 = actor.getString("attach_docgroup_gid");
                                            String n4 = actor.getString("attach_docuname");
                                            String n5 = actor.getString("attach_descripition");
                                            String n6 = actor.getString("attach_filename");

                                            db.onAttachmentInsert(n1, n2, n3, n4, n5, n6);
                                        }
                                        Log.e("Insert", "Attach Insert Completed" + response);


                                        // Scheme Insert
                                        JSONArray scheme = obj.getJSONArray("schemes");
                                        for (int i = 0; i < scheme.length(); i++) {


                                            JSONObject actor = scheme.getJSONObject(i);


                                            String n1 = actor.getString("scheme_gid");
                                            String n2 = actor.getString("scheme_category");
                                            String n3 = actor.getString("scheme_date");
                                            String n4 = actor.getString("scheme_state");
                                            String n5 = actor.getString("scheme_schname");
                                            String n6 = actor.getString("scheme_description");
                                            String n7 = actor.getString("scheme_keyword");
                                            String n8 = actor.getString("schema_des_locallang");
                                            String n9 = actor.getString("scheme_keyword_locallang");
                                            String n10 = actor.getString("scheme_url");
                                            String n11 = actor.getString("scheme_upload");
                                            String n12 = actor.getString("scheme_status");
                                            String n13 = actor.getString("scheme_notes");
                                            String n14 = actor.getString("scheme_schname_locallang");

                                            db.onSchemeInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14);
                                        }
                                        Log.e("Insert", "Scheme Insert Completed" + response);


                                        // Soil Insert
                                        JSONArray soil = obj.getJSONArray("soil");
                                        for (int i = 0; i < soil.length(); i++) {


                                            JSONObject actor = soil.getJSONObject(i);


                                            String n1 = actor.getString("soil_gid");
                                            String n2 = actor.getString("soil_farmer_gid");
                                            String n3 = actor.getString("farmer_colldate");
                                            String n4 = actor.getString("farmer_tranid");
                                            String n5 = actor.getString("farmer_samplests");
                                            String n6 = actor.getString("farmer_sampleid");
                                            String n7 = actor.getString("farmer_sampledrawn");
                                            String n8 = actor.getString("farmer_customerref");
                                            String n9 = actor.getString("farmer_labreportno");
                                            String n10 = actor.getString("farmer_labid");
                                            String n11 = actor.getString("farmer_samplereceived");
                                            String n12 = actor.getString("farmer_analystarted");
                                            String n13 = actor.getString("farmer_analycompleted");
                                            String n14 = actor.getString("farmer_testmethod");
                                            String n15 = actor.getString("farmer_soil_rejreason");
                                            String n16 = actor.getString("farmer_soil_CropRecom");
                                            String n17 = actor.getString("famer_soil_notes");
                                            String n18 = actor.getString("farmer_soil_status");

                                            db.onSoilInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18);
                                        }
                                        Log.e("Insert", "Soil Insert Completed" + response);


                                        // Soil Param Insert
                                        JSONArray soilparam = obj.getJSONArray("soilparam");
                                        for (int i = 0; i < soilparam.length(); i++) {


                                            JSONObject actor = soilparam.getJSONObject(i);


                                            String n1 = actor.getString("soilparam_gid");
                                            String n2 = actor.getString("soilparam_soil_gid");
                                            String n3 = actor.getString("soil_parameter");
                                            String n4 = actor.getString("soil_uom");
                                            String n5 = actor.getString("soil_results");


                                            db.onSoilParamInsert(n1, n2, n3, n4, n5);
                                        }
                                        Log.e("Insert", "Soil Param Insert Completed" + response);


                                        // Video Insert
                                        JSONArray video = obj.getJSONArray("video");
                                        for (int i = 0; i < video.length(); i++) {


                                            JSONObject actor = video.getJSONObject(i);

                                            String n1 = actor.getString("video_gid");
                                            String n2 = actor.getString("video_category");
                                            String n3 = actor.getString("video_title");
                                            String n4 = actor.getString("video_filename");
                                            String n5 = actor.getString("video_filepath");
                                            String n6 = actor.getString("video_source");
                                            String n7 = actor.getString("video_keywords");
                                            String n8 = actor.getString("video_attribute");
                                            String n9 = actor.getString("video_notes");

                                            db.onVideoInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9);
                                        }
                                        Log.e("Insert", "Video Insert Completed" + response);


                                        // Master Trans Insert
                                        JSONArray mtrans = obj.getJSONArray("masterheaderdetails");
                                        for (int i = 0; i < mtrans.length(); i++) {


                                            JSONObject actor = mtrans.getJSONObject(i);


                                            String n1 = actor.getString("mastertranslate_rowid");
                                            String n2 = actor.getString("mastertranslate_master_code");
                                            String n3 = actor.getString("mastertranslate_parent_code");
                                            String n4 = actor.getString("mastertranslate_lang_code");
                                            String n5 = actor.getString("master_name");

                                            db.onMasTransInsert(n1, n2, n3, n4, n5);
                                        }
                                        Log.e("Insert", "Master Trans Insert Completed" + response);


                                        // Loc Insert
                                        JSONArray loc = obj.getJSONArray("localize");
                                        for (int i = 0; i < loc.length(); i++) {


                                            JSONObject actor = loc.getJSONObject(i);


                                            String n1 = actor.getString("localizationtranslate_rowid");
                                            String n2 = actor.getString("localizationtranslate_activity_code");
                                            String n3 = actor.getString("control_code");
                                            String n4 = actor.getString("data_field");
                                            String n5 = actor.getString("lang_code");
                                            String n6 = actor.getString("control_value");
                                            String n7 = actor.getString("loc_insertedby");
                                            String n8 = actor.getString("loc_inserteddate");
                                            String n9 = actor.getString("loc_modifyby");
                                            String n10 = actor.getString("loc_modifydate");
                                            String n11 = actor.getString("loc_isremoved");

                                            db.onLocInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11);
                                        }
                                        Log.e("Insert", "Loc Insert Completed" + response);


                                        // Farmer Insert
                                        JSONArray farmer = obj.getJSONArray("farmer");
                                        for (int i = 0; i < farmer.length(); i++) {


                                            JSONObject actor = farmer.getJSONObject(i);


                                            String n1 = actor.getString("farmer_rowid");
                                            String n2 = actor.getString("farmer_code");
                                            String n3 = actor.getString("version_no");
                                            String n4 = actor.getString("farmer_name");
                                            String n5 = actor.getString("farmer_dob");
                                            String n6 = actor.getString("farmer_addr1");
                                            String n7 = actor.getString("farmer_addr2");
                                            String n8 = actor.getString("farmer_ll_name");
                                            String n9 = actor.getString("sur_ll_name");
                                            String n10 = actor.getString("fhw_ll_name");
                                            String n11 = actor.getString("farmer_ll_addr1");
                                            String n12 = actor.getString("farmer_ll_addr2");
                                            String n13 = actor.getString("farmer_country");
                                            String n14 = actor.getString("farmer_state");
                                            String n15 = actor.getString("farmer_dist");
                                            String n16 = actor.getString("farmer_taluk");
                                            String n17 = actor.getString("farmer_panchayat");
                                            String n18 = actor.getString("farmer_village");
                                            String n19 = actor.getString("farmer_pincode");
                                            String n20 = actor.getString("marital_status");
                                            String n21 = actor.getString("gender_flag");
                                            String n22 = actor.getString("photo_farmer");
                                            String n23 = actor.getString("reg_note");
                                            String n24 = actor.getString("sur_name");
                                            String n25 = actor.getString("fhw_name");
                                            String n26 = actor.getString("status_code");

                                            db.onFarmerInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18,
                                                    n19, n20, n21, n22, n23, n24, n25, n26);
                                        }
                                        Log.e("Insert", "Farmer Insert Completed" + response);

                                        JSONArray irr = obj.getJSONArray("irrigationheader");
                                        for (int i = 0; i < irr.length(); i++) {


                                            JSONObject actor = irr.getJSONObject(i);


                                            String n1 = actor.getString("irrigation_gid");
                                            String n2 = actor.getString("farmer_gid");
                                            String n3 = actor.getString("farmer_colldate");
                                            String n4 = actor.getString("farmer_tranid");
                                            String n5 = actor.getString("farmer_samplests");
                                            String n6 = actor.getString("farmer_sampleid");
                                            String n7 = actor.getString("farmer_sampledrawn");
                                            String n8 = actor.getString("farmer_customerref");
                                            String n9 = actor.getString("farmer_labreportno");
                                            String n10 = actor.getString("farmer_labid");
                                            String n11 = actor.getString("farmer_samplereceived");
                                            String n12 = actor.getString("farmer_analystarted");
                                            String n13 = actor.getString("farmer_analycompleted");
                                            String n14 = actor.getString("farmer_testmethod");
                                            String n15 = actor.getString("farmer_irrigation_rejreason");
                                            String n16 = actor.getString("farmer_irrigation_CropRecom");
                                            String n17 = actor.getString("famer_irrigation_notes");
                                            String n18 = actor.getString("farmer_irrigation_status");
                                            String n19 = actor.getString("farmer_crop_confirm");
                                            String n20 = actor.getString("farmer_irrigation_confirm");

                                            db.iListinsert(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18,n19,n20);
                                        }

                                        JSONArray irrp = obj.getJSONArray("irrigationparameters");
                                        Log.e("irplength", ""+irrp.length());
                                        for (int i = 0; i < irrp.length(); i++) {


                                            JSONObject actor = irrp.getJSONObject(i);


                                            String n1 = actor.getString("irrigationparam_gid");
                                            String n2 = actor.getString("irrigation_gid");
                                            String n3 = actor.getString("irrigation_parameter");
                                            String n4 = actor.getString("irrigation_uom");
                                            String n5 = actor.getString("irrigation_results");


                                        //db.iParInsert("162", "37", "1263", "1088", "11.8");
                                        //db.iParInsert("179", "42", "1263", "1088", "1.1");
                                            db.iParInsert(n1,n2,n3,n4,n5);
                                         }


                                        //  pdialog.dismiss();
                                        progress.dismiss();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("CCCC", "" + error);

                                    //on error storing the name to sqlite with status unsynced
                                    // Toast.makeText(Demo.this, "Farmer "+n+" SuccessFull Added to Sync List" , Toast.LENGTH_SHORT).show();

                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);



                    try {


                        userd2 = new JSONObject();
                        userd2.put("token", "befd6907fad984fae880395825efe5d0");
                        Log.e("OUTPUT", "" + userd2.toString());

                    } catch (Exception e) {
                        Log.e("OUTPUT", "" + e.getMessage());
                    }

                    JsonObjectRequest stringRequest2 = new JsonObjectRequest(Request.Method.GET, Constants.Url1 + "/Api/v1/village/list", userd2,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("CCCC", "" + response);
                                    try {
                                        //   JSONObject obj = response.getJSONObject("");

                                        // User Insert
                                        JSONArray user = response.getJSONArray("data");
                                        for (int i = 0; i < user.length(); i++) {


                                            JSONObject actor = user.getJSONObject(i);


                                            String n1 = actor.getString("id");
                                            String n2 = actor.getString("block_id");
                                            String n3 = actor.getString("block_name");
                                            String n4 = actor.getString("district_id");
                                            String n5 = actor.getString("district_name");
                                            String n6 = actor.getString("state_id");
                                            String n7 = actor.getString("state_name");
                                            String n8 = actor.getString("village");
                                            String n9 = actor.getString("createdby");
                                            String n10 = actor.getString("modifiedby");
                                            String n11 = actor.getString("createdon");
                                            String n12 = actor.getString("modifiedon");

                                            db.onVillageListInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9,
                                                    n10, n11, n12);
                                        }
                                        Log.e("Insert", "Village List Insert Completed" + response);




                                        //  pdialog.dismiss();
                                        //progress.dismiss();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("CCCC", "" + error);

                                    //on error storing the name to sqlite with status unsynced
                                    // Toast.makeText(Demo.this, "Farmer "+n+" SuccessFull Added to Sync List" , Toast.LENGTH_SHORT).show();

                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();

                            return params;
                        }
                    };
                    stringRequest2.setRetryPolicy(new DefaultRetryPolicy(
                            1500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest2);
                    // Past Weather
                    JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, Constants.Url1+"/Api/v1/external/forecast?village_id=134&date=2021-03-08", null,
                            new Response.Listener<JSONArray>()
                            {
                                @Override
                                public void onResponse(JSONArray response) {
                                    // display response
                                    //pdialog.dismiss();
                                    Log.d("getWeatherDetailsaa", response.toString());
                                    if(response.length()>0){
                                        // weatherListModels.clear();
                                        for (int k=0;k<response.length();k++){
                                            try {
                                                JSONObject object=response.getJSONObject(k);
                                                //weatherListModels.add(new WeatherListModel(object.getString("date"),object.getString("village_id"),object.getDouble("tmax"),object.getDouble("tmin"),
                                                // object.getInt("rh1"),object.getInt("rh2"),object.getDouble("rh_avg"),object.getInt("ws1"),object.getInt("ws2"),object.getDouble("ws_avg"),object.getDouble("rf")));
                                                String n1 = object.getString("date");
                                                String n2 = object.getString("village_id");
                                                String n3 = object.getString("tmax");
                                                String n4 = object.getString("tmin");
                                                String n5 = object.getString("rh1");
                                                String n6 = object.getString("rh2");
                                                String n7 = object.getString("rh_avg");
                                                String n8 = object.getString("ws1");
                                                String n9 = object.getString("ws2");
                                                String n10 = object.getString("ws_avg");
                                                String n11 = object.getString("rf");
                                                db.onWeatherListInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9,
                                                        n10, n11);



                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }Log.e("Insert", "Past Weather List Insert Completed" + response);
                                        //Log.e("weatherListModels",weatherListModels.size()+"");
                                        // adapter.notifyDataSetChanged();


                                    }else{
                                        //getWeatherDetails(wType,"134","2021-03-29");
                                        //  Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Error.Response", error.toString());
                                    //pdialog.dismiss();
                                }
                            }
                    );
                    getRequest.setRetryPolicy(new DefaultRetryPolicy(
                            2500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(getRequest);

                    // Past Weather Ends

                    //Forecast Weather
                    JsonArrayRequest getRequest1 = new JsonArrayRequest(Request.Method.GET, Constants.Url1+"/Api/v1/external/past?village_id=134&date=2021-03-08", null,
                            new Response.Listener<JSONArray>()
                            {
                                @Override
                                public void onResponse(JSONArray response) {
                                    // display response
                                    //pdialog.dismiss();
                                    Log.d("getWeatherDetailsaa", response.toString());
                                    if(response.length()>0){
                                        // weatherListModels1.clear();
                                        for (int k=0;k<response.length();k++){
                                            try {
                                                JSONObject object=response.getJSONObject(k);
                                                //weatherListModels.add(new WeatherListModel(object.getString("date"),object.getString("village_id"),object.getDouble("tmax"),object.getDouble("tmin"),
                                                // object.getInt("rh1"),object.getInt("rh2"),object.getDouble("rh_avg"),object.getInt("ws1"),object.getInt("ws2"),object.getDouble("ws_avg"),object.getDouble("rf")));
                                                String n1 = object.getString("date");
                                                String n2 = object.getString("village_id");
                                                String n3 = object.getString("tmax");
                                                String n4 = object.getString("tmin");
                                                String n5 = object.getString("rh1");
                                                String n6 = object.getString("rh2");
                                                String n7 = object.getString("rh_avg");
                                                String n8 = object.getString("ws1");
                                                String n9 = object.getString("ws2");
                                                String n10 = object.getString("ws_avg");
                                                String n11 = object.getString("rf");
                                                db.onForecastWeatherListInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9,
                                                        n10, n11);



                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }Log.e("Insert", "Forecast Weather List Insert Completed" + response);
                                        //Log.e("weatherListModels",weatherListModels.size()+"");
                                        // adapter.notifyDataSetChanged();


                                    }else{
                                        //getWeatherDetails(wType,"134","2021-03-29");
                                        //  Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Error.Response", error.toString());
                                    //pdialog.dismiss();
                                }
                            }
                    );
                    getRequest1.setRetryPolicy(new DefaultRetryPolicy(
                            2500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(getRequest1);

                    // Village Details

                    JsonObjectRequest stringRequest3 = new JsonObjectRequest(Request.Method.GET, Constants.Url + "/api/Web_MasterDefinition/mastercode_screenid?org=test&locn=TA&user=admin&lang=en_US&screen_code=KioskSetup", null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("CCCC", "" + response);
                                    try {
                                        //   JSONObject obj = response.getJSONObject("");

                                        // User Insert
                                        JSONArray user = response.getJSONArray("detail");
                                        for (int i = 0; i < user.length(); i++) {


                                            JSONObject actor = user.getJSONObject(i);


                                            String n1 = actor.getString("out_master_rowid");
                                            String n2 = actor.getString("out_row_slno");
                                            String n3 = actor.getString("out_parent_code");
                                            String n4 = actor.getString("out_parent_description");
                                            String n5 = actor.getString("out_master_code");
                                            String n6 = actor.getString("out_master_description");
                                            String n7 = actor.getString("out_master_ll_description");
                                            String n8 = actor.getString("out_depend_code");
                                            String n9 = actor.getString("out_depend_desc");
                                            String n10 = actor.getString("out_locallang_flag");
                                            String n11 = actor.getString("out_status_code");
                                            String n12 = actor.getString("out_status_desc");
                                            String n13 = actor.getString("out_row_timestamp");
                                            String n14 = actor.getString("out_mode_flag");

                                            db.onVillageDetailsInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9,
                                                    n10, n11, n12, n13, n14);
                                        }
                                        Log.e("Insert", "Village Details Insert Completed" + response);




                                        //  pdialog.dismiss();
                                        //progress.dismiss();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("CCCC", "" + error);

                                    //on error storing the name to sqlite with status unsynced
                                    // Toast.makeText(Demo.this, "Farmer "+n+" SuccessFull Added to Sync List" , Toast.LENGTH_SHORT).show();

                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();

                            return params;
                        }
                    };
                    stringRequest3.setRetryPolicy(new DefaultRetryPolicy(
                            1500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest3);

                    //Crop Advisory
                    String Url3 = "";
                    Url3 = "https://nafcca.co.in/Api/v1/external/crop_advisory?district_id=49&village_id=133&period_from=21-03-2021&period_to=26-03-2021";
                    JsonArrayRequest getRequest2 = new JsonArrayRequest(Request.Method.GET,Url3,null,
                            new Response.Listener<JSONArray>()
                            {
                                @Override
                                public void onResponse(JSONArray response) {
                                    // display response
                                    //pdialog.dismiss();
                                    Log.d("getWeatherDetailsaa1", response.toString());
                                    if(response.length()>0){
                                        // weatherListModels1.clear();
                                        for (int k=0;k<response.length();k++){
                                            try {
                                                JSONObject object=response.getJSONObject(k);
                                                //weatherListModels.add(new WeatherListModel(object.getString("date"),object.getString("village_id"),object.getDouble("tmax"),object.getDouble("tmin"),
                                                // object.getInt("rh1"),object.getInt("rh2"),object.getDouble("rh_avg"),object.getInt("ws1"),object.getInt("ws2"),object.getDouble("ws_avg"),object.getDouble("rf")));
                                                String n1 = object.getString("id");
                                                String n2 = object.getString("crop_reg_id");
                                                String n3 = object.getString("userid");
                                                String n4 = object.getString("mobileno");
                                                String n5 = object.getString("cropid");
                                                String n6 = object.getString("crop_name");
                                                String n7 = object.getString("village_id");
                                                String n8 = object.getString("message_ta");
                                                String n9 = object.getString("message_en");
                                                String n10 = object.getString("status");
                                                String n11 = object.getString("name");
                                                String n12 = object.getString("village_name");
                                                String n13 = object.getString("block_id");
                                                String n14 = object.getString("block");
                                                String n15 = object.getString("district_id");
                                                String n16 = object.getString("district");
                                                String n17 = object.getString("state_id");
                                                String n18 = object.getString("state");
                                                String n19 = object.getString("createdon");
                                                db.onCropAdvInsert(n1, n2, n3, n4, n5, n6, n7, n8, n9,
                                                        n10, n11, n12, n13, n14, n15, n16, n17, n18, n19);



                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }Log.e("Insert", "Crop Adv List Insert Completed" + response);
                                        //Log.e("weatherListModels",weatherListModels.size()+"");
                                        // adapter.notifyDataSetChanged();


                                    }else{
                                        //getWeatherDetails(wType,"134","2021-03-29");
                                        //  Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Error.Response", error.toString());
                                    //pdialog.dismiss();
                                }
                            }
                    )
                    {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("token", "befd6907fad984fae880395825efe5d0");
                            return params;
                        }
                    };
                    getRequest2.setRetryPolicy(new DefaultRetryPolicy(
                            2500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(getRequest2);

                    String url= Constants.Url+"/api/Mobile_Kiosk_Fileurlpath/Kioskurlpath?Instance=TA";
                    VolleyUtils.getVolleyResult(MainActivity.this, Request.Method.GET, url, false,null, false, new VolleyListener() {
                        @Override
                        public void onError(String message) {
                        }
                        @Override
                        public void onResponse(JSONObject response) {

                            String[] ur;
                            Log.e("getDe",response+"--");
                            try {
                                JSONArray array = response.getJSONArray("trainingvideos");
                                if(array.length()>0) {
                                    for (int k=0;k< array.length();k++) {
                                        JSONObject object=array.getJSONObject(k);
                                        Log.e("getDe",object.getString("video_filepath")+"--");

                                        ar.add(object.getString("video_filepath"));
                                        /// new DownloadFileFromURL().execute(object.getString("video_filepath"),object.getString("video_filename"));
                                    }


                                }

                                JSONArray array2 = response.getJSONArray("advertismentvideos");
                                if(array2.length()>0) {
                                    for (int k=0;k< array2.length();k++) {
                                        JSONObject object=array2.getJSONObject(k);
                                        ar.add(object.getString("ad_path"));
                                        //  new DownloadFileFromURL().execute(object.getString("ad_path"),object.getString("ad_name"));
                                    }


                                }

                                JSONArray array3 = response.getJSONArray("faqfilepaths");
                                if(array3.length()>0) {
                                    for (int k=0;k< array3.length();k++) {
                                        JSONObject object=array3.getJSONObject(k);

                                        if(object.getString("faq_ans_upload").equalsIgnoreCase(""))
                                        {

                                        }
                                        else {

                                            // String name = FilenameUtils.getName(object.getString("faq_ans_upload"));
                                            ar.add(object.getString("faq_ans_upload"));
                                        }
                                        // new DownloadFileFromURL().execute(object.getString("faq_ans_upload"),name);
                                    }


                                }

                                JSONArray array4 = response.getJSONArray("governmentschemes");
                                if(array4.length()>0) {
                                    for (int k=0;k< array4.length();k++) {
                                        JSONObject object=array4.getJSONObject(k);
                                        if(object.getString("scheme_upload").equalsIgnoreCase(""))
                                        {

                                        }
                                        else {
                                            // String name = FilenameUtils.getName(object.getString("scheme_upload"));
                                            ar.add(object.getString("scheme_upload"));
                                        }
                                        //new DownloadFileFromURL().execute(object.getString("scheme_upload"),name);
                                    }


                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            String [] stringArray = ar.toArray(new String[ar.size()]);
                            //  Log.e("CCC",""ar.size());
                            new DownloadFileFromURL().execute(stringArray);
                        }
                    });

                } else {

                }


            }
        });
        mySharePreference = MySharePreference.getInstance(MainActivity.this);

    }

    private void CheckPermissions() {
        List<String> permission = new ArrayList<>();
        permission.add(EasyPermissionList.ACCESS_FINE_LOCATION);
        permission.add(EasyPermissionList.READ_EXTERNAL_STORAGE);
        permission.add(EasyPermissionList.WRITE_EXTERNAL_STORAGE);
        new EasyPermissionInit(MainActivity.this, permission);
    }

    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    private void getAdDetails() {
        String url= Constants.Url+"/api/kiosk_advertisement/advertisementList?org=flexi&locn=TA&user=kiosk&lang=en_US";
        VolleyUtils.getVolleyResult(MainActivity.this, Request.Method.GET, url, false,null, false, new VolleyListener() {
            @Override
            public void onError(String message) {
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getAdDetails",response+"--");
                try {
                    JSONArray array = response.getJSONArray("list");
                    if(array.length()>0) {
                        for (int k=0;k< array.length();k++) {
                            JSONObject object=array.getJSONObject(k);
                            UrlList.add(object.getString("ad_path_url"));
                        }
                        Log.e("URL",UrlList.get(pos));
                        buildMediaSource(Uri.parse(UrlList.get(pos)));

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        progressBar = this.findViewById(R.id.progressBar);
        loginTxt = this.findViewById(R.id.login);
        //   buildMediaSource(Uri.parse(videoUrl));
        videoFullScreenPlayer = this.findViewById(R.id.videoView);
        initializePlayer();
        loginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                releasePlayer();
                Intent loginIntent=new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
            }
        });
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                startAnim();
            }
        }, 1000);

    }
    public void startAnim(){
        loginTxt.setVisibility(View.VISIBLE);
        final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce_anim);
    /*    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 15);
        myAnim.setInterpolator(interpolator);*/
        myAnim.setRepeatCount(Animation.INFINITE);
        myAnim.setRepeatMode(Animation.INFINITE);
        loginTxt.startAnimation(myAnim);
    }
    private void initializePlayer() {
        if (player == null) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            // 1. Create a default TrackSelector
            DefaultLoadControl loadControl = new DefaultLoadControl.Builder().setBufferDurationsMs(32*1024, 64*1024, 1024, 1024).createDefaultLoadControl();
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(MainActivity.this, trackSelector, loadControl);

            videoFullScreenPlayer.setPlayer(player);
            videoFullScreenPlayer.setUseController(false);
            videoFullScreenPlayer.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);

        }
    }
    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(MainActivity.this,
                Util.getUserAgent(MainActivity.this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        DefaultBandwidthMeter   bandwidthMeter2 = new DefaultBandwidthMeter();

// Produces DataSource instances through which media data is loaded.
        dataSourceFactory = new DefaultDataSourceFactory(MainActivity.this,
                Util.getUserAgent(MainActivity.this, getString(R.string.app_name)), bandwidthMeter2);

// Produces Extractor instances for parsing the media data.
        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        MediaSource mediaSource = new ExtractorMediaSource(mUri,dataSourceFactory, extractorsFactory, null, null);
        // Loops the video indefinitely.
        LoopingMediaSource loopingSource = new LoopingMediaSource(mediaSource);
        // Prepare the player with the source.
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);

        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }
            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
                        progressBar.setVisibility(View.VISIBLE);
                        // thumImg.setVisibility(View.GONE);
                        videoFullScreenPlayer.setVisibility(View.VISIBLE);
                        Log.e("onPlayerStateChanged","Buffereing");
                        break;
                    case Player.STATE_ENDED:
                        // Activate the force enable
                        if(!isVideoEnded){
                            pos= pos+1;
                            if(pos<=UrlList.size()-1){
                                buildMediaSource(Uri.parse(UrlList.get(pos)));
                            }else{
                                pos=0;
                                buildMediaSource(Uri.parse(UrlList.get(pos)));
                            }
                            isVideoEnded=true;
                        }
                        Log.e("onPlayerStateChanged","Completed--"+pos+"=="+isVideoEnded);
                        break;
                    case Player.STATE_IDLE:
                        break;
                    case Player.STATE_READY:
                        isVideoEnded=false;
                        progressBar.setVisibility(View.GONE);
                        //  thumImg.setVisibility(View.GONE);
                        Log.e("onPlayerStateChanged","Ready now");

                        break;
                    default:
                        // status = PlaybackStatus.IDLE;
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
        Log.e("Lifecycle","OnResumeCalled");
       /* if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Lifecycle","OnResumeCalled");
        resumePlayer();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case EasyPermissionConstants.INTENT_CODE:

                if (data != null) {
                    boolean isGotAllPermissions =
                            data.getBooleanExtra(EasyPermissionConstants.IS_GOT_ALL_PERMISSION, false);

                    if (data.hasExtra(EasyPermissionConstants.IS_GOT_ALL_PERMISSION)) {
                       /* if (isGotAllPermissions) {
                            Toast.makeText(this, "All Permissions Granted", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "All permission not Granted", Toast.LENGTH_SHORT).show();
                        }}*/

                        // if you want to know which are the denied permissions.
                        if (data.getSerializableExtra(EasyPermissionConstants.DENIED_PERMISSION_LIST) != null) {

                            deniedPermissions = new ArrayList<>();

                            deniedPermissions.addAll((Collection<? extends String>) data.getSerializableExtra(
                                    EasyPermissionConstants.DENIED_PERMISSION_LIST));

                            if (deniedPermissions.size() > 0) {
                                for (int i = 0; i < deniedPermissions.size(); i++) {
                                    switch (deniedPermissions.get(i)) {

                                        case EasyPermissionList.READ_EXTERNAL_STORAGE:

                                            Toast.makeText(this, "Storage Permission not granted", Toast.LENGTH_SHORT)
                                                    .show();

                                            break;

                                        case EasyPermissionList.ACCESS_FINE_LOCATION:

                                            Toast.makeText(this, "Location Permission not granted", Toast.LENGTH_SHORT)
                                                    .show();

                                            break;

                                        case EasyPermissionList.WRITE_EXTERNAL_STORAGE:

                                            Toast.makeText(this, "Storage Permission not granted", Toast.LENGTH_SHORT)
                                                    .show();

                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
        }
        if (requestCode == 2296) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                } else {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Master Synchronising.......");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         **/
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Master Synchronising.......");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }

        /**
         * Downloading file in background thread
         **/
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                for (int i = 0; i < f_url.length; i++) {
                    URL url = new URL(f_url[i]);

                    try {
                        fname = FilenameUtils.getName(f_url[i]);
                    }
                    catch (Exception e)
                    {

                    }


                    // URL url = new URL(f_url[0]);
                    URLConnection conection = url.openConnection();
                    conection.connect();

                    // this will be useful so that you can show a tipical 0-100%
                    // progress bar
                    int lenghtOfFile = conection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(),
                            8192);
                    File pdfFolder = new File(Environment.getExternalStorageDirectory(), "kiosk");
                    //File pdfFolder = new File(getApplicationContext().getFilesDir(), "kiosk");
                    if (!pdfFolder.exists()) {
                        pdfFolder.mkdirs();
                        Log.e("LOG_TAG", "Video Directory created"+pdfFolder);
                    }


                    SharedPreferences preferences;
                    File myFile = new File(pdfFolder +"/"+fname);
                    preferences = getSharedPreferences("Videoinfo",0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("video", String.valueOf(pdfFolder));

                    // Output stream
                    OutputStream output = new FileOutputStream(myFile);

                    byte data[] = new byte[1024];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();
                }
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         **/
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            //dismissDialog(progress_bar_type);
            pDialog.dismiss();
            progress.dismiss();

        }


    }

    private void requestPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s",getApplicationContext().getPackageName())));
                startActivityForResult(intent, 2296);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, 2296);
            }
        } else {
            //below android 11
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

}