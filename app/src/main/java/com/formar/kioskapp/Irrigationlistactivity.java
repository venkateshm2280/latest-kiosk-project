package com.formar.kioskapp;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.adapter.Irrigationadapter;
import com.formar.kioskapp.database.DBHandler;
import com.formar.kioskapp.model.irrigationlist;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class Irrigationlistactivity extends BaseActivity {
    RecyclerView soilList;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public String FarmerCode="";
    AppCompatImageView logout,home,backBtn;
    ArrayList<irrigationlist> soilArrayList=new ArrayList<>();
    private Irrigationadapter adapter;
    AppCompatImageView language_icon;
    DBHandler dbHandler;
    SQLiteDatabase dbs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_irrigationlistactivity);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        initViews();
    }

    private void initViews() {
        dbHandler = new DBHandler(this);
        dbs = dbHandler.getWritableDatabase();
        soilList=findViewById(R.id.soilCodeList);
        adapter=new Irrigationadapter(Irrigationlistactivity.this,soilArrayList);
        soilList.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Irrigationlistactivity.this,RecyclerView.VERTICAL,false);
        soilList.setLayoutManager(mLayoutManager);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        FarmerCode=sharedpreferences.getString("farmerGid","");
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLogout(Irrigationlistactivity.this);
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHomepage(Irrigationlistactivity.this);
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Log.e("VVV",""+sharedpreferences.getString("farmerGid",""));

        if(isNetworkAvailable()) {


            getSoilListDetails();
        }
        else
        {
            String selectQuery = "SELECT  * FROM ilist WHERE farmer_gid = "+sharedpreferences.getString("farmerGid","");

            Cursor cursor = dbs.rawQuery(selectQuery, null);

            if(cursor.moveToFirst())
            {
                do {
                    Log.e("VVV",""+cursor.getString(1));
                    Log.e("VVV",""+cursor.getString(2));
                    Log.e("VVV",""+cursor.getString(3));
                    Log.e("VVV",""+cursor.getString(4));

                    String selectQuery2 = "SELECT  * FROM core_mst_tmaster WHERE master_rowid = "+cursor.getString(5);
                    Cursor cursor2 = dbs.rawQuery(selectQuery2, null);
                    if(cursor2.moveToNext())
                    {
                        Log.e("VVV",""+cursor2.getString(2));
                        Cursor cursor3= dbs.query("core_mst_tmastertranslate", new String[]{"master_name"
                                }, "mastertranslate_master_code" + "=? COLLATE NOCASE",
                                new String[]{cursor2.getString(2)}, null, null, null, null);

                        if(cursor3.moveToNext())
                        {
                            // Log.e("VVV",""+cursor3.getString(1));
                            soilArrayList.add(new irrigationlist(Integer.parseInt(cursor.getString(1)),Integer.parseInt(sharedpreferences.getString("farmerGid","")),sharedpreferences.getString("fcode",""),
                                    sharedpreferences.getString("fname",""),cursor.getString(2),cursor.getString(3),cursor3.getString(0),"",cursor.getString(16)));
                        }

                    }



                }while (cursor.moveToNext());
                adapter.notifyDataSetChanged();
            }
//            Toast.makeText(context, ""+cursor.getCount(), Toast.LENGTH_SHORT).show();


        }
        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(Irrigationlistactivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(Irrigationlistactivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
    }
    private void getSoilListDetails() {
        String url= Constants.Url+"/api/Kiosk_IrrigationWaterTest/IrrigationList?org="+FarmerCode+"&locn=TA&lang=en_US&In_farmer_gid=0&status=ALL";
        VolleyUtils.getVolleyResult(Irrigationlistactivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getirri",response.toString());
                try {
                    JSONArray array=response.getJSONArray("list");
                    if(array.length()>0){
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            soilArrayList.add(new irrigationlist(object.getInt("in_irrigation_gid"),object.getInt("in_farmer_gid"),object.getString("in_farmer_code"),
                                    object.getString("in_farmer_name"),object.getString("collection_date"),object.getString("tran_id"),object.getString("irrigation_status"),object.getString("farmer_irrigation_rejreason"),object.getString("farmer_irrigation_CropRecom")));
                        }
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }





}