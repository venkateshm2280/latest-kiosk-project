package com.formar.kioskapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CropRec extends AppCompatActivity {

    ImageView back;
    TextView tvJustified;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_rec);
        setTitle("CROP Recomentation");

        back = (ImageView)findViewById(R.id.back);
        tvJustified = (TextView)findViewById(R.id.tvJustified);

        Intent i = getIntent();
        tvJustified.setText(i.getStringExtra("TXT"));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}