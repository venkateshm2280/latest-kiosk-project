package com.formar.kioskapp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyUtils {
    public static String AuthKey="";
    public static final String MyPREFERENCES = "MyPrefs";
    public static void getVolleyResult(Context cntx, int type, String url, boolean isloading, JSONObject object, boolean iskey, final VolleyListener listener){
        ProgressDialog pdialog;
        pdialog = new ProgressDialog(cntx);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setTitle(cntx.getResources().getString(R.string.loader_title));
        if(isloading) {
            pdialog.show();
        }
        SharedPreferences sharedpreferences = cntx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String  AuthKey= sharedpreferences.getString("AuthKey","");
        Log.e("getVolleyResult",url+"\n"+AuthKey);
        JsonObjectRequest request=new JsonObjectRequest(type, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                if(isloading) {
                    pdialog.dismiss();
                }

            }
        },   new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(isloading) {
                    pdialog.dismiss();
                }
                listener.onError(error.toString());
                Toast.makeText(cntx, cntx.getResources().getString(R.string.error_title), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                if(iskey){
                    params.put("Authorization", AuthKey);
                }
                return params;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                2500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(cntx).addToRequestQueue(request);
    }
    public static final String downloadDirectory = "Kiosk Downloads";
    public static final String mainUrl = "http://www.africau.edu/images/default/sample.pdf";

}
