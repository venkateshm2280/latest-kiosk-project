package com.formar.kioskapp.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import java.util.Locale;

public class AppClass extends Application
{

    private static AppClass appClass;
    public static AppClass getintance()
    {
        return appClass;
    }
    private Locale locale;
    private static Context context;
    public static Context getAppContext() {
        return context;
    }
    @Override
    public void onCreate()
    {
        super.onCreate();
        appClass = this;
        MySharePreference.getInstance(this);
        context = getBaseContext();
        changeLocale(context);

    }


    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
    public void changeLocale(Context context) {
        Configuration config = context.getResources().getConfiguration();
      MySharePreference  mySharePreference = MySharePreference.getInstance(context);
        String lang =mySharePreference.getLanguage();
        Log.e("MyApp", "language in MyApp = " + lang);
        if (!(config.locale.getLanguage().equals(lang))) {
            locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            Log.e("MyApp", "Inside if = " + lang);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            context.getResources().updateConfiguration(newConfig, context.getResources().getDisplayMetrics());
        }
    }

}