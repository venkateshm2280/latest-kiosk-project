package com.formar.kioskapp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadTask {
    private static final String TAG = "Download Task";
    private Context context;
    private String downloadUrl = "", downloadFileName = "";
    String pathFolder = "";
    String pathFile = "";

    public DownloadTask(Context context,  String downloadUrl) {
        this.context = context;
        this.downloadUrl = downloadUrl;
        String fileName = downloadUrl.substring( downloadUrl.lastIndexOf('/')+1, downloadUrl.length() );
        String fileNameWithoutExtn = fileName.substring(fileName.indexOf('.'), fileName.length());
        downloadFileName = "KioskDownload"+"_"+System.currentTimeMillis()+fileNameWithoutExtn;//Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName);
        //Start Downloading Task
        new DownloadFileFromURL().execute();
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        File apkStorage = null;
        File outputFile = null;
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setTitle("Processing...");
            pd.setMessage("Please wait.");
            pd.setMax(100);
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setCancelable(true);
            pd.show();
        }



        @Override
        protected String doInBackground(String... f_url) {
            int count;

            try {
                pathFolder = Environment.getExternalStorageDirectory() + "/KioskDownloads";
                pathFile = pathFolder + "/"+downloadFileName;
                File futureStudioIconFile = new File(pathFolder);
                if(!futureStudioIconFile.exists()){
                    futureStudioIconFile.mkdirs();
                }

                URL url = new URL(downloadUrl);
                URLConnection connection = url.openConnection();
                connection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lengthOfFile = connection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream());
                FileOutputStream output = new FileOutputStream(pathFile);

                byte data[] = new byte[1024]; //anybody know what 1024 means ?
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+ (int)((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();


            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return pathFile;

        }
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pd.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String file_url) {
            if (pd!=null) {
                pd.dismiss();
            }
            Toast.makeText(context, "File Downloaded at: "+file_url, Toast.LENGTH_SHORT).show();
           /* StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent i = new Intent(Intent.ACTION_VIEW);

            i.setDataAndType(Uri.fromFile(new File(file_url)), "application/vnd.android.package-archive" );
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(i);*/
        }


    }

}
