package com.formar.kioskapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import androidx.appcompat.widget.AppCompatImageView;

import com.formar.kioskapp.view.Dashboard;
import com.formar.kioskapp.view.LoginActivity;

public class Constants {
    public static String Url="http://169.38.82.131:751";
    public static String Url1="https://nafcca.co.in";
    public static String getMasters="";
    public static final String MyPREFERENCES = "MyPrefs" ;
    private static SharedPreferences sharedpreferences;
    public  static String AuthKey="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJuYmYiOjE2MTYwNjcwMDUsImV4cCI6MTc3Mzc0NzAwNSwiaWF0IjoxNjE2MDY3MDA1fQ.s96PBFXnQEppTnkSMisgFP6VP5dtJQ3WifFq1jzKi4E";
    public static int dpToPx(Context cntx,int dp) {
        DisplayMetrics displayMetrics = cntx.getResources().getDisplayMetrics();
        Resources r = cntx.getResources();
        int px = Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
        return px;
    }
    public static int pxToDp(Context cntx,int px) {
        DisplayMetrics displayMetrics = cntx.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void setLanguage(Context cntx, String name, AppCompatImageView language_icon,int type){
      MySharePreference  mySharePreference = MySharePreference.getInstance(cntx);
        mySharePreference.setLanguage(name);
        if(type==1) {
            cntx.startActivity(new Intent(cntx, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }else{
            cntx.startActivity(new Intent(cntx, Dashboard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }
    public static void setHomepage(Context cntx){
        Intent i = new Intent(cntx, Dashboard.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        cntx.startActivity(i);
    }
    public static void setLogout(Context cntx){
        SharedPreferences sharedpreferences = cntx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("fname", "");
        editor.putString("fcode", "");
        editor.putString("fvillage", "");
        editor.putString("fvillage_code", "");
        editor.putString("ftaluk", "");
        editor.putString("fdistname", "");
        editor.putString("state_name", "");
        editor.putString("farmerGid","");
        editor.putString("AuthKey","");
        editor.putString("villnames", "");
        editor.putString("kvillageid","");
        editor.commit();
        Intent i = new Intent(cntx, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        cntx.startActivity(i);

    }

}
