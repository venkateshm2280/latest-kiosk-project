package com.formar.kioskapp.model;

public class WeatherListModel {

    private String date;

    private String villageId;

    private Double tmax;

    private Double tmin;

    private Integer rh1;

    private Integer rh2;

    private Double rhAvg;

    private Integer ws1;

    private Integer ws2;

    private Double wsAvg;

    private Double rf;

    /**
     * No args constructor for use in serialization
     *
     */
    public WeatherListModel() {
    }

    /**
     *
     * @param date
     * @param ws1
     * @param wsAvg
     * @param rf
     * @param tmax
     * @param ws2
     * @param tmin
     * @param rh1
     * @param rhAvg
     * @param villageId
     * @param rh2
     */
    public WeatherListModel(String date, String villageId, Double tmax, Double tmin, Integer rh1, Integer rh2, Double rhAvg, Integer ws1, Integer ws2, Double wsAvg, Double rf) {
        super();
        this.date = date;
        this.villageId = villageId;
        this.tmax = tmax;
        this.tmin = tmin;
        this.rh1 = rh1;
        this.rh2 = rh2;
        this.rhAvg = rhAvg;
        this.ws1 = ws1;
        this.ws2 = ws2;
        this.wsAvg = wsAvg;
        this.rf = rf;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public Double getTmax() {
        return tmax;
    }

    public void setTmax(Double tmax) {
        this.tmax = tmax;
    }

    public Double getTmin() {
        return tmin;
    }

    public void setTmin(Double tmin) {
        this.tmin = tmin;
    }

    public Integer getRh1() {
        return rh1;
    }

    public void setRh1(Integer rh1) {
        this.rh1 = rh1;
    }

    public Integer getRh2() {
        return rh2;
    }

    public void setRh2(Integer rh2) {
        this.rh2 = rh2;
    }

    public Double getRhAvg() {
        return rhAvg;
    }

    public void setRhAvg(Double rhAvg) {
        this.rhAvg = rhAvg;
    }

    public Integer getWs1() {
        return ws1;
    }

    public void setWs1(Integer ws1) {
        this.ws1 = ws1;
    }

    public Integer getWs2() {
        return ws2;
    }

    public void setWs2(Integer ws2) {
        this.ws2 = ws2;
    }

    public Double getWsAvg() {
        return wsAvg;
    }

    public void setWsAvg(Double wsAvg) {
        this.wsAvg = wsAvg;
    }

    public Double getRf() {
        return rf;
    }

    public void setRf(Double rf) {
        this.rf = rf;
    }

}
