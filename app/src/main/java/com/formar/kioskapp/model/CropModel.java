package com.formar.kioskapp.model;

public class CropModel {
    String cropId;
    String cropName;
    String message_ta;
    String message_en;

    public CropModel(String cropId, String cropName, String message_ta, String message_en, String villageId, String formarName, String villageName, String district_name) {
        this.cropId = cropId;
        this.cropName = cropName;
        this.message_ta = message_ta;
        this.message_en = message_en;
        this.villageId = villageId;
        this.formarName = formarName;
        this.villageName = villageName;
        this.District_name = district_name;
    }

    String villageId;
    String formarName;
    String villageName;
    String District_name;

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getMessage_ta() {
        return message_ta;
    }

    public void setMessage_ta(String message_ta) {
        this.message_ta = message_ta;
    }

    public String getMessage_en() {
        return message_en;
    }

    public void setMessage_en(String message_en) {
        this.message_en = message_en;
    }

    public String getVillageId() {
        return villageId;
    }

    public void setVillageId(String villageId) {
        this.villageId = villageId;
    }

    public String getFormarName() {
        return formarName;
    }

    public void setFormarName(String formarName) {
        this.formarName = formarName;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getDistrict_name() {
        return District_name;
    }

    public void setDistrict_name(String district_name) {
        District_name = district_name;
    }

}
