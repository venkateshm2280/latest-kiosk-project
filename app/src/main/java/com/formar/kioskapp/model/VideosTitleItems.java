package com.formar.kioskapp.model;

public class VideosTitleItems {
    String Title;

    public VideosTitleItems(String title, String colorCode, String id) {
        Title = title;
        this.colorCode = colorCode;
        this.id = id;
    }

    String colorCode;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }



}
