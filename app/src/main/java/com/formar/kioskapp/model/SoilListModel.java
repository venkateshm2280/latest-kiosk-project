package com.formar.kioskapp.model;

public class SoilListModel {

    private Integer inSoilGid;

    private Integer inFarmerGid;

    private String inFarmerCode;

    private String inFarmerName;

    private String collectionDate;

    private String tranId;

    private String soilStatus;

    public String getCroprec() {
        return croprec;
    }

    public void setCroprec(String croprec) {
        this.croprec = croprec;
    }

    private String croprec;

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    private String rejectReason;

    /**
     * No args constructor for use in serialization
     *
     */
    public SoilListModel() {
    }

    /**
     *
     * @param tranId
     * @param inFarmerCode
     * @param inSoilGid
     * @param inFarmerGid
     * @param inFarmerName
     * @param soilStatus
     * @param collectionDate
     */
    public SoilListModel(Integer inSoilGid, Integer inFarmerGid, String inFarmerCode, String inFarmerName, String collectionDate, String tranId, String soilStatus,String reason,String croprec) {
        super();
        this.inSoilGid = inSoilGid;
        this.inFarmerGid = inFarmerGid;
        this.inFarmerCode = inFarmerCode;
        this.inFarmerName = inFarmerName;
        this.collectionDate = collectionDate;
        this.tranId = tranId;
        this.soilStatus = soilStatus;
        this.rejectReason = reason;
        this.croprec = croprec;
    }

    public Integer getInSoilGid() {
        return inSoilGid;
    }

    public void setInSoilGid(Integer inSoilGid) {
        this.inSoilGid = inSoilGid;
    }

    public Integer getInFarmerGid() {
        return inFarmerGid;
    }

    public void setInFarmerGid(Integer inFarmerGid) {
        this.inFarmerGid = inFarmerGid;
    }

    public String getInFarmerCode() {
        return inFarmerCode;
    }

    public void setInFarmerCode(String inFarmerCode) {
        this.inFarmerCode = inFarmerCode;
    }

    public String getInFarmerName() {
        return inFarmerName;
    }

    public void setInFarmerName(String inFarmerName) {
        this.inFarmerName = inFarmerName;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getSoilStatus() {
        return soilStatus;
    }

    public void setSoilStatus(String soilStatus) {
        this.soilStatus = soilStatus;
    }

}
