package com.formar.kioskapp.model;

public class VillageModel {

    private String outParentCode;

    private String outMasterCode;

    private String outMasterDescription;

    private String outMasterLlDescription;

    private String outKamarajVillageId;

    private String outKamarajDistrictId;
    /**
     * No args constructor for use in serialization
     *
     */
    public VillageModel() {
    }

    /**
     *
     * @param outMasterDescription
     * @param outParentCode
     * @param outMasterCode
     * @param outMasterLlDescription
     *
     */
    public VillageModel(String outParentCode, String outMasterCode, String outMasterDescription, String outMasterLlDescription
    , String outKamarajVillageId, String outKamarajDistrictId) {
        super();
        this.outParentCode = outParentCode;
        this.outMasterCode = outMasterCode;
        this.outMasterDescription = outMasterDescription;
        this.outMasterLlDescription = outMasterLlDescription;
        this.outKamarajVillageId = outKamarajVillageId;
        this.outKamarajDistrictId = outKamarajDistrictId;
    }

    public String getOutParentCode() {
        return outParentCode;
    }

    public void setOutParentCode(String outParentCode) {
        this.outParentCode = outParentCode;
    }

    public String getOutMasterCode() {
        return outMasterCode;
    }

    public void setOutMasterCode(String outMasterCode) {
        this.outMasterCode = outMasterCode;
    }

    public String getOutMasterDescription() {
        return outMasterDescription;
    }

    public void setOutMasterDescription(String outMasterDescription) {
        this.outMasterDescription = outMasterDescription;
    }

    public String getOutMasterLlDescription() {
        return outMasterLlDescription;
    }

    public void setOutMasterLlDescription(String outMasterLlDescription) {
        this.outMasterLlDescription = outMasterLlDescription;
    }

    public String getOutKamarajVillageId() {
        return outKamarajVillageId;
    }

    public void setOutKamarajVillageId(String outKamarajVillageId) {
        this.outKamarajVillageId = outKamarajVillageId;
    }

    public String getOutKamarajDistrictId() {
        return outKamarajDistrictId;
    }

    public void setOutKamarajDistrictId(String outKamarajDistrictId) {
        this.outKamarajDistrictId = outKamarajDistrictId;
    }
}
