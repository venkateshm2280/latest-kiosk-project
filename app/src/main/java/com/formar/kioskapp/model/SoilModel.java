package com.formar.kioskapp.model;

public class SoilModel {

    private Integer outSoilparamRowid;

    private String outSoilId;

    private String outSoilParameter;

    private String outUOM;

    private String outSoilResult;

    /**
     * No args constructor for use in serialization
     *
     */


    /**
     *
     * @param outSoilId
     * @param outUOM
     * @param outSoilResult
     * @param outSoilparamRowid
     * @param outSoilParameter
     */
    public SoilModel(int outSoilparamRowid, String outSoilId, String outSoilParameter, String outUOM, String outSoilResult) {
        super();
        this.outSoilparamRowid = outSoilparamRowid;
        this.outSoilId = outSoilId;
        this.outSoilParameter = outSoilParameter;
        this.outUOM = outUOM;
        this.outSoilResult = outSoilResult;
    }

    public Integer getOutSoilparamRowid() {
        return outSoilparamRowid;
    }

    public void setOutSoilparamRowid(Integer outSoilparamRowid) {
        this.outSoilparamRowid = outSoilparamRowid;
    }

    public String getOutSoilId() {
        return outSoilId;
    }

    public void setOutSoilId(String outSoilId) {
        this.outSoilId = outSoilId;
    }

    public String getOutSoilParameter() {
        return outSoilParameter;
    }

    public void setOutSoilParameter(String outSoilParameter) {
        this.outSoilParameter = outSoilParameter;
    }

    public String getOutUOM() {
        return outUOM;
    }

    public void setOutUOM(String outUOM) {
        this.outUOM = outUOM;
    }

    public String getOutSoilResult() {
        return outSoilResult;
    }

    public void setOutSoilResult(String outSoilResult) {
        this.outSoilResult = outSoilResult;
    }
}
