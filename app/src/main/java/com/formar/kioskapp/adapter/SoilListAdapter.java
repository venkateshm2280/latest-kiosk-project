package com.formar.kioskapp.adapter;


import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.internal.service.Common;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.formar.kioskapp.CropRec;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.database.DBHandler;
import com.formar.kioskapp.model.SoilListModel;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;
import com.formar.kioskapp.view.SoilActivity;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class SoilListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    DBHandler dbHandler;
    SQLiteDatabase dbs;

    private WebView wv=null;
    private PrintManager mgr=null;
    List<SoilListModel> data=new ArrayList<>();
    Typeface typefaceRegular, typefaceBold;
    SharedPreferences sharedpreferences;
    String tm;
    String value1="";

    public static final String MyPREFERENCES = "MyPrefs" ;
    public String FarmerCode="";
    public String FarmerName="";
    public String soilgid="";
    public String tran_id="";
    private String qrReportno="";
    private String qrReportDate="";
    private String qrUniLabReportNo="";
    private String qrCustRefeerence="";
    private String qrLabId="";
    private String headrValue;
    private String signatureValue="";
    public String Farmeraddress="";
    public String Farmeraddress2="";
    public String date2="";
    public String adate2="";
    public String acdate2="";
    public String sdate2="";

    public SoilListAdapter(Context context, List<SoilListModel> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.soillistitem, parent,false);
        MyHolder holder=new MyHolder(view);
        //mgr=(PrintManager)getSystemService(PRINT_SERVICE);
        mgr=(PrintManager) context.getSystemService(Context.PRINT_SERVICE);

        return holder;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final SoilListModel list=data.get(position);

        MyHolder myHolder = (MyHolder) holder;

        myHolder.textview_fcode.setText(" "+list.getInFarmerCode());
        myHolder.textview_name.setText(" "+list.getInFarmerName());
        myHolder.textview_date.setText(" "+list.getCollectionDate());
        myHolder.textview_tranId.setText(" "+list.getTranId());
        myHolder.textview_soilStatus.setText(" "+list.getSoilStatus());
        myHolder.viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, SoilActivity.class);
                i.putExtra("soilGid",String.valueOf(list.getInSoilGid()));
                i.putExtra("tran_id",String.valueOf(list.getTranId()));
                context.startActivity(i);
            }
        });

        myHolder.sendemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {


                    sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    FarmerCode = sharedpreferences.getString("fcode", "");
                    FarmerName = sharedpreferences.getString("farmer_name", "");
                    soilgid = String.valueOf(list.getInSoilGid());
                    tran_id = String.valueOf(list.getTranId());
                    showVillageDialog();
                }
                else
                {
                    Toast.makeText(context, "No Internet Available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        myHolder.print.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                showDialog(list);


               /* try {
                    createPdf(String.valueOf(list.getInSoilGid()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }*/

            }
        });

        myHolder.prints.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                // Toast.makeText(context.getApplicationContext(),"Hello Javatpoint",Toast.LENGTH_SHORT).show();

                try {
                    //  createPdf(String.valueOf(list.getInSoilGid()));

                    createPdf(String.valueOf(list.getInSoilGid()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                //doPrint();
            }
        });

        myHolder.cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CropRec.class);
                i.putExtra("TXT",list.getCroprec());
                context.startActivity(i);
            }
        });
        if(list.getSoilStatus().equals("Rejected")){
            myHolder.viewBtn.setVisibility(View.GONE);
            myHolder.reasonContainer.setVisibility(View.VISIBLE);
            myHolder.rejectreasontxt.setText(" "+list.getRejectReason());
            myHolder.sendemail.setVisibility(View.INVISIBLE);
            myHolder.print.setVisibility(View.INVISIBLE);
            myHolder.prints.setVisibility(View.INVISIBLE);
            myHolder.cr.setVisibility(View.INVISIBLE);
            myHolder.txtemails.setVisibility(View.INVISIBLE);
            myHolder.txtviews.setVisibility(View.INVISIBLE);
            myHolder.txtprints.setVisibility(View.INVISIBLE);
            myHolder.txtcroprecomm.setVisibility(View.INVISIBLE);
        }else{
            //myHolder.viewBtn.setVisibility(View.VISIBLE);
            myHolder.reasonContainer.setVisibility(View.GONE);
            myHolder.sendemail.setVisibility(View.VISIBLE);
            myHolder.print.setVisibility(View.VISIBLE);
            myHolder.prints.setVisibility(View.VISIBLE);
            myHolder.cr.setVisibility(View.VISIBLE);
            myHolder.txtemails.setVisibility(View.VISIBLE);
            myHolder.txtviews.setVisibility(View.VISIBLE);
            myHolder.txtprints.setVisibility(View.VISIBLE);
            myHolder.txtcroprecomm.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView textview_fcode;
        TextView textview_name;
        TextView textview_date;
        TextView textview_tranId;
        TextView textview_soilStatus,rejectreasontxt;
        TextView txtemails, txtviews, txtprints, txtcroprecomm;
        AppCompatButton viewBtn;
        Button sendemail,print,cr,prints;
        LinearLayout reasonContainer;
        public MyHolder(View itemView) {
            super(itemView);
            textview_fcode=(TextView)itemView.findViewById(R.id.farmerCodetxt);
            textview_name=(TextView)itemView.findViewById(R.id.farmernametxt);
            textview_date=(TextView)itemView.findViewById(R.id.collectdatetxt);
            textview_tranId= (TextView)itemView.findViewById(R.id.transIdtxt);
            textview_soilStatus= (TextView)itemView.findViewById(R.id.statustxt);
            rejectreasontxt= (TextView)itemView.findViewById(R.id.rejectreasontxt);
            sendemail= (Button) itemView.findViewById(R.id.sendemail);
            print= (Button) itemView.findViewById(R.id.print);
            prints= (Button) itemView.findViewById(R.id.prints);
            cr= (Button) itemView.findViewById(R.id.cr);
            reasonContainer= itemView.findViewById(R.id.reasonContainer);
            viewBtn=itemView.findViewById(R.id.viewitem);
            txtemails=itemView.findViewById(R.id.txtemails);
            txtviews=itemView.findViewById(R.id.txtviews);
            txtprints=itemView.findViewById(R.id.txtprints);
            txtcroprecomm=itemView.findViewById(R.id.txtcroprecomm);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void doPrint() {
        String customHtml = "<html><body><h1>Hello, AbhiAndroid</h1>" +
                "<h1>Heading 1</h1><h2>Heading 2</h2><h3>Heading 3</h3>" +
                "<p>This is a sample paragraph of static HTML In Web view</p>" +
                "</body></html>";
        WebView print=prepPrintWebView("Web page");

        //print.loadUrl("https://commonsware.com/Android");
        print.loadData(customHtml, "text/html", "UTF-8");

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private PrintJob print(String name, PrintDocumentAdapter adapter, PrintAttributes attrs) {

        context.startService(new Intent(context,PrintJobMonitorService.class));
        return(mgr.print(name, adapter, attrs));

    }

    private WebView prepPrintWebView(final String name) {
        WebView result=getWebView();

        result.setWebViewClient(new WebViewClient() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onPageFinished(WebView view, String url) {
                print(name, view.createPrintDocumentAdapter(),
                        new PrintAttributes.Builder().build());
            }
        });

        return(result);
    }

    private WebView getWebView() {
        if (wv == null) { wv=new WebView(context); }
        return(wv);
    }

    private void showVillageDialog() {
        LayoutInflater inflater = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        View customView = inflater.inflate(R.layout.mailpopup_item, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(customView);
        builder.setTitle("Send Mail..");
        builder.setCancelable(true);
        AppCompatEditText mailtxt=customView.findViewById(R.id.mailtxt);
        AppCompatButton submitxt=customView.findViewById(R.id.submit_button);
        AlertDialog alertDialog = builder.show();

        submitxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mailtxt.getText().toString().isEmpty()){
                    Toast.makeText(context, "Enter valid emailid!..", Toast.LENGTH_SHORT).show();
                }else if(!isValidEmail(mailtxt.getText().toString())){
                    Toast.makeText(context, "Enter valid emailid!..", Toast.LENGTH_SHORT).show();
                }else {
                    sendEmail(mailtxt.getText().toString());
                    alertDialog.cancel();
                }
            }
        });
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void  sendEmail(String email){
        String url= Constants.Url+"/api/Kiosk_Soil_Card/kioskEmailsent?org=kiosk&locn=ta&user=admin&lang=en_us&email="+email+"&In_farmer_code="+FarmerCode+"&soil_gid="+soilgid+"&In_Tran_Id="+tran_id+"&In_user_code="+FarmerName;
        VolleyUtils.getVolleyResult(context, Request.Method.GET, url, true, null, false, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("sendEmail",response.toString());
                Toast.makeText(context, "Mail sent successfully!..", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPrintingDetails() {
        String url=Constants.Url+"/api/Print/apiSoilCardPrint?org=test&locn=TA&user=admin&lang=en_US&soil_gid="+soilgid+"&In_Tran_Id="+tran_id+"&In_farmer_code="+FarmerCode;
        VolleyUtils.getVolleyResult(context, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getPrintingDetails",response.toString());
                try {
                    String path=response.getString("path");
                    if(path!=null && !path.isEmpty()) {
                        openWebPage(path);
                    }else{
                        Toast.makeText(context, "Soil Report Failed!..", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public void openWebPage(String url) {
        Log.e("openWebPage",url);
        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createPdf(String id) throws FileNotFoundException, DocumentException {

        File pdfFolder = new File(Environment.getExternalStorageDirectory(), "pdfdemo"+ UUID.randomUUID());
        //File pdfFolder = new File(context.getFilesDir(), "pdfdemo");
        if (!pdfFolder.exists()) {
            pdfFolder.mkdirs();
            Log.e("LOG_TAG", "Pdf Directory created"+pdfFolder);
        }

        //Create time stamp
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(date);

        File myFile = new File(pdfFolder +"/"+ timeStamp + ".pdf");

        OutputStream output = new FileOutputStream(myFile);

        //Step 1
        Document document = new Document(PageSize.A4, 36, 36, 36, 36);

        //Step 2
        PdfWriter writer = PdfWriter.getInstance(document, output);

        //Step 3
        document.open();



        // add image
        Drawable d = context.getResources().getDrawable(R.drawable.updatednaflogos);
        BitmapDrawable bitDw = ((BitmapDrawable) d);
        Bitmap bmp = bitDw.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = null;
        try {
            image = Image.getInstance(stream.toByteArray());
            image.scaleToFit(100,100);

            // image.setAbsolutePosition( 0,180);
            image.setAlignment(Element.ALIGN_RIGHT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PdfPTable table = new PdfPTable(3);
        float[] columnWidths = {4, 12, 3};
        table.setWidthPercentage(100);
        table.setWidths(columnWidths);
        PdfPCell cellimage = new PdfPCell();
        cellimage.setImage(image);
        cellimage.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellimage);

        PdfPCell cell = new PdfPCell();
        //cell.setPaddingRight(40);
        Font green = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, WebColors.getRGBColor("#8CBB78"));
        Chunk redText = new Chunk("NATIONAL AGRO FOUNDATION", green);
        Paragraph p = new Paragraph();
        p.setAlignment(Element.ALIGN_LEFT);
        p.add(redText);
        cell.addElement(p);
        Font black = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
        Chunk text2 = new Chunk("LABORATARY SERVICES DIVISION", black);
        Paragraph p2 = new Paragraph();
        p2.setAlignment(Element.ALIGN_LEFT);
        p2.add(text2);
        cell.addElement(p2);
        Font black2 = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);
        Chunk text3 = new Chunk("Anna University Tharamani Campus,", black2);
        Paragraph p3 = new Paragraph();
        p3.setAlignment(Element.ALIGN_LEFT);
        p3.add(text3);
        cell.addElement(p3);
        Font black3 = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);
        Chunk text4 = new Chunk("CSIR Road, Tharamani, Chennai-600113", black3);
        Paragraph p4 = new Paragraph();
        p4.setAlignment(Element.ALIGN_LEFT);
        p4.add(text4);
        cell.addElement(p4);
        Font black4 = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);
        Chunk text5 = new Chunk("India", black4);
        Paragraph p5 = new Paragraph();
        p5.setAlignment(Element.ALIGN_LEFT);
        p5.add(text5);
        cell.addElement(p5);


        Font black5 = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
        Chunk text6 = new Chunk("Phone: +91-44-22542598/+91-90807-11501", black5);
        Paragraph p6 = new Paragraph();
        p6.setAlignment(Element.ALIGN_LEFT);
        p6.add(Chunk.NEWLINE);
        p6.add(text6);
        cell.addElement(p6);
        Font black6 = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
        Chunk text7 = new Chunk("Email: labservices@nationalagro.org", black6);
        Paragraph p7 = new Paragraph();
        p7.setAlignment(Element.ALIGN_LEFT);
        p7.add(text7);
        cell.addElement(p7);
        Font black7 = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
        Chunk text8 = new Chunk("Website: www.nationalagro.org", black7);
        Paragraph p8 = new Paragraph();
        p8.setAlignment(Element.ALIGN_LEFT);
        p8.add(text8);
        cell.addElement(p8);



        cell.setVerticalAlignment(Element.ALIGN_TOP);
        cell.setBorder(Rectangle.NO_BORDER);
        //cell.setPaddingLeft(2);
        table.addCell(cell);

        //nabl
        Drawable d1 = context.getResources().getDrawable(R.drawable.nabl);
        BitmapDrawable bitDw1 = ((BitmapDrawable) d1);
        Bitmap bmp1 = bitDw1.getBitmap();
        ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
        bmp1.compress(Bitmap.CompressFormat.PNG, 100, stream1);
        Image image1 = null;
        try {
            image1 = Image.getInstance(stream1.toByteArray());
            image1.scaleToFit(100,100);

            // image.setAbsolutePosition( 0,180);
            image1.setAlignment(Element.ALIGN_RIGHT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*table.setWidthPercentage(100);
        table.setWidths(new int[]{1, 3});*/
        PdfPCell cellimage1 = new PdfPCell();
        cellimage1.setImage(image1);
        cellimage1.setBorder(Rectangle.NO_BORDER);
        table.addCell(cellimage1);


        document.add(table);

        PdfPTable table2 = new PdfPTable(2);

        table2.setWidthPercentage(100);
        table2.setWidths(new int[]{3, 3});
       /* PdfPCell cellimage2 = new PdfPCell();
        cellimage2.setImage("");
        cellimage2.setBorder(Rectangle.NO_BORDER);
        table2.addCell(cellimage2)*/;

        PdfPCell cellnextpara = new PdfPCell();
        cellnextpara.setPaddingLeft(10);



        cellnextpara.setVerticalAlignment(Element.ALIGN_TOP);
        cellnextpara.setBorder(Rectangle.NO_BORDER);
        table2.addCell(cellnextpara);

        //  document.add(table2);

        PdfPCell cellnextpara1 = new PdfPCell();
        //  cellnextpara1.setPaddingLeft(20);


        Font black10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
        Chunk text11 = new Chunk("Accredited as per ISO/IEC 17025:2017 Standard", black10);
        Paragraph p11 = new Paragraph();
        p11.setAlignment(Element.ALIGN_RIGHT);
        p11.add(text11);
        cellnextpara1.addElement(p11);
        Font black11 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
        Chunk text12 = new Chunk("by National Accreditation Board for Testing and", black11);
        Paragraph p12 = new Paragraph();
        p12.setAlignment(Element.ALIGN_RIGHT);
        p12.add(text12);
        cellnextpara1.addElement(p12);
        Font black12 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
        Chunk text13 = new Chunk("Calibration Laboratories (NABL),a constituent", black12);
        Paragraph p13 = new Paragraph();
        p13.setAlignment(Element.ALIGN_RIGHT);
        p13.add(text13);
        cellnextpara1.addElement(p13);
        Font black13 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
        Chunk text14 = new Chunk("Board of Quality Council Of India(QCI)", black13);
        Paragraph p14 = new Paragraph();
        p14.setAlignment(Element.ALIGN_RIGHT);
        p14.add(text14);
        cellnextpara1.addElement(p14);

        cellnextpara1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellnextpara1.setBorder(Rectangle.NO_BORDER);
        cellnextpara1.setPaddingTop(-50);
        table2.addCell(cellnextpara1);

        document.add(table2);
        document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
        Font black8 = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, BaseColor.BLACK);
        Chunk text9 = new Chunk("TEST REPORT", black8);
        Paragraph p9 = new Paragraph();
        p9.setAlignment(Element.ALIGN_CENTER);
        p9.add(text9);
        document.add(p9);
        //document.add( Chunk.NEWLINE );
        //generate qrcode
        Bitmap qrbm=generateQrCode();
        ByteArrayOutputStream qrstream = new ByteArrayOutputStream();
        qrbm.compress(Bitmap.CompressFormat.PNG, 100, qrstream);
        Image qrimage = null;
        try {
            qrimage = Image.getInstance(qrstream.toByteArray());
            qrimage.scaleToFit(50,50);

            // image.setAbsolutePosition( 0,180);
            qrimage.setAlignment(Element.ALIGN_RIGHT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.add(qrimage);


        dbHandler = new DBHandler(context);
        dbs = dbHandler.getWritableDatabase();
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String selectQuery = "SELECT  * FROM kiosk_trn_tsoil WHERE soil_gid = "+id;

        Cursor cursor = dbs.rawQuery(selectQuery, null);

        String qry = "select * from core_mst_tfarmer where farmer_code='"+sharedpreferences.getString("fcode", "")+"'";
        Log.e("query1234", qry);
        Cursor cr1 = dbs.rawQuery(qry,null);
        if(cr1.moveToFirst()){
            FarmerName = cr1.getString(3);
            Farmeraddress = cr1.getString(5);
            Farmeraddress2 = cr1.getString(6);
        }

        if(cursor.moveToFirst())
        {

            PdfPTable tbl1 = new PdfPTable(4);

            tbl1.setWidthPercentage(100);
            Font font1 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
            PdfPCell cell1 = new PdfPCell(new Phrase("Issued To:", font1));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(BaseColor.GRAY);
            //cell1.setBackgroundColor(BaseColor.GRAY);
            cell1.setRowspan(3);
            tbl1.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(FarmerName,font1));

            tbl1.addCell(cell1);
            cell1 = new PdfPCell(new Phrase("Sample Description",font1));
            cell1.setBackgroundColor(BaseColor.GRAY);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setRowspan(3);
            tbl1.addCell(cell1);

            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1 = new PdfPCell(new Phrase(cursor.getString(16),font1));

            tbl1.addCell(cell1);
            document.add(tbl1);

            PdfPTable tbl2 = new PdfPTable(4);

            tbl2.setWidthPercentage(100);

            PdfPCell cell2 = new PdfPCell(new Phrase(""));
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell2.setBackgroundColor(BaseColor.GRAY);
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setRowspan(3);
            tbl2.addCell(cell2);
            cell2 = new PdfPCell(new Phrase(Farmeraddress,font1));
            tbl2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Sample Drawn By",font1));
            cell2.setBackgroundColor(BaseColor.GRAY);
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell2.setRowspan(3);
            tbl2.addCell(cell2);

            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell2 = new PdfPCell(new Phrase("CUSTOMER",font1));

            tbl2.addCell(cell2);
            document.add(tbl2);

            PdfPTable tbl3 = new PdfPTable(4);

            tbl3.setWidthPercentage(100);

            PdfPCell cell3 = new PdfPCell(new Phrase(""));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(BaseColor.GRAY);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setRowspan(3);
            tbl3.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(Farmeraddress2,font1));

            tbl3.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Customer's Reference",font1));
            cell3.setBackgroundColor(BaseColor.GRAY);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setRowspan(3);
            tbl3.addCell(cell3);

            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3 = new PdfPCell(new Phrase(cursor.getString(7),font1));

            tbl3.addCell(cell3);
            document.add(tbl3);

            PdfPTable tbl4 = new PdfPTable(4);

            tbl4.setWidthPercentage(100);

            PdfPCell cell4 = new PdfPCell(new Phrase("Report Number",font1));
            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell4.setBackgroundColor(BaseColor.GRAY);
            tbl4.addCell(cell4);
            cell4 = new PdfPCell(new Phrase(cursor.getString(3),font1));

            tbl4.addCell(cell4);
            cell4 = new PdfPCell(new Phrase("Sample Received on",font1));
            cell4.setBackgroundColor(BaseColor.GRAY);
            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl4.addCell(cell4);

            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
           // cell4 = new PdfPCell(new Phrase(cursor.getString(10)));
            String sdates = cursor.getString(10);
            String[] sfindate = sdates.split(" ");
            String sdate1 = sfindate[0];
            String[] sfindate1 = sdate1.split("-");
            sdate2 = sfindate1[2]+"/"+sfindate1[1]+"/"+sfindate1[0];
            cell4 = new PdfPCell(new Phrase(sdate2,font1));

            tbl4.addCell(cell4);
            document.add(tbl4);

            PdfPTable tbl5 = new PdfPTable(4);

            tbl5.setWidthPercentage(100);

            PdfPCell cell5 = new PdfPCell(new Phrase("Report Date",font1));
            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell5.setBackgroundColor(BaseColor.GRAY);
            tbl5.addCell(cell5);
            //cell5 = new PdfPCell(new Phrase(cursor.getString(2)));
            String dates = cursor.getString(2);
            String[] findate = dates.split(" ");
            String date1 = findate[0];
            String[] findate1 = date1.split("-");
            date2 = findate1[2]+"/"+findate1[1]+"/"+findate1[0];
            cell5 = new PdfPCell(new Phrase(date2,font1));

            tbl5.addCell(cell5);
            cell5 = new PdfPCell(new Phrase("Analysis Started on",font1));
            cell5.setBackgroundColor(BaseColor.GRAY);
            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl5.addCell(cell5);

            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            //cell5 = new PdfPCell(new Phrase(cursor.getString(11)));
            String anlysdates = cursor.getString(11);
            String[] afindate = anlysdates.split(" ");
            String adate1 = afindate[0];
            String[] afindate1 = adate1.split("-");
            adate2 = afindate1[2]+"/"+afindate1[1]+"/"+afindate1[0];
            cell5 = new PdfPCell(new Phrase(adate2,font1));

            tbl5.addCell(cell5);
            document.add(tbl5);

            PdfPTable tbl6 = new PdfPTable(4);

            tbl6.setWidthPercentage(100);

            PdfPCell cell6 = new PdfPCell(new Phrase("Unique Lab Report No.",font1));
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell6.setBackgroundColor(BaseColor.GRAY);
            tbl6.addCell(cell6);
            cell6 = new PdfPCell(new Phrase(cursor.getString(8),font1));

            tbl6.addCell(cell6);
            cell6 = new PdfPCell(new Phrase("Analysis Completed on",font1));
            cell6.setBackgroundColor(BaseColor.GRAY);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl6.addCell(cell6);

            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            //cell6 = new PdfPCell(new Phrase(cursor.getString(12)));
            String anlyscdates = cursor.getString(12);
            String[] acfindate = anlyscdates.split(" ");
            String acdate1 = acfindate[0];
            String[] acfindate1 = acdate1.split("-");
            acdate2 = acfindate1[2]+"/"+acfindate1[1]+"/"+acfindate1[0];
            cell6 = new PdfPCell(new Phrase(acdate2,font1));

            tbl6.addCell(cell6);
            document.add(tbl6);

            PdfPTable tbl7 = new PdfPTable(4);

            tbl7.setWidthPercentage(100);

            PdfPCell cell7 = new PdfPCell(new Phrase("Lab ID",font1));
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBackgroundColor(BaseColor.GRAY);
            tbl7.addCell(cell7);
            cell7 = new PdfPCell(new Phrase(cursor.getString(9),font1));

            tbl7.addCell(cell7);
            cell7 = new PdfPCell(new Phrase("Sample ID",font1));
            cell7.setBackgroundColor(BaseColor.GRAY);
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl7.addCell(cell7);

            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7 = new PdfPCell(new Phrase(cursor.getString(5),font1));

            tbl7.addCell(cell7);
            document.add(tbl7);

            PdfPTable tbl8 = new PdfPTable(4);

            tbl8.setWidthPercentage(100);

            PdfPCell cell8 = new PdfPCell(new Phrase("Discipline",font1));
            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8.setBackgroundColor(BaseColor.GRAY);
            tbl8.addCell(cell8);
            cell8 = new PdfPCell(new Phrase("Chemical",font1));

            tbl8.addCell(cell8);
            cell8 = new PdfPCell(new Phrase("Group",font1));
            cell8.setBackgroundColor(BaseColor.GRAY);
            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl8.addCell(cell8);

            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8 = new PdfPCell(new Phrase("Pollution & Environment",font1));

            tbl8.addCell(cell8);
            document.add(tbl8);
            tm = cursor.getString(13);

        }


       document.add( Chunk.NEWLINE );

        Paragraph paragraph = new Paragraph();
        PdfPCell cells = null;


        PdfPTable mainTable = new PdfPTable(2);
        mainTable.setWidthPercentage(105.0f);

        Font font2 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
        // First table
        PdfPCell firstTableCell = new PdfPCell();
        firstTableCell.setBorder(PdfPCell.NO_BORDER);
        firstTableCell.setPaddingRight(-28);
        firstTableCell.setPaddingLeft(15);

        PdfPTable firstTable = new PdfPTable(4);
        firstTable.setWidthPercentage(100.0f);
        firstTable.setWidths(new int[]{1,4,2,2});
        cells = new PdfPCell(new Phrase("S No",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);
        cells = new PdfPCell(new Phrase("Parameter",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);
        cells = new PdfPCell(new Phrase("Unit",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);
        cells = new PdfPCell(new Phrase("Results",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);

        Cursor c= dbs.query("kiosk_trn_tsoilparameter", new String[]{"soilparam_gid","soilparam_soil_gid","soil_parameter","soil_uom","soil_results"
                }, "soilparam_soil_gid" + "=? COLLATE NOCASE",
                new String[]{id}, null, null, null, null);
        int i = 1;
        if(c.moveToFirst()){
          do{
              String group = String.valueOf(i);
              cells = new PdfPCell(new Phrase(group));
              firstTable.addCell(cells);
              String selectQuery3 = "SELECT  * FROM core_mst_tmaster WHERE master_rowid = "+c.getString(2);
              Cursor cursor2 = dbs.rawQuery(selectQuery3, null);
              if(cursor2.moveToNext())
              {
                  Log.e("VVV",""+cursor2.getString(2));
                  Cursor cursor3= dbs.query("core_mst_tmastertranslate", new String[]{"master_name"
                          }, "mastertranslate_master_code" + "=? COLLATE NOCASE",
                          new String[]{cursor2.getString(2)}, null, null, null, null);

                  if(cursor3.moveToNext())
                  {
                      String machine = cursor3.getString(0);
                      cells = new PdfPCell(new Phrase(machine,font2));
                      firstTable.addCell(cells);
                  }

              }
              String selectQuery4 = "SELECT  * FROM core_mst_tmaster WHERE master_rowid = "+c.getString(3);
              Cursor cursor4 = dbs.rawQuery(selectQuery4, null);
              if(cursor4.moveToNext())
              {
                  Log.e("VVV",""+cursor2.getString(2));
                  Cursor cursor3= dbs.query("core_mst_tmastertranslate", new String[]{"master_name"
                          }, "mastertranslate_master_code" + "=? COLLATE NOCASE",
                          new String[]{cursor4.getString(2)}, null, null, null, null);

                  if(cursor3.moveToNext())
                  {
                      String machine = cursor3.getString(0);
                      cells = new PdfPCell(new Phrase(machine,font2));
                      firstTable.addCell(cells);
                  }

              }
              String machine = c.getString(4);
              cells = new PdfPCell(new Phrase(machine,font2));
              firstTable.addCell(cells);
              i++;
          }while(c.moveToNext());
        }

        firstTableCell.addElement(firstTable);
        mainTable.addCell(firstTableCell);

        PdfPCell thirdTableCell = new PdfPCell();
        thirdTableCell.setBorder(PdfPCell.NO_BORDER);
        PdfPTable thirdTable = new PdfPTable(1);
        //thirdTable.setWidthPercentage(90.0f);
        cells = new PdfPCell(new Phrase("Test Method",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        thirdTable.addCell(cells);
        String machine2 = tm;
        cells = new PdfPCell(new Phrase(machine2,font2));
        cells.setVerticalAlignment(Element.ALIGN_MIDDLE);
        thirdTable.addCell(cells);
        thirdTable.setExtendLastRow(true);
        thirdTableCell.addElement(thirdTable);
        mainTable.addCell(thirdTableCell);


        paragraph.add(mainTable);
        document.add(mainTable);


        Drawable d2 = context.getResources().getDrawable(R.drawable.newfooter);
        BitmapDrawable bitDw2 = ((BitmapDrawable) d2);
        Bitmap bmp2 = bitDw2.getBitmap();
        ByteArrayOutputStream stream2= new ByteArrayOutputStream();
        bmp2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
        Image image2 = null;
        try {
            image2 = Image.getInstance(stream2.toByteArray());
            image2.scaleToFit(800,100);

            // image.setAbsolutePosition( 0,180);
            image2.setAlignment(Element.ALIGN_CENTER);
        } catch (IOException e) {
            e.printStackTrace();
        }

        document.add(image2);
        document.add( Chunk.NEWLINE );

        Font black9 = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, BaseColor.BLACK);
        Chunk text10 = new Chunk("Notes:-", black9);
        Paragraph p10 = new Paragraph();
        p10.setAlignment(Element.ALIGN_LEFT);
        p10.add(text10);
       // document.add(p10);


//        document.add(new Paragraph(". The above report is applicable only to the sample received and tested. "));
//        document.add(new Paragraph(". The tested samples will not be retained after 30 days from the date of issue of the test report."));
//        document.add(new Paragraph(". Perishable samples will be destroyed after testing. "));
//        document.add(new Paragraph(". Any request for retesting will not be considered after 30 days from the date of issue of the report. "));
//        document.add(new Paragraph(". Our report shall not be reproduced wholly or in part and cannot be used as evidence in the court of law . "));
//        document.add(new Paragraph(". Our report in full or part shall not be used for any advertising media without our prior written approval. "));

        Font fontwhite = new Font(Font.FontFamily.HELVETICA,15,Font.NORMAL,BaseColor.WHITE);
        PdfPTable tablef = new PdfPTable(1);
        tablef.setWidthPercentage(100);


        PdfPCell cellf1 = new PdfPCell(new Phrase("Note : The above report is applicable only to the sample received and tested. " + "\u2022"+
                " The tested samples will not be retained after 30 days from the date of issue of the test report. " + "\u2022"+
                " Perishable samples will be destroyed after testing. Any request for retesting will not be considered after 30 days from the date of issue of the report. " + "\u2022"+
                " Our report shall not be reproduced wholly or in part and cannot be used as evidence in the court of law . " + "\u2022"+
                " Our report in full or part shall not be used for any advertising media without our prior written approval.",fontwhite));
        cellf1.setBackgroundColor(BaseColor.GRAY);
        cellf1.setPadding(5);
        cellf1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
        tablef.addCell(cellf1);
        document.add(tablef);



        //Step 5: Close the document
        document.close();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();

        StrictMode.setVmPolicy(builder.build());
        Toast.makeText(context, "Created", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        context.startActivity(intent);
//        Intent target = Intent.createChooser(intent, "Open File");
//        try {
//            context.startActivity(target);
//        } catch (ActivityNotFoundException e) {
//            // Instruct the user to install a PDF reader here, or something
//        }
        String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(myFile).toString());

        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_NEW_TASK);

        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", myFile);

        intent.setDataAndType(uri, mimeType);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        context.startActivity(Intent.createChooser(intent, "choseFile"));
    }



    private Bitmap generateQrCode() {
        StringBuilder textToSend = new StringBuilder();
        textToSend.append(qrReportno+",\n"+qrReportDate+",\n"+qrUniLabReportNo+",\n"+qrLabId+",\n"+qrCustRefeerence);
        Bitmap bitmap = null;
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(textToSend.toString(), BarcodeFormat.QR_CODE, 600, 600);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            bitmap = barcodeEncoder.createBitmap(bitMatrix);
            // imageView.setImageBitmap(bitmap);
            //imageView.setVisibility(View.VISIBLE);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private void showDialog(SoilListModel list) {
        final Dialog dialog = new Dialog(context);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_header_dialog);
        dialog.setTitle("Kiosk");
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.90);
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        final TextView txtWith = (TextView) dialog.findViewById(R.id.txt_with);
        final TextView txtWithout = (TextView) dialog.findViewById(R.id.txt_without);
        final Spinner spinnerSignature=(Spinner)dialog.findViewById(R.id.spin_signature);
        Button okButton = (Button) dialog.findViewById(R.id.but_Ok);
        headrValue="With";
        txtWith.setBackground(ContextCompat.getDrawable(context,R.drawable.yellow_round_button));
        txtWithout.setBackground(ContextCompat.getDrawable(context,R.drawable.white_round_button));

        List<String> signatureList=new ArrayList<>();
        signatureList.add(new String("Select"));
        signatureList.add(new String("Manager"));
        signatureList.add(new String("Assistant Manager"));
        signatureList.add(new String("Supervisor"));
        signatureList.add(new String("Employee"));

        SignatureSpinnerAdapter signatureSpinnerAdapter=new SignatureSpinnerAdapter(context,signatureList);
        spinnerSignature.setAdapter(signatureSpinnerAdapter);


        spinnerSignature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                signatureValue=signatureList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        txtWith.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                headrValue="With";
                txtWith.setBackground(ContextCompat.getDrawable(context,R.drawable.yellow_round_button));
                txtWithout.setBackground(ContextCompat.getDrawable(context,R.drawable.white_round_button));
                //  dialog.dismiss();
            }
        });

        txtWithout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headrValue="WithOut";
                txtWith.setBackground(ContextCompat.getDrawable(context,R.drawable.white_round_button));
                txtWithout.setBackground(ContextCompat.getDrawable(context,R.drawable.yellow_round_button));
                //    dialog.dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if(signatureValue.equalsIgnoreCase("Select")){
                    Toast.makeText(context,"Please select signature",Toast.LENGTH_SHORT).show();
                }else {
                    if(headrValue.equalsIgnoreCase("With")){
                        try {
                            createPdf(String.valueOf(list.getInSoilGid()));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                    }else {
                        try {
                            createWithoutHeaderPdf(String.valueOf(list.getInSoilGid()));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                    }
                    dialog.dismiss();
                }



            }
        });


        ImageView dialogButton = (ImageView) dialog.findViewById(R.id.cls);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void createWithoutHeaderPdf(String id) throws FileNotFoundException, DocumentException{

        File pdfFolder = new File(Environment.getExternalStorageDirectory(), "pdfdemo"+ UUID.randomUUID());
        //File pdfFolder = new File(context.getFilesDir(), "pdfdemo");
        if (!pdfFolder.exists()) {
            pdfFolder.mkdirs();
            Log.e("LOG_TAG", "Pdf Directory created"+pdfFolder);
        }

        //Create time stamp
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(date);

        File myFile = new File(pdfFolder +"/"+ timeStamp + ".pdf");

        OutputStream output = new FileOutputStream(myFile);

        //Step 1
        Document document = new Document(PageSize.A4, 36, 36, 36, 36);

        //Step 2
        PdfWriter writer = PdfWriter.getInstance(document, output);

        //Step 3
        document.open();

        Font black8 = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, BaseColor.BLACK);
        Chunk text9 = new Chunk("TEST REPORT:-", black8);
        Paragraph p9 = new Paragraph();
        p9.setAlignment(Element.ALIGN_CENTER);
        p9.add(text9);
        document.add(p9);
        document.add( Chunk.NEWLINE );

        //generate qrcode
        Bitmap qrbm=generateQrCode();
        ByteArrayOutputStream qrstream = new ByteArrayOutputStream();
        qrbm.compress(Bitmap.CompressFormat.PNG, 100, qrstream);
        Image qrimage = null;
        try {
            qrimage = Image.getInstance(qrstream.toByteArray());
            qrimage.scaleToFit(100,100);

            // image.setAbsolutePosition( 0,180);
            qrimage.setAlignment(Element.ALIGN_RIGHT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.add(qrimage);

        dbHandler = new DBHandler(context);
        dbs = dbHandler.getWritableDatabase();
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String selectQuery = "SELECT  * FROM kiosk_trn_tsoil WHERE soil_gid = "+id;

        Cursor cursor = dbs.rawQuery(selectQuery, null);

        String qry = "select * from core_mst_tfarmer where farmer_code='"+sharedpreferences.getString("fcode", "")+"'";
        Log.e("query1234", qry);
        Cursor cr1 = dbs.rawQuery(qry,null);
        if(cr1.moveToFirst()){
            FarmerName = cr1.getString(3);
            Farmeraddress = cr1.getString(5);
            Farmeraddress2 = cr1.getString(6);
        }

        if(cursor.moveToFirst())
        {

            PdfPTable tbl1 = new PdfPTable(4);

            tbl1.setWidthPercentage(100);
            Font font1 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
            PdfPCell cell1 = new PdfPCell(new Phrase("Issued To:", font1));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(BaseColor.GRAY);
            //cell1.setBackgroundColor(BaseColor.GRAY);
            cell1.setRowspan(3);
            tbl1.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(FarmerName,font1));

            tbl1.addCell(cell1);
            cell1 = new PdfPCell(new Phrase("Sample Description",font1));
            cell1.setBackgroundColor(BaseColor.GRAY);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setRowspan(3);
            tbl1.addCell(cell1);

            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1 = new PdfPCell(new Phrase(cursor.getString(16),font1));

            tbl1.addCell(cell1);
            document.add(tbl1);

            PdfPTable tbl2 = new PdfPTable(4);

            tbl2.setWidthPercentage(100);

            PdfPCell cell2 = new PdfPCell(new Phrase(""));
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell2.setBackgroundColor(BaseColor.GRAY);
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setRowspan(3);
            tbl2.addCell(cell2);
            cell2 = new PdfPCell(new Phrase(Farmeraddress,font1));
            tbl2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Sample Drawn By",font1));
            cell2.setBackgroundColor(BaseColor.GRAY);
            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell2.setRowspan(3);
            tbl2.addCell(cell2);

            cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell2 = new PdfPCell(new Phrase("CUSTOMER",font1));

            tbl2.addCell(cell2);
            document.add(tbl2);

            PdfPTable tbl3 = new PdfPTable(4);

            tbl3.setWidthPercentage(100);

            PdfPCell cell3 = new PdfPCell(new Phrase(""));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(BaseColor.GRAY);
            cell3.setBorder(Rectangle.NO_BORDER);
            cell3.setRowspan(3);
            tbl3.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(Farmeraddress2,font1));

            tbl3.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Customer's Reference",font1));
            cell3.setBackgroundColor(BaseColor.GRAY);
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setRowspan(3);
            tbl3.addCell(cell3);

            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3 = new PdfPCell(new Phrase(cursor.getString(7),font1));

            tbl3.addCell(cell3);
            document.add(tbl3);

            PdfPTable tbl4 = new PdfPTable(4);

            tbl4.setWidthPercentage(100);

            PdfPCell cell4 = new PdfPCell(new Phrase("Report Number",font1));
            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell4.setBackgroundColor(BaseColor.GRAY);
            tbl4.addCell(cell4);
            cell4 = new PdfPCell(new Phrase(cursor.getString(3),font1));

            tbl4.addCell(cell4);
            cell4 = new PdfPCell(new Phrase("Sample Received on",font1));
            cell4.setBackgroundColor(BaseColor.GRAY);
            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl4.addCell(cell4);

            cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
            // cell4 = new PdfPCell(new Phrase(cursor.getString(10)));
            String sdates = cursor.getString(10);
            String[] sfindate = sdates.split(" ");
            String sdate1 = sfindate[0];
            String[] sfindate1 = sdate1.split("-");
            sdate2 = sfindate1[2]+"/"+sfindate1[1]+"/"+sfindate1[0];
            cell4 = new PdfPCell(new Phrase(sdate2,font1));

            tbl4.addCell(cell4);
            document.add(tbl4);

            PdfPTable tbl5 = new PdfPTable(4);

            tbl5.setWidthPercentage(100);

            PdfPCell cell5 = new PdfPCell(new Phrase("Report Date",font1));
            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell5.setBackgroundColor(BaseColor.GRAY);
            tbl5.addCell(cell5);
            //cell5 = new PdfPCell(new Phrase(cursor.getString(2)));
            String dates = cursor.getString(2);
            String[] findate = dates.split(" ");
            String date1 = findate[0];
            String[] findate1 = date1.split("-");
            date2 = findate1[2]+"/"+findate1[1]+"/"+findate1[0];
            cell5 = new PdfPCell(new Phrase(date2,font1));

            tbl5.addCell(cell5);
            cell5 = new PdfPCell(new Phrase("Analysis Started on",font1));
            cell5.setBackgroundColor(BaseColor.GRAY);
            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl5.addCell(cell5);

            cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
            //cell5 = new PdfPCell(new Phrase(cursor.getString(11)));
            String anlysdates = cursor.getString(11);
            String[] afindate = anlysdates.split(" ");
            String adate1 = afindate[0];
            String[] afindate1 = adate1.split("-");
            adate2 = afindate1[2]+"/"+afindate1[1]+"/"+afindate1[0];
            cell5 = new PdfPCell(new Phrase(adate2,font1));

            tbl5.addCell(cell5);
            document.add(tbl5);

            PdfPTable tbl6 = new PdfPTable(4);

            tbl6.setWidthPercentage(100);

            PdfPCell cell6 = new PdfPCell(new Phrase("Unique Lab Report No.",font1));
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell6.setBackgroundColor(BaseColor.GRAY);
            tbl6.addCell(cell6);
            cell6 = new PdfPCell(new Phrase(cursor.getString(8),font1));

            tbl6.addCell(cell6);
            cell6 = new PdfPCell(new Phrase("Analysis Completed on",font1));
            cell6.setBackgroundColor(BaseColor.GRAY);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl6.addCell(cell6);

            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            //cell6 = new PdfPCell(new Phrase(cursor.getString(12)));
            String anlyscdates = cursor.getString(12);
            String[] acfindate = anlyscdates.split(" ");
            String acdate1 = acfindate[0];
            String[] acfindate1 = acdate1.split("-");
            acdate2 = acfindate1[2]+"/"+acfindate1[1]+"/"+acfindate1[0];
            cell6 = new PdfPCell(new Phrase(acdate2,font1));

            tbl6.addCell(cell6);
            document.add(tbl6);

            PdfPTable tbl7 = new PdfPTable(4);

            tbl7.setWidthPercentage(100);

            PdfPCell cell7 = new PdfPCell(new Phrase("Lab ID",font1));
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBackgroundColor(BaseColor.GRAY);
            tbl7.addCell(cell7);
            cell7 = new PdfPCell(new Phrase(cursor.getString(9),font1));

            tbl7.addCell(cell7);
            cell7 = new PdfPCell(new Phrase("Sample ID",font1));
            cell7.setBackgroundColor(BaseColor.GRAY);
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl7.addCell(cell7);

            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7 = new PdfPCell(new Phrase(cursor.getString(5),font1));

            tbl7.addCell(cell7);
            document.add(tbl7);

            PdfPTable tbl8 = new PdfPTable(4);

            tbl8.setWidthPercentage(100);

            PdfPCell cell8 = new PdfPCell(new Phrase("Discipline",font1));
            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8.setBackgroundColor(BaseColor.GRAY);
            tbl8.addCell(cell8);
            cell8 = new PdfPCell(new Phrase("Chemical",font1));

            tbl8.addCell(cell8);
            cell8 = new PdfPCell(new Phrase("Group",font1));
            cell8.setBackgroundColor(BaseColor.GRAY);
            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            tbl8.addCell(cell8);

            cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8 = new PdfPCell(new Phrase("Pollution & Environment",font1));

            tbl8.addCell(cell8);
            document.add(tbl8);
            tm = cursor.getString(13);

        }


        document.add( Chunk.NEWLINE );

        Paragraph paragraph = new Paragraph();
        PdfPCell cells = null;


        PdfPTable mainTable = new PdfPTable(2);
        mainTable.setWidthPercentage(105.0f);

        Font font2 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.BLACK);
        // First table
        PdfPCell firstTableCell = new PdfPCell();
        firstTableCell.setBorder(PdfPCell.NO_BORDER);
        firstTableCell.setPaddingRight(-28);
        firstTableCell.setPaddingLeft(15);

        PdfPTable firstTable = new PdfPTable(4);
        firstTable.setWidthPercentage(100.0f);
        firstTable.setWidths(new int[]{1,4,2,2});
        cells = new PdfPCell(new Phrase("S No",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);
        cells = new PdfPCell(new Phrase("Parameter",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);
        cells = new PdfPCell(new Phrase("Unit",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);
        cells = new PdfPCell(new Phrase("Results",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        firstTable.addCell(cells);

        Cursor c= dbs.query("kiosk_trn_tsoilparameter", new String[]{"soilparam_gid","soilparam_soil_gid","soil_parameter","soil_uom","soil_results"
                }, "soilparam_soil_gid" + "=? COLLATE NOCASE",
                new String[]{id}, null, null, null, null);
        int i = 1;
        if(c.moveToFirst()){
            do{
                String group = String.valueOf(i);
                cells = new PdfPCell(new Phrase(group));
                firstTable.addCell(cells);
                String selectQuery3 = "SELECT  * FROM core_mst_tmaster WHERE master_rowid = "+c.getString(2);
                Cursor cursor2 = dbs.rawQuery(selectQuery3, null);
                if(cursor2.moveToNext())
                {
                    Log.e("VVV",""+cursor2.getString(2));
                    Cursor cursor3= dbs.query("core_mst_tmastertranslate", new String[]{"master_name"
                            }, "mastertranslate_master_code" + "=? COLLATE NOCASE",
                            new String[]{cursor2.getString(2)}, null, null, null, null);

                    if(cursor3.moveToNext())
                    {
                        String machine = cursor3.getString(0);
                        cells = new PdfPCell(new Phrase(machine,font2));
                        firstTable.addCell(cells);
                    }

                }
                String selectQuery4 = "SELECT  * FROM core_mst_tmaster WHERE master_rowid = "+c.getString(3);
                Cursor cursor4 = dbs.rawQuery(selectQuery4, null);
                if(cursor4.moveToNext())
                {
                    Log.e("VVV",""+cursor2.getString(2));
                    Cursor cursor3= dbs.query("core_mst_tmastertranslate", new String[]{"master_name"
                            }, "mastertranslate_master_code" + "=? COLLATE NOCASE",
                            new String[]{cursor4.getString(2)}, null, null, null, null);

                    if(cursor3.moveToNext())
                    {
                        String machine = cursor3.getString(0);
                        cells = new PdfPCell(new Phrase(machine,font2));
                        firstTable.addCell(cells);
                    }

                }
                String machine = c.getString(4);
                cells = new PdfPCell(new Phrase(machine,font2));
                firstTable.addCell(cells);
                i++;
            }while(c.moveToNext());
        }

        firstTableCell.addElement(firstTable);
        mainTable.addCell(firstTableCell);

        PdfPCell thirdTableCell = new PdfPCell();
        thirdTableCell.setBorder(PdfPCell.NO_BORDER);
        PdfPTable thirdTable = new PdfPTable(1);
        //thirdTable.setWidthPercentage(90.0f);
        cells = new PdfPCell(new Phrase("Test Method",font2));
        cells.setBackgroundColor(BaseColor.GRAY);
        thirdTable.addCell(cells);
        String machine2 = tm;
        cells = new PdfPCell(new Phrase(machine2,font2));
        cells.setVerticalAlignment(Element.ALIGN_MIDDLE);
        thirdTable.addCell(cells);
        thirdTable.setExtendLastRow(true);
        thirdTableCell.addElement(thirdTable);
        mainTable.addCell(thirdTableCell);


        paragraph.add(mainTable);
        document.add(mainTable);


        Drawable d2 = context.getResources().getDrawable(R.drawable.newfooter);
        BitmapDrawable bitDw2 = ((BitmapDrawable) d2);
        Bitmap bmp2 = bitDw2.getBitmap();
        ByteArrayOutputStream stream2= new ByteArrayOutputStream();
        bmp2.compress(Bitmap.CompressFormat.PNG, 100, stream2);
        Image image2 = null;
        try {
            image2 = Image.getInstance(stream2.toByteArray());
            image2.scaleToFit(800,100);

            // image.setAbsolutePosition( 0,180);
            image2.setAlignment(Element.ALIGN_CENTER);
        } catch (IOException e) {
            e.printStackTrace();
        }

        document.add(image2);
        document.add( Chunk.NEWLINE );

        Font black9 = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD, BaseColor.BLACK);
        Chunk text10 = new Chunk("Notes:-", black9);
        Paragraph p10 = new Paragraph();
        p10.setAlignment(Element.ALIGN_LEFT);
        p10.add(text10);
        // document.add(p10);


//        document.add(new Paragraph(". The above report is applicable only to the sample received and tested. "));
//        document.add(new Paragraph(". The tested samples will not be retained after 30 days from the date of issue of the test report."));
//        document.add(new Paragraph(". Perishable samples will be destroyed after testing. "));
//        document.add(new Paragraph(". Any request for retesting will not be considered after 30 days from the date of issue of the report. "));
//        document.add(new Paragraph(". Our report shall not be reproduced wholly or in part and cannot be used as evidence in the court of law . "));
//        document.add(new Paragraph(". Our report in full or part shall not be used for any advertising media without our prior written approval. "));

        Font fontwhite = new Font(Font.FontFamily.HELVETICA,15,Font.NORMAL,BaseColor.WHITE);
        PdfPTable tablef = new PdfPTable(1);
        tablef.setWidthPercentage(100);


        PdfPCell cellf1 = new PdfPCell(new Phrase("Note : The above report is applicable only to the sample received and tested. " + "\u2022"+
                " The tested samples will not be retained after 30 days from the date of issue of the test report. " + "\u2022"+
                " Perishable samples will be destroyed after testing. Any request for retesting will not be considered after 30 days from the date of issue of the report. " + "\u2022"+
                " Our report shall not be reproduced wholly or in part and cannot be used as evidence in the court of law . " + "\u2022"+
                " Our report in full or part shall not be used for any advertising media without our prior written approval.",fontwhite));
        cellf1.setBackgroundColor(BaseColor.GRAY);
        cellf1.setPadding(5);
        cellf1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
        tablef.addCell(cellf1);
        document.add(tablef);

//        document.add(new Paragraph(". The above report is applicable only to the sample received and tested. "));
//        document.add(new Paragraph(". The tested samples will not be retained after 30 days from the date of issue of the test report."));
//        document.add(new Paragraph(". Perishable samples will be destroyed after testing. "));
//        document.add(new Paragraph(". Any request for retesting will not be considered after 30 days from the date of issue of the report. "));
//        document.add(new Paragraph(". Our report shall not be reproduced wholly or in part and cannot be used as evidence in the court of law . "));
//        document.add(new Paragraph(". Our report in full or part shall not be used for any advertising media without our prior written approval. "));




        //Step 5: Close the document
        document.close();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();

        StrictMode.setVmPolicy(builder.build());
        Toast.makeText(context, "Created", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
//
//        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        context.startActivity(intent);
//        Intent target = Intent.createChooser(intent, "Open File");
//        try {
//            context.startActivity(target);
//        } catch (ActivityNotFoundException e) {
//            // Instruct the user to install a PDF reader here, or something
//        }
        String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(myFile).toString());

        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_NEW_TASK);

        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", myFile);

        intent.setDataAndType(uri, mimeType);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        context.startActivity(Intent.createChooser(intent, "choseFile"));
    }
}
