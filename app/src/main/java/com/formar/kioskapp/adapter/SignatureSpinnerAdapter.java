package com.formar.kioskapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.formar.kioskapp.R;

import java.util.ArrayList;
import java.util.List;

public class SignatureSpinnerAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    List<String> arrayList = new ArrayList<>();

    public SignatureSpinnerAdapter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList =arrayList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView spinnerText = (TextView) view.findViewById(R.id.spinnerText);
        spinnerText.setText(arrayList.get(i));
        return view;
    }}
