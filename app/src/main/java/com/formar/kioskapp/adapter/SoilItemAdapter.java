package com.formar.kioskapp.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.formar.kioskapp.R;
import com.formar.kioskapp.model.SoilModel;

import java.util.ArrayList;
import java.util.List;

public class SoilItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    List<SoilModel> data=new ArrayList<>();
    Typeface typefaceRegular, typefaceBold;

    public SoilItemAdapter(Context context, List<SoilModel> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.soil_item, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final SoilModel list=data.get(position);

        MyHolder myHolder = (MyHolder) holder;

        myHolder.textview_soilparam.setText("  "+list.getOutSoilParameter());
        myHolder.textview_sNo.setText("   "+list.getOutSoilId());
        myHolder.textview_uom.setText("  "+list.getOutUOM());
        myHolder.textview_result.setText("  "+list.getOutSoilResult());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView textview_sNo;
        TextView textview_soilparam;
        TextView textview_uom;
        TextView textview_result;

        public MyHolder(View itemView) {
            super(itemView);
            textview_sNo=(TextView)itemView.findViewById(R.id.sltxt);
            textview_soilparam=(TextView)itemView.findViewById(R.id.soilparamatxt);
            textview_uom=(TextView)itemView.findViewById(R.id.Uomtxt);
            textview_result= (TextView)itemView.findViewById(R.id.resulttxt);
           }
    }
}
