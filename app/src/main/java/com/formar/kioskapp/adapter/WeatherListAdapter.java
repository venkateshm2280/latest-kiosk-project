package com.formar.kioskapp.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.formar.kioskapp.R;
import com.formar.kioskapp.model.WeatherListModel;

import java.util.ArrayList;
import java.util.List;

public class WeatherListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    List<WeatherListModel> data=new ArrayList<>();
    Typeface typefaceRegular, typefaceBold;

    public WeatherListAdapter(Context context, List<WeatherListModel> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.weatheritem, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final WeatherListModel list=data.get(position);

        MyHolder myHolder = (MyHolder) holder;

        myHolder.textview_date.setText(list.getDate());
        myHolder.textview_tmax.setText(String.valueOf(list.getTmax()));
        myHolder.textview_tmin.setText(String.valueOf(list.getTmin()));
        myHolder.textview_rh1.setText(String.valueOf(list.getRh1()));
        myHolder.textview_rh2.setText(String.valueOf(list.getRh2()));
        myHolder.textview_rh_avg.setText(String.valueOf(list.getRhAvg()));
        myHolder.textview_ws1.setText(String.valueOf(list.getWs1()));
        myHolder.raintxt.setText(String.valueOf(list.getRf()));
        myHolder.textview_ws2.setText(String.valueOf(list.getWs2()));
        myHolder.textview_ws_avg.setText(String.valueOf(list.getWsAvg()));
       // myHolder.textview_rf.setText(String.valueOf(list.getRf()));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView textview_date;
        TextView textview_tmax;
        TextView textview_tmin;
        TextView textview_rh1;
        TextView textview_rh2;
        TextView textview_rh_avg;
        TextView textview_ws1;
        TextView textview_ws2;
        TextView raintxt;
        TextView textview_ws_avg;
        TextView textview_rf;
        AppCompatButton viewBtn;
        public MyHolder(View itemView) {
            super(itemView);
            textview_date=(TextView)itemView.findViewById(R.id.wdate);
            raintxt=(TextView)itemView.findViewById(R.id.raintxt);
            textview_tmax=(TextView)itemView.findViewById(R.id.max_temptxt);
            textview_tmin=(TextView)itemView.findViewById(R.id.min_temptxt);
            textview_rh1= (TextView)itemView.findViewById(R.id.rh1txt);
            textview_rh2= (TextView)itemView.findViewById(R.id.rh2txt);
            textview_rh_avg= (TextView)itemView.findViewById(R.id.avg_humtxt);
            textview_ws1= (TextView)itemView.findViewById(R.id.winspeed_humtxt);
            textview_ws2= (TextView)itemView.findViewById(R.id.ws2txt);
            textview_ws_avg= (TextView)itemView.findViewById(R.id.ws_avgtxt);
          //  textview_rf= (TextView)itemView.findViewById(R.id.rftxt);
           }
    }
}
