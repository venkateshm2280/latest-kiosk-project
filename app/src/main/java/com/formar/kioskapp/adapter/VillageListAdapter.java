package com.formar.kioskapp.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.formar.kioskapp.R;
import com.formar.kioskapp.model.VillageModel;

import java.util.ArrayList;
import java.util.List;

public class VillageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    List<VillageModel> data=new ArrayList<>();
    Typeface typefaceRegular, typefaceBold;
    VillageItemclick lister;
    public interface VillageItemclick{
       void setItemClick(int pos);

    }
    public VillageListAdapter(Context context, List<VillageModel> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.village_list_item, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final VillageModel list=data.get(position);

        MyHolder myHolder = (MyHolder) holder;

        myHolder.textview_title.setText(list.getOutMasterDescription());

        myHolder.textview_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lister.setItemClick(position);
            }
        });
    }
    public void setCallback(VillageItemclick lnr){
        this.lister=lnr;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView textview_title;

        public MyHolder(View itemView) {
            super(itemView);
            textview_title=(TextView)itemView.findViewById(R.id.title);

           }
    }
}
