package com.formar.kioskapp.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.codesgood.views.JustifiedTextView;
import com.formar.kioskapp.R;
import com.formar.kioskapp.model.CropModel;

import java.util.ArrayList;
import java.util.List;

public class CropListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    List<CropModel> data=new ArrayList<>();
    Typeface typefaceRegular, typefaceBold;

    public CropListAdapter(Context context, List<CropModel> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.crop_advisory_item, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        CropModel list=data.get(position);
        MyHolder myHolder = (MyHolder) holder;
        myHolder.textview_cropname.setText(list.getCropName());
        myHolder.textview_farmername.setText(list.getFormarName());
        myHolder.textview_desc.setText(list.getMessage_ta());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView textview_cropname;
        TextView textview_farmername;
        JustifiedTextView textview_desc;

        public MyHolder(View itemView) {
            super(itemView);
            textview_cropname=(TextView)itemView.findViewById(R.id.cropnametxt);
            textview_farmername=(TextView)itemView.findViewById(R.id.farmernametxt);
            textview_desc=itemView.findViewById(R.id.desctxt);

           }
    }
}
