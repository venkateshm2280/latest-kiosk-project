package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.formar.kioskapp.R;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class WeatherSelectActivity extends BaseActivity {

    AppCompatImageView logout,home,backBtn;
    AppCompatImageView language_icon;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    AppCompatButton weatherBtn,cropAdvBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_weather_select);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String loginType= sharedpreferences.getString("login","");
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        language_icon=findViewById(R.id.language_icon);
        cropAdvBtn=findViewById(R.id.cropbtn);
        weatherBtn=findViewById(R.id.weatherbtn);
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        weatherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable() && loginType.equals("2")) {
                    Intent i = new Intent(WeatherSelectActivity.this, WeatherActivity.class);
                    i.putExtra("viewType","1");
                    startActivity(i);
                } else {
                    Intent i = new Intent(WeatherSelectActivity.this, WeatherActivity.class);
                    i.putExtra("viewType","1");
                    startActivity(i);
                }
            }
        });cropAdvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable() && loginType.equals("2")) {
                    Intent i = new Intent(WeatherSelectActivity.this, WeatherActivity.class);
                    i.putExtra("viewType","2");
                    startActivity(i);
                } else {
                    Intent i = new Intent(WeatherSelectActivity.this, WeatherActivity.class);
                    i.putExtra("viewType","2");
                    startActivity(i);
                }
            }
        });
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(WeatherSelectActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(WeatherSelectActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(WeatherSelectActivity.this);
                } else {
                    setLogout(WeatherSelectActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setHomepage(WeatherSelectActivity.this);
                }else {
                    setHomepage(WeatherSelectActivity.this);
                }
            }
        });
    }
    public Boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}