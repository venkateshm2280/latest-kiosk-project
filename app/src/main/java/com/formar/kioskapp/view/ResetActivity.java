package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONObject;

public class ResetActivity extends BaseActivity {

    AppCompatEditText userIdtxt,passtxt,retypetxt;
    AppCompatButton subbtn;
    String userId="";
    AppCompatImageView backBtn;
    AppCompatImageView language_icon;
    AppCompatTextView resetTitle,userIdtitle,passTitle,retypetitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_reset);
        userId=getIntent().getStringExtra("userId");
        userIdtxt=findViewById(R.id.userIdtxt);
        passtxt=findViewById(R.id.passtxt);
        retypetxt=findViewById(R.id.retypetxt);
        subbtn=findViewById(R.id.confirm_button);
        backBtn=findViewById(R.id.backBtn);
        language_icon=findViewById(R.id.language_icon);

        resetTitle=findViewById(R.id.reset_title);
        userIdtitle=findViewById(R.id.userid_title);
        passTitle=findViewById(R.id.password_title);
        retypetitle=findViewById(R.id.retype_title);

        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(ResetActivity.this,"ta",language_icon,1);
                }else{
                    Constants.setLanguage(ResetActivity.this,"en",language_icon,1);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
            //subbtn.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );

        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
           /* subbtn.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,15 );
            resetTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP ,18 );
            userIdtitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
            passTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
            retypetitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );*/
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(!userId.isEmpty()){
            userIdtxt.setText(userId);
        }
        subbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userIdtxt.getText().toString().isEmpty()){
                    Toast.makeText(ResetActivity.this, "Enter User Id details", Toast.LENGTH_SHORT).show();
                }else if(passtxt.getText().toString().isEmpty()){
                    Toast.makeText(ResetActivity.this, "Enter password details", Toast.LENGTH_SHORT).show();
                }else if(retypetxt.getText().toString().isEmpty()){
                    Toast.makeText(ResetActivity.this, "Retype password", Toast.LENGTH_SHORT).show();
                }else if(!retypetxt.getText().toString().equals(passtxt.getText().toString())){
                    Toast.makeText(ResetActivity.this, "Password mismatch", Toast.LENGTH_SHORT).show();
                }else{
                    ResetPassword();
                }
            }
        });
    }
    private void ResetPassword() {
        String url= Constants.Url+"/api/kiosk_login/kioskresetpass?In_user_code="+userIdtxt.getText().toString()+"&In_user_pwd="+passtxt.getText().toString()+"&instance=ta";
        VolleyUtils.getVolleyResult(ResetActivity.this, Request.Method.GET, url, true, null, false, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("onResponse",response.toString());
                JSONObject sys  = null;
                try {
                    sys = response.getJSONObject("context");
                    String result=sys.getString("in_Reponse");
                    Toast.makeText(ResetActivity.this, result, Toast.LENGTH_SHORT).show();
                    finish();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}