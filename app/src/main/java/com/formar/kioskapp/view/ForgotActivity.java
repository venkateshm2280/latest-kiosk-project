package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONObject;

public class ForgotActivity extends BaseActivity {

    AppCompatEditText userIdTxt,otptxt;
    AppCompatButton Submit,verify;
    LinearLayout otpContainer;
    String Otpstring="";
    AppCompatImageView backBtn;
    AppCompatImageView language_icon;
    public boolean isLanguageChanged=false;
    AppCompatTextView forgotTitle,userIdtitle,OtpTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_forgot);
        initViews();
    }

    private void initViews() {
        userIdTxt=findViewById(R.id.userIdtxt);
        otptxt=findViewById(R.id.otptxt);
        Submit=findViewById(R.id.submit_button);
        verify=findViewById(R.id.confirm_button);
        otpContainer=findViewById(R.id.otpContainer);
        backBtn=findViewById(R.id.backBtn);
        language_icon=findViewById(R.id.language_icon);
        forgotTitle=findViewById(R.id.forgot_title);
        userIdtitle=findViewById(R.id.userid_title);
        OtpTitle=findViewById(R.id.Otp_title);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(ForgotActivity.this,"ta",language_icon,1);
                }else{
                    Constants.setLanguage(ForgotActivity.this,"en",language_icon,1);
                }
                isLanguageChanged=true;
              /*  finish();
                Intent intent = getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
         //   Submit.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
          //  verify.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );

        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
           /* Submit.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,15 );
            verify.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,15 );
            forgotTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP ,18 );
            userIdtitle.setTextSize(TypedValue.COMPLEX_UNIT_SP ,14 );
            OtpTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP ,14 );*/
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userIdTxt.getText().toString().isEmpty()){
                    Toast.makeText(ForgotActivity.this, "Enter UserId", Toast.LENGTH_SHORT).show();
                }else{
                    getOtp();
                }
            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(otptxt.getText().toString().isEmpty()){
                    Toast.makeText(ForgotActivity.this, "Enter Otp Code", Toast.LENGTH_SHORT).show();
                }else{
                    if(Otpstring.equals(otptxt.getText().toString())){
                        Intent newin=new Intent(ForgotActivity.this,ResetActivity.class);
                        newin.putExtra("userId",userIdTxt.getText().toString());
                        startActivity(newin);
                        finish();
                    }else{
                        Toast.makeText(ForgotActivity.this, "Enter Valid otp", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void getOtp() {
        String url= Constants.Url+"/api/kiosk_login/kioskforgotpass?In_user_code="+userIdTxt.getText().toString()+"&instance=ta";
        VolleyUtils.getVolleyResult(ForgotActivity.this, Request.Method.GET, url, true, null, false, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
                otpContainer.setVisibility(View.GONE);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("onResponse",response.toString());
                JSONObject sys  = null;
                try {
                    sys = response.getJSONObject("context");
                    String resul=sys.getString("in_Reponse");
                    if(resul.equals("Sucess")){
                        Otpstring=sys.getString("in_otp");
                        otpContainer.setVisibility(View.VISIBLE);
                        verify.setVisibility(View.VISIBLE);
                    }else{
                        otpContainer.setVisibility(View.GONE);
                        verify.setVisibility(View.GONE);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                    otpContainer.setVisibility(View.GONE);
                    verify.setVisibility(View.GONE);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        if(isLanguageChanged){
            Intent intent = new Intent();
            intent.putExtra("ischanged", "1");
            setResult(-1, intent);
        }else {
           // super.onBackPressed();
            Intent intent = new Intent();
            intent.putExtra("ischanged", "0");
            setResult(-1, intent);
        }
        finish();
    }


}