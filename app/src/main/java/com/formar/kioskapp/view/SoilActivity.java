package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.codesgood.views.JustifiedTextView;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.adapter.SoilItemAdapter;
import com.formar.kioskapp.model.SoilModel;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class SoilActivity extends BaseActivity {
    AppCompatImageView logout,home;
    AppCompatEditText farmerCodetxt,fnametxt,transIdtxt,collectdatatxt,statustxt,rejecttxt,sampletxt,drawntxt,referencetxt,
    reporttxt,labidtxt,sReceivetxt,analysiststarttxt,analysisendtxt,testtxt;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public String FarmerCode="";
    public String FarmerName="";
    public String soilgid="";
    public String tran_id="";
    ArrayList<SoilModel> soilModelArrayList=new ArrayList<>();
    RecyclerView formerCodeList;
    SoilItemAdapter adapter;
    AppCompatImageView language_icon,print_icon,shareImg;
    String File_Url="http://www.africau.edu/images/default/sample.pdf";
    JustifiedTextView crop;
    private AppCompatImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_soil_2);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        FarmerCode=sharedpreferences.getString("fcode","");
        FarmerName=sharedpreferences.getString("farmer_name","");
        soilgid=getIntent().getStringExtra("soilGid");
        tran_id=getIntent().getStringExtra("tran_id");
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        formerCodeList=findViewById(R.id.formerCodeList);
        print_icon=findViewById(R.id.print);
        shareImg=findViewById(R.id.shareimg);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLogout(SoilActivity.this);
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setHomepage(SoilActivity.this);
            }
        });
        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showVillageDialog();
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        adapter=new SoilItemAdapter(SoilActivity.this,soilModelArrayList);
        formerCodeList.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(SoilActivity.this,RecyclerView.VERTICAL,false);
        formerCodeList.setLayoutManager(mLayoutManager);
        initviews();
    }
    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void showVillageDialog() {
        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        View customView = inflater.inflate(R.layout.mailpopup_item, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(SoilActivity.this);
        builder.setView(customView);
        builder.setTitle("Send Mail..");
        builder.setCancelable(true);
        AppCompatEditText mailtxt=customView.findViewById(R.id.mailtxt);
        AppCompatButton submitxt=customView.findViewById(R.id.submit_button);
        AlertDialog alertDialog = builder.show();

        submitxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mailtxt.getText().toString().isEmpty()){
                    Toast.makeText(context, "Enter valid emailid!..", Toast.LENGTH_SHORT).show();
                }else if(!isValidEmail(mailtxt.getText().toString())){
                    Toast.makeText(context, "Enter valid emailid!..", Toast.LENGTH_SHORT).show();
                }else {
                    sendEmail(mailtxt.getText().toString());
                    alertDialog.cancel();
                }
            }
        });
    }
    public void  sendEmail(String email){
        String url=Constants.Url+"/api/Kiosk_Soil_Card/kioskEmailsent?org=kiosk&locn=ta&user=admin&lang=en_us&email="+email+"&In_farmer_code="+FarmerCode+"&soil_gid="+soilgid+"&In_Tran_Id="+tran_id+"&In_user_code="+FarmerName;
        VolleyUtils.getVolleyResult(SoilActivity.this, Request.Method.GET, url, true, null, false, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("sendEmail",response.toString());
                Toast.makeText(SoilActivity.this, "Mail sent successfully!..", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void initviews() {

        farmerCodetxt=findViewById(R.id.farmerCodetxt);
        fnametxt=findViewById(R.id.fnametxt);
        transIdtxt=findViewById(R.id.transIdtxt);
        collectdatatxt=findViewById(R.id.collectdatatxt);
        statustxt=findViewById(R.id.statustxt);
        rejecttxt=findViewById(R.id.rejecttxt);
        sampletxt=findViewById(R.id.sampletxt);
        drawntxt=findViewById(R.id.drawntxt);
        referencetxt=findViewById(R.id.referencetxt);
        reporttxt=findViewById(R.id.reporttxt);
        labidtxt=findViewById(R.id.labidtxt);
        sReceivetxt=findViewById(R.id.sReceivetxt);
        analysiststarttxt=findViewById(R.id.analysiststarttxt);
        analysisendtxt=findViewById(R.id.analysisendtxt);
        testtxt=findViewById(R.id.testtxt);
        crop=findViewById(R.id.crop);
        getSoilCardDetails();
        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(SoilActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(SoilActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        print_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (isConnectingToInternet())
                    new DownloadTask(SoilActivity.this, File_Url);*/
                getPrintingDetails();


            }
        });
    }

    private void getPrintingDetails() {
        String url=Constants.Url+"/api/Print/apiSoilCardPrint?org=test&locn=TA&user=admin&lang=en_US&soil_gid="+soilgid+"&In_Tran_Id="+tran_id+"&In_farmer_code="+FarmerCode;
        VolleyUtils.getVolleyResult(SoilActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getPrintingDetails",response.toString());
                try {
                    String path=response.getString("path");
                    if(path!=null && !path.isEmpty()) {
                        openWebPage(path);
                    }else{
                        Toast.makeText(SoilActivity.this, "Soil Report Failed!..", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public void openWebPage(String url) {
        Log.e("openWebPage",url);
        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void getSoilCardDetails() {
       // http://169.38.82.131:751/api/Kiosk_Soil_Card/SoilCardListView?org=flexi&locn=TA&user=kiosk&lang=en_US&soil_gid=1
        String url= Constants.Url+"/api/Kiosk_Soil_Card/SoilCardListView?org=flexi&locn=TA&user=kiosk&lang=en_US&soil_gid="+soilgid;
        VolleyUtils.getVolleyResult(SoilActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getSoilCardDetails",response.toString());
                JSONObject sys  = null;
                JSONArray arry  = null;

                try {
                    if(response!=null) {
                       sys=response.getJSONObject("header");
                        arry=response.getJSONArray("detail");
                        farmerCodetxt.setText( sys.getString("out_farmer_code"));
                        fnametxt.setText( sys.getString("out_farmer_name"));
                        transIdtxt.setText( sys.getString("out_Tran_Id"));
                        collectdatatxt.setText( sys.getString("out_collection_date"));
                        statustxt.setText( sys.getString("out_sample_status"));
                        rejecttxt.setText( sys.getString("out_reject_reason"));
                        sampletxt.setText( sys.getString("out_sample_Id"));
                        drawntxt.setText( sys.getString("out_sample_drawnby"));
                        referencetxt.setText( sys.getString("out_customer_ref"));
                        reporttxt.setText( sys.getString("out_Lab_reportno"));
                        if(sys.getString("out_Lab_Id")!=null){
                            labidtxt.setText( sys.getString("out_Lab_Id"));
                        }
                        sReceivetxt.setText( sys.getString("out_sample_receiveon"));
                        analysiststarttxt.setText( sys.getString("out_Analysis_starton"));
                        analysisendtxt.setText( sys.getString("out_Analysis_completeon"));
                        testtxt.setText( sys.getString("out_test_method"));
                        crop.setText( sys.getString("out_crop_recommendation"));
                        if(arry.length()>0){
                            for (int k=0;k<arry.length();k++){
                                JSONObject object=arry.getJSONObject(k);
                                soilModelArrayList.add(new SoilModel(object.getInt("in_soilparam_rowid"),object.getString("out_soil_Id"),object.getString("in_soil_Parameter_desc"),object.getString("in_soil_uom_desc"),object.getString("in_soil_results")));
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    Toast.makeText(SoilActivity.this, "Please Try again Later", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }
}