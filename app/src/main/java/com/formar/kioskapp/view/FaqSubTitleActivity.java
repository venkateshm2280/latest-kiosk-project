package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.model.VideosTitleItems;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class FaqSubTitleActivity extends BaseActivity {

    AppCompatImageView logout,home;
    List<VideosTitleItems> titleItems=new ArrayList<>();
    String catId="";
    String catname="";
    String lan;
    AppCompatTextView title;
    LinearLayout dynamicviewcontainer;
    AppCompatImageView backBtn;
    private LinearLayout.LayoutParams llLP=null;
    AppCompatImageView language_icon;
    AppCompatEditText searchTxt;
    AppCompatImageView searchBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_faq_subtitle);
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        title=findViewById(R.id.title);
        dynamicviewcontainer=findViewById(R.id.dynamicviewcontainer);
        catId=getIntent().getStringExtra("catIds");
        catname=getIntent().getStringExtra("catname");
        title.setText(catname);
        llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                LinearLayout.LayoutParams.MATCH_PARENT,
                //android:layout_height='wrap_content'
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llLP.setMargins(0, Constants.dpToPx(FaqSubTitleActivity.this,20),0,0);
        llLP.gravity= Gravity.CENTER;
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    setLogout(FaqSubTitleActivity.this);
                } else {
                    setLogout(FaqSubTitleActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    setHomepage(FaqSubTitleActivity.this);
                }else {
                    setHomepage(FaqSubTitleActivity.this);
                }
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    finish();
                }else {
                    finish();
                }
            }
        });
        if (isNetworkAvailable()) {
            getSubCategoryList();
        } else {
            getOfflineSubCategoryList();
        }

        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(FaqSubTitleActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(FaqSubTitleActivity.this,"en",language_icon,2);
                }
            }
        });
         lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        searchTxt=findViewById(R.id.searchTxt);
        searchBtn=findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!searchTxt.getText().toString().isEmpty()){
                    if(searchTxt.length()>2) {
                        if(isNetworkAvailable()) {
                            getSearchResults(searchTxt.getText().toString());
                        } else {
                            getOfflineSearchResults(searchTxt.getText().toString());
                        }
                    }
                }else{
                    Toast.makeText(FaqSubTitleActivity.this, "Kindly type keyword", Toast.LENGTH_SHORT).show();
                }
            }
        });
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==0){
                    if(isNetworkAvailable()) {
                        getSubCategoryList();
                    } else {
                        getOfflineSubCategoryList();
                    }

                    //getSearchResults(editable.toString());
                }
            }
        });
    }

    private void getOfflineSearchResults(String key){
        dynamicviewcontainer.removeAllViews();
        titleItems.clear();
        SQLiteDatabase db = getApplicationContext().
                openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kisok_trn_tfaq where faq_category = ?",
                new String[]{catId});
        Log.e("type",key);
        if(cr.getCount() == 0) {
            Toast.makeText(FaqSubTitleActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
        }
        while(cr.moveToNext()){
            if(cr.getCount()>0){
                Log.e("type",key);
                Log.e("type",cr.getString(5));
                if (key.equalsIgnoreCase(cr.getString(5))) {
                    titleItems.add(new VideosTitleItems(cr.getString(3),"#0069D9",cr.getString(0)));
                }
                bindViews();
            } else {
                dynamicviewcontainer.removeAllViews();
               // Toast.makeText(FaqSubTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                dynamicviewcontainer.addView(child);
            }
        }
    }
    private void getSearchResults(String key) {
        String url= Constants.Url+"/api/Kiosk_FAQS/FAQSQuestion?org=flexi&locn=TA&user=kiosk&lang=en_US"+"&In_faqscat_Id="+catId+"&keyword="+key;
        dynamicviewcontainer.removeAllViews();
        VolleyUtils.getVolleyResult(FaqSubTitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getCategoryList",response.toString());
                JSONArray array  = null;
                titleItems.clear();
                try {
                    array=response.getJSONArray("questions");
                    if(array.length()>0){
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            titleItems.add(new VideosTitleItems(object.getString("out_faq_question"),"#0069D9",object.getString("out_faqscategory_Id")));
                        }
                        bindViews();
                    }else{
                        dynamicviewcontainer.removeAllViews();
                        Toast.makeText(FaqSubTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public Boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void getOfflineSubCategoryList () {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext().
                openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        //Log.e("result", catname);
       // String query = "SELECT * FROM kisok_trn_tfaq WHERE faq_category = " +catId;
        // Cursor cursor = db.rawQuery(query,null);
        Cursor cr = db.rawQuery("select * from kisok_trn_tfaq where faq_category = ?",
                new String[]{catId});
        if (cr.getCount() == 0) {
            Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_LONG).show();
           // Log.e("Data",cr.getString(3));
        }

        while (cr.moveToNext()) {
            if(cr.getCount()>0){

                Log.e("res",lan);
                Log.e("res",cr.getString(3));
                if (lan.equals("en_US")) {
                    titleItems.add(new VideosTitleItems(cr.getString(3),"#0069D9",cr.getString(0)));
                } else {
                    titleItems.add(new VideosTitleItems(cr.getString(6),"#0069D9",cr.getString(0)));
                }

                bindViews();
            }else{

                dynamicviewcontainer.removeAllViews();
               // Toast.makeText(FaqSubTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                dynamicviewcontainer.addView(child);
            }
        }
        }


    private void getSubCategoryList() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }

       // http://localhost:49462/api/Kiosk_FAQS/FAQSQuestion?org=flexi&locn=TA&lang=ta_IN&user=Kiosk&In_faqscat_Id=1095&keyword=

        String url= Constants.Url+"/api/Kiosk_FAQS/FAQSQuestion?org=flexi&locn=TA&user=kiosk&lang="+lan+"&In_faqscat_Id="+catId+"&keyword=";
        VolleyUtils.getVolleyResult(FaqSubTitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getSubCategoryList",response.toString());
                JSONArray array  = null;
                try {
                    array=response.getJSONArray("questions");
                    if(array.length()>0){
                        titleItems.clear();
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            Log.e("CHK",""+object.getString("out_faq_question"));
                            titleItems.add(new VideosTitleItems(object.getString("out_faq_question"),"#0069D9",object.getString("out_faqscategory_Id")));
                        }
                        dynamicviewcontainer.removeAllViews();
                        bindViews();
                    }else{
                        titleItems.clear();
                        dynamicviewcontainer.removeAllViews();
                        Toast.makeText(FaqSubTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindViews() {
        dynamicviewcontainer.removeAllViews();
        List<List<VideosTitleItems>> smallerLists = splittedbyList(titleItems,1);
        for (int k=0;k<smallerLists.size();k++){
            List<VideosTitleItems> sublist= smallerLists.get(k);
            LinearLayout linearLayout=new LinearLayout(FaqSubTitleActivity.this);
            linearLayout.removeAllViews();
            linearLayout.setLayoutParams(llLP);
            linearLayout.setOrientation(LinearLayout.VERTICAL);


                for (int j = 0; j < sublist.size(); j++) {
                    VideosTitleItems titleItems = sublist.get(j);
                    LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                            //android:layout_width='match_parent' an in xml
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            //android:layout_height='wrap_content'
                            Constants.dpToPx(FaqSubTitleActivity.this,50));
                    //LinearLayout innerlayout=new LinearLayout(this);
                    // innerlayout.setOrientation(LinearLayout.HORIZONTAL);
                    LayoutInflater inflater = (LayoutInflater)this.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                    View child = inflater.inflate(R.layout.vidotitle_itemlayouts, linearLayout,false);
                    AppCompatTextView subtitle= child.findViewById(R.id.subTitle);
                    LinearLayout titleContainer= child.findViewById(R.id.titleContainer);
                    AppCompatImageView img= child.findViewById(R.id.correctimg);
                    img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_correct));
                    subtitle.setText(sublist.get(j).getTitle());
                    //innerlayout.addView(child);
                    int finalJ = j;
                    titleContainer.setClickable(true);
                    titleContainer.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Log.i("TAG", "Layout width :"+ titleContainer.getWidth());
                            Log.i("TAG", "Layout height :"+ titleContainer.getHeight());
                        }
                    });

                    subtitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            img.performClick();
                        }
                    });

                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.e("titleContainer","True");
                            Intent i=new Intent(FaqSubTitleActivity.this,FaqsViewActivity.class);
                            i.putExtra("out_faqscategory_Id",sublist.get(finalJ).getId());
                            startActivity(i);
                        }
                    });
                    linearLayout.addView(child);
                }
            dynamicviewcontainer.addView(linearLayout);

        }
    }
    public LinearLayout.LayoutParams getLayoutParams(int pos){
        LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                0,
                //android:layout_height='wrap_content'
                Constants.dpToPx(FaqSubTitleActivity.this,50));
        llLP.weight = 1;
        if(pos==0)
        {
            llLP.setMargins(0, 0, 0, 0);
        }else{
            llLP.setMargins(Constants.dpToPx(FaqSubTitleActivity.this, 30), 0, 0, 0);
        }

        return llLP;
    }
    static <VideosTitleItems> List<List<VideosTitleItems>> splittedbyList(List<VideosTitleItems> list, final int L) {
        List<List<VideosTitleItems>> parts = new ArrayList<List<VideosTitleItems>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<VideosTitleItems>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
}