package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.formar.kioskapp.R;
import com.formar.kioskapp.adapter.CropListAdapter;
import com.formar.kioskapp.adapter.WeatherListAdapter;
import com.formar.kioskapp.model.CropModel;
import com.formar.kioskapp.model.WeatherListModel;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class WeathersViewActivity extends BaseActivity {

    AppCompatImageView logout,home,backBtn;
    AppCompatImageView language_icon;
    String date="";
    String villageId="";
    String wType="";
    ArrayList<WeatherListModel> weatherListModels=new ArrayList<>();
    ArrayList<CropModel> cropModelArrayList=new ArrayList<>();
    RecyclerView weatherrecyclerview;
    private WeatherListAdapter adapter;
    private CropListAdapter cropListAdapter;
    public String viewType="1";
    public String fromDate="";
    public String toDate="";
    public String kamarajvillageid="";
    AppCompatTextView Wtitle;
    LinearLayout weatherViewContainer,cropViewContainer;
    RecyclerView cropRecyclerview;
    AppCompatTextView villagetxt,disttxt;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public String loginType="";
    //public String kvillageId="";
    // public  String kdistrictId="";
    public String mobno="";
    public String villagename="";
    public String kamvillageid="";
    public String kamdistid="";
    public String kvid="";
    public String kamarajvid="";
    LinearLayout dynamicviewcontainer;
    AppCompatTextView croptitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weathers_view);
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        date=getIntent().getStringExtra("date");
        villageId=getIntent().getStringExtra("VillageId");
        kamarajvid = getIntent().getStringExtra("kvillageid");
        wType=getIntent().getStringExtra("wtype");
        viewType=getIntent().getStringExtra("viewtype");
        fromDate=getIntent().getStringExtra("fromdates");
        toDate=getIntent().getStringExtra("todates");
        Wtitle = findViewById(R.id.wviewtitle);
        weatherViewContainer = findViewById(R.id.weatherViewContainer);
        cropViewContainer = findViewById(R.id.cropViewContainer);
        cropRecyclerview = findViewById(R.id.cropRecyclerview);
        villagetxt = findViewById(R.id.villagetxt);
        disttxt = findViewById(R.id.districttxt);
        croptitle = findViewById(R.id.croptitle);
        dynamicviewcontainer=findViewById(R.id.dynamicviewcontainer);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        loginType=sharedpreferences.getString("login","");
        mobno=sharedpreferences.getString("fmobno","");
        kamvillageid = sharedpreferences.getString("kamvillage","");
        villagename = sharedpreferences.getString("villnames","");
        kamarajvillageid = sharedpreferences.getString("kvillageid","");

        Log.e("Mobnumber",mobno+"\n"+loginType+"\n"+kamarajvillageid+"\n"+villagename+"\n"+wType);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(WeathersViewActivity.this);
                } else {
                    setLogout(WeathersViewActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setHomepage(WeathersViewActivity.this);
                }else {
                    setHomepage(WeathersViewActivity.this);
                }
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    finish();
                } else {
                    finish();
                }
            }
        });
        language_icon=findViewById(R.id.language_icon);
        weatherrecyclerview=findViewById(R.id.dateRecyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(WeathersViewActivity.this,RecyclerView.HORIZONTAL,false);
        LinearLayoutManager clayoutmanager = new LinearLayoutManager(WeathersViewActivity.this,RecyclerView.VERTICAL,false);
        weatherrecyclerview.setLayoutManager(mLayoutManager);
        cropRecyclerview.setLayoutManager(clayoutmanager);
        adapter=new WeatherListAdapter(WeathersViewActivity.this,weatherListModels);
        cropListAdapter=new CropListAdapter(WeathersViewActivity.this,cropModelArrayList);
        weatherrecyclerview.setAdapter(adapter);
        cropRecyclerview.setAdapter(cropListAdapter);

        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(WeathersViewActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(WeathersViewActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        loginType=sharedpreferences.getString("login","");
        Log.e("lstyle",loginType);
        if(isNetworkAvailable()) {
            if(lan.equals("en")) {
                language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
            }else{
                language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
            }
            if(viewType.equals("2")){
                Wtitle.setText(getResources().getString(R.string.cropadvis_title));
                weatherViewContainer.setVisibility(View.GONE);
                cropViewContainer.setVisibility(View.VISIBLE);
                //getCropAdvisoryDetails(fromDate,toDate,"133","49");
                getCropAdvisoryDetails(fromDate,toDate,kamdistid,kamarajvillageid);
            }else{
                Wtitle.setText(getResources().getString(R.string.weather_title));
                weatherViewContainer.setVisibility(View.VISIBLE);
                cropViewContainer.setVisibility(View.GONE);
                //getWeatherDetails(wType,"133",date);
                getWeatherDetails(wType,kamarajvillageid,date);
            }
        } else {
            if(lan.equals("en")) {
                language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
            }else{
                language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
            }
            loginType=sharedpreferences.getString("login","");
            mobno=sharedpreferences.getString("fmobno","");
            villagename = sharedpreferences.getString("villnames","");
            kamarajvillageid = sharedpreferences.getString("kvillageid","");
            Log.e("lstyle",loginType);
            Log.e("lstyle",villagename);
            Log.e("lstyle",kamarajvillageid);
            if(viewType.equals("2")) {
                String weatherTitle=getResources().getString(R.string.cropadvis_titles)+" - "+villagename+",";
                Log.e("weatherTitle",weatherTitle);
              // Wtitle.setText(weatherTitle);
                weatherViewContainer.setVisibility(View.GONE);
                cropViewContainer.setVisibility(View.VISIBLE);
                //getOfflineCropAdvisoryDetails(fromDate,toDate,"134","49");
                getOfflineCropAdvisoryDetails(fromDate,toDate,kamarajvillageid,kamdistid);
            }else{
                String weatherTitle=getResources().getString(R.string.weather_title)+" - "+villagename+"";
               // Wtitle.setText(weatherTitle);
                weatherViewContainer.setVisibility(View.VISIBLE);
                cropViewContainer.setVisibility(View.GONE);
                //getOfflineWeatherDetails(wType,"134",date);
                getOfflineWeatherDetails(wType,kamarajvillageid,date);
            }
        }

    }

    private void getOfflineCropAdvisoryDetails(String fromDate, String toDate, String villageId, String distrId) {
        loginType=sharedpreferences.getString("login","");
        mobno=sharedpreferences.getString("fmobno","");
        villagename = sharedpreferences.getString("villnames","");
        kamarajvillageid = sharedpreferences.getString("kvillageid","");
        ProgressDialog pdialog;
        pdialog = new ProgressDialog(WeathersViewActivity.this);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setTitle(getResources().getString(R.string.loader_title));
        pdialog.show();
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk", Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from core_mst_crop_adv"
                , new String[] {});
        pdialog.dismiss();
        Log.e("msg",loginType);
        Log.e("msg",villagename);
        Log.e("msg",fromDate);
        Log.e("msg",toDate);
        Log.d("msg", cr.toString()+"--"+mobno);
        if(cr.moveToFirst()){
            cropModelArrayList.clear();
            if(loginType.equals("2")) {
                if(viewType.equals("2")) {
                    String weatherTitle=getResources().getString(R.string.cropadvis_titles)+" - "+villagename+","+cr.getString(15);
                    Log.e("weatherTitle",weatherTitle);
                    croptitle.setText(weatherTitle);

                } else {
                    String cropTitle=getResources().getString(R.string.weather_title)+" - "+villagename+","+cr.getString(15);
                    Log.e("cropTitle",cropTitle);
                    Wtitle.setText(cropTitle);
                }

                villagetxt.setText(cr.getString(11));
                disttxt.setText(cr.getString(15));
                cropModelArrayList.add(new CropModel(cr.getString(4),
                        cr.getString(5),
                        cr.getString(7),
                        cr.getString(8),
                        cr.getString(6),
                        cr.getString(10),
                        cr.getString(11),
                        cr.getString(15)));

            } else {
                for (int k = 0; k < cr.getCount(); k++) {
                    if (k == 0) {
                        do{
                            villagetxt.setText(cr.getString(11));
                            disttxt.setText(cr.getString(15));
                            villagename = sharedpreferences.getString("villnames","");
                            Log.e("msg",cr.getString(3));
                            if(viewType.equals("2")) {
                                String weatherTitle=getResources().getString(R.string.cropadvis_titles)+" - "+villagename+","+cr.getString(15);
                                Log.e("weatherTitle",weatherTitle);
                                croptitle.setText(weatherTitle);
                            }else{
                                String cropTitle=getResources().getString(R.string.weather_title)+" - "+villagename+","+cr.getString(15);
                                Log.e("cropTitle",cropTitle);
                                Wtitle.setText(cropTitle);
                            }if (kamarajvillageid.equals(cr.getString(6))) {
                                Log.e("msg",cr.getString(3));
                                cropModelArrayList.add(new CropModel(cr.getString(4),
                                        cr.getString(5),
                                        cr.getString(7),
                                        cr.getString(8),
                                        cr.getString(6),
                                        cr.getString(10),
                                        cr.getString(11),
                                        cr.getString(15)));
                            }
                        }while(cr.moveToNext());
                    }
                }
            }
            Log.e("weatherListModels",cropModelArrayList.size()+"");
            cropListAdapter.notifyDataSetChanged();
            if(cropModelArrayList.size()==0){
                dynamicviewcontainer.removeAllViews();
                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
               // View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                //dynamicviewcontainer.addView(child);
                AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
                builder.setMessage("Crop advisory detail is not available for this village");
                builder.setCancelable(true);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        } else {
            if(cropModelArrayList.size()==0){
                dynamicviewcontainer.removeAllViews();
                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                //View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                //dynamicviewcontainer.addView(child);
                AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
                builder.setMessage("Crop advisory detail is not available for this village");
                builder.setCancelable(true);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            //Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();

        }
    }


    private void getCropAdvisoryDetails(String fromDate, String toDate, String village_id, String district_id) {
        villagename = sharedpreferences.getString("villnames","");
        kamarajvillageid = sharedpreferences.getString("kvillageid","");
        kamdistid = sharedpreferences.getString("kamdistid","");
        String Url="";
        Url="https://nafcca.co.in/Api/v1/external/crop_advisory?district_id="+kamdistid+"&village_id="+kamarajvillageid+"&period_from="+fromDate+"&period_to="+toDate;
        ProgressDialog pdialog;
        pdialog = new ProgressDialog(WeathersViewActivity.this);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setTitle(getResources().getString(R.string.loader_title));
        pdialog.show();
        Log.e("getCropAdvisoryDetails",Url);
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        pdialog.dismiss();
                        Log.d("getCropAdvisoryDetails", response.toString()+"--"+mobno);
                        if(response.length()>0){
                            cropModelArrayList.clear();
                            try {
                                if(loginType.equals("2")) {
                                    JSONObject object=response.getJSONObject(0);
                                    if(viewType.equals("2")) {
                                        //String weatherTitle=getResources().getString(R.string.cropadvis_titles)+" - "+object.getString("village_name")+","+object.getString("district");
                                        String weatherTitle=getResources().getString(R.string.cropadvis_titles)+" - "+villagename+","+object.getString("district");
                                        Log.e("weatherTitle",weatherTitle);
                                        //croptitle.setText(weatherTitle);
                                          Wtitle.setText(getResources().getString(R.string.weather_title)+" - "+villagename+","+object.getString("district"));
                                    }else{
                                        String cropTitle=getResources().getString(R.string.weather_title)+" - "+villagename+","+object.getString("district");

                                        Log.e("cropTitle",cropTitle);
                                        Wtitle.setText(cropTitle);
                                        croptitle.setText(getResources().getString(R.string.farmadvis_titles)+" - "+villagename+","+object.getString("district"));
                                    }
                                    villagetxt.setText(object.getString("village_name"));
                                    disttxt.setText(object.getString("district"));
                                    cropModelArrayList.add(new CropModel(object.getString("cropid"), object.getString("crop_name"), object.getString("message_ta"), object.getString("message_en"),
                                            object.getString("village_id"), object.getString("name"), object.getString("village_name"), object.getString("district")));
                                }else {
                                    for (int k = 0; k < response.length(); k++) {
                                        JSONObject object = response.getJSONObject(k);
                                        if (k == 0) {
                                            villagename = sharedpreferences.getString("villnames","");
                                            villagetxt.setText(object.getString("village_name"));
                                            kamarajvillageid = sharedpreferences.getString("kvillageid","");
                                            disttxt.setText(object.getString("district"));
                                            if(viewType.equals("2")){
                                                String weatherTitle=getResources().getString(R.string.cropadvis_titles)+" - "+villagename+","+object.getString("district");
                                                Log.e("weatherTitle",weatherTitle);
                                                croptitle.setText(weatherTitle);
                                            } else {
                                                String lan= mySharePreference.getLanguage();
                                                if(lan.equalsIgnoreCase("en")){
                                                    String cropTitle=getResources().getString(R.string.weather_title)+" - "+villagename+","+object.getString("district");
                                                    Log.e("cropTitle",cropTitle);
                                                    Wtitle.setText(cropTitle);
                                                    //croptitle.setText("Farmer Level crop advisory");
                                                    croptitle.setText(getResources().getString(R.string.farmadvis_titles)+" - "+villagename+","+object.getString("district"));
                                                }else{
                                                    String cropTitle=getResources().getString(R.string.weather_title)+" - "+villagename+","+object.getString("district");
                                                    Log.e("cropTitle",cropTitle);
                                                    Wtitle.setText(cropTitle);
                                                    //croptitle.setText("Farmer Level crop advisory");
                                                    croptitle.setText(getResources().getString(R.string.farmadvis_titles)+" - "+villagename+","+object.getString("district"));
                                                }

                                            }
                                        }

                                        kamarajvillageid = sharedpreferences.getString("kvillageid","");
                                        Log.e("vid",villagename);
                                        Log.e("vid",kvid);
                                        if (kamarajvillageid.equals(object.getString("village_id"))) {
                                            cropModelArrayList.add(new CropModel(object.getString("cropid"), object.getString("crop_name"), object.getString("message_ta"), object.getString("message_en"),
                                                    object.getString("village_id"), object.getString("name"), object.getString("village_name"), object.getString("district")));
                                        } else {

                                        }
                                        Log.e("villageid",kvid);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("weatherListModels",cropModelArrayList.size()+"");
                            cropListAdapter.notifyDataSetChanged();
                            if(cropModelArrayList.size()==0){
                                dynamicviewcontainer.removeAllViews();
                                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                               // View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                               // dynamicviewcontainer.addView(child);
                                AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
                                builder.setMessage("Crop advisory detail is not available for this village");
                                builder.setCancelable(true);
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                AlertDialog alertDialog = builder.create();
                                alertDialog.show();
                            }
                        }else{
                            if(cropModelArrayList.size()==0){
                                dynamicviewcontainer.removeAllViews();
                                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                                //View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                                //dynamicviewcontainer.addView(child);
                                AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
                                builder.setMessage("Crop advisory detail is not available for this village");
                                builder.setCancelable(true);
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                AlertDialog alertDialog = builder.create();
                                alertDialog.show();
                            }
                            //Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();

                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        pdialog.dismiss();
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("token", "befd6907fad984fae880395825efe5d0");
                return params;
            }
        };
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                2500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(WeathersViewActivity.this).addToRequestQueue(getRequest);
    }

    private void getOfflineWeatherDetails (String wType, String villageId, String date){
        loginType=sharedpreferences.getString("login","");
        mobno=sharedpreferences.getString("fmobno","");
        villagename = sharedpreferences.getString("villnames","");
        kamarajvillageid = sharedpreferences.getString("kvillageid","");
        //Log.e("wtypes",wType);
        if(wType.equals("0")){
            SQLiteDatabase db = getApplicationContext()
                    .openOrCreateDatabase("kiosk", Context.MODE_PRIVATE,null);
            Cursor cr = db.rawQuery("select * from core_mst_weather_list where village_id = ?",
                    new String[]{villageId});
            if(cr.getCount() == 0){
               // Toast.makeText(WeathersViewActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
                builder.setMessage("Weather forecast is not available for this village");
                builder.setCancelable(true);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            while(cr.moveToNext()) {
                if(cr.getCount() > 0) {
                    //Log.e("msgs", cr.getString(10));
                    weatherListModels.add(new WeatherListModel(cr.getString(0),
                            cr.getString(1),
                            cr.getDouble(2),
                            cr.getDouble(3),
                            cr.getInt(4),
                            cr.getInt(5),
                            cr.getDouble(6),
                            cr.getInt(7),
                            cr.getInt(8),
                            cr.getDouble(9),
                            cr.getDouble(10)));
                }

            }

        }
        adapter.notifyDataSetChanged();
        //  Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();
        if(!loginType.equals("2")) {
            if (weatherListModels.size() > 0) {
                fromDate = weatherListModels.get(0).getDate();
                toDate = weatherListModels.get(weatherListModels.size() - 1).getDate();
                cropViewContainer.setVisibility(View.VISIBLE);
                //getOfflineCropAdvisoryDetails(fromDate, toDate, "134", "49");
                getOfflineCropAdvisoryDetails(fromDate,toDate,kamarajvillageid,kamdistid);
            }
        }else {
            //getOfflineWeatherDetails(wType, "134", "2021-03-29");
        }
        if(wType.equals("1")){
            SQLiteDatabase db = getApplicationContext()
                    .openOrCreateDatabase("kiosk", Context.MODE_PRIVATE,null);
            Cursor cr = db.rawQuery("select * from core_mst_forecast_weather_list where village_id = ?",
                    new String[]{villageId});
            if(cr.getCount() == 0){
               // Toast.makeText(WeathersViewActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
                builder.setMessage("Weather forecast is not available for this village");
                builder.setCancelable(true);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            while(cr.moveToNext()) {
                if(cr.getCount() > 0) {
                    //Log.e("msgs", cr.getString(10));
                    weatherListModels.add(new WeatherListModel(cr.getString(0),
                            cr.getString(1),
                            cr.getDouble(2),
                            cr.getDouble(3),
                            cr.getInt(4),
                            cr.getInt(5),
                            cr.getDouble(6),
                            cr.getInt(7),
                            cr.getInt(8),
                            cr.getDouble(9),
                            cr.getDouble(10)));
                }
            }
        }
        adapter.notifyDataSetChanged();
        if(!loginType.equals("2")) {
            if (weatherListModels.size() > 0) {
                fromDate = weatherListModels.get(0).getDate();
                toDate = weatherListModels.get(weatherListModels.size() - 1).getDate();
                cropViewContainer.setVisibility(View.VISIBLE);
                //getOfflineCropAdvisoryDetails(fromDate, toDate, "134", "49");
                getOfflineCropAdvisoryDetails(fromDate,toDate,kamarajvillageid,kamdistid);
            }
        } else {
            //getOfflineWeatherDetails(wType,"134","2021-03-29");
        }

    }


    private void getWeatherDetails(String wType, String village_id, String date) {
        kamarajvillageid = sharedpreferences.getString("kvillageid","");
        loginType=sharedpreferences.getString("login","");
        String Url="";
        if(wType.equals("0")){
            Url="https://nafcca.co.in/Api/v1/external/past?village_id="+kamarajvillageid+"&date="+date;
        }else if(wType.equals("1") && (kamarajvillageid.equals("0")) || (kamarajvillageid.isEmpty())){
            //Toast.makeText(WeathersViewActivity.this,"Hello World",Toast.LENGTH_LONG).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(WeathersViewActivity.this);
            builder.setMessage("Weather forecast is not available for this village");
            builder.setCancelable(true);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(WeathersViewActivity.this,WeatherSelectActivity.class);
                    startActivity(intent);
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else{
            Url="https://nafcca.co.in/Api/v1/external/forecast?village_id="+kamarajvillageid+"&date="+date;
        }
        //Log.e("wtypes",wType);
        Log.e("getWeatherDetailsaa",Url.toString());
        ProgressDialog pdialog;
        pdialog = new ProgressDialog(WeathersViewActivity.this);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setTitle(getResources().getString(R.string.loader_title));
        pdialog.show();
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        pdialog.dismiss();
                        Log.d("getWeatherDetailsaa", response.toString());
                        if(response.length()>0){
                            weatherListModels.clear();
                            for (int k=0;k<=4;k++){
                                //for (int k=0;k<=5;k++){
                                try {
                                    JSONObject object=response.getJSONObject(k);
                                    weatherListModels.add(new WeatherListModel(object.getString("date"),object.getString("village_id"),object.getDouble("tmax"),object.getDouble("tmin"),
                                            object.getInt("rh1"),object.getInt("rh2"),object.getDouble("rh_avg"),object.getInt("ws1"),object.getInt("ws2"),object.getDouble("ws_avg"),object.getDouble("rf")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.e("weatherListModels",weatherListModels.size()+"");
                            adapter.notifyDataSetChanged();
                            if(!loginType.equals("2")) {
                                if (weatherListModels.size() > 0) {
                                    fromDate = weatherListModels.get(0).getDate();
                                    toDate = weatherListModels.get(weatherListModels.size() - 1).getDate();
                                    cropViewContainer.setVisibility(View.GONE);
                                    //getCropAdvisoryDetails(fromDate, toDate, "134", "49");
                                    getCropAdvisoryDetails(fromDate,toDate,kamarajvillageid,kamdistid);
                                }
                            } else {
                                kamarajvillageid = sharedpreferences.getString("kvillageid","");
                                if (weatherListModels.size() > 0) {
                                    //Log.e("msgss",kamarajvillageid);
                                    //Log.e("msgss",loginType);
                                    fromDate = weatherListModels.get(0).getDate();
                                    toDate = weatherListModels.get(weatherListModels.size() - 1).getDate();
                                    cropViewContainer.setVisibility(View.GONE);
                                    //getCropAdvisoryDetails(fromDate, toDate, "134", "49");
                                    getCropAdvisoryDetails(fromDate,toDate,kamarajvillageid,kamdistid);
                                }
                            }

                        }else{
                            kamarajvillageid = sharedpreferences.getString("kvillageid","");
                            //getWeatherDetails(wType,"134","2021-03-29");
                            getWeatherDetails(wType,kamarajvillageid,"2021-03-29");
                            //  Toast.makeText(WeathersViewActivity.this, getResources().getString(R.string.norecord_title), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                        pdialog.dismiss();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                2500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(WeathersViewActivity.this).addToRequestQueue(getRequest);


    }
    public Boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}