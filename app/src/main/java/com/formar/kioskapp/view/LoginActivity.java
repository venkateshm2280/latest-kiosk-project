package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.database.DBHandler;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.MySharePreference;
import com.formar.kioskapp.utils.VolleySingleton;
import com.formar.kioskapp.utils.VolleyUtils;
import com.sachinvarma.easypermission.EasyPermissionConstants;
import com.sachinvarma.easypermission.EasyPermissionInit;
import com.sachinvarma.easypermission.EasyPermissionList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LoginActivity extends BaseActivity {

    AppCompatButton signBtn;
    AppCompatTextView setPasswd, forgotPasswd;
    AppCompatEditText userIdtxt, passwdtxt;
    SharedPreferences sharedpreferences;
    DBHandler db;
    ImageView master;
    JSONObject userd;
    SQLiteDatabase dbs;
    ProgressDialog pdialog;

    public static final String MyPREFERENCES = "MyPrefs";
    AppCompatImageView language_icon;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE}; // Here i used multiple permission check
    private int MULTIPLE_PERMISSIONS = 120;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private ArrayList<String> deniedPermissions;
    AppCompatTextView loginTitle, userIdtitle, passTitle, guestusertxt;

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        signBtn = findViewById(R.id.signBtn);
        setPasswd = findViewById(R.id.setPasswd);
        forgotPasswd = findViewById(R.id.forgotPasswd);
        userIdtxt = findViewById(R.id.userIdtxt);
        passwdtxt = findViewById(R.id.passtxt);
        language_icon = findViewById(R.id.language_icon);
        guestusertxt = findViewById(R.id.guestusertxt);
        pdialog = new ProgressDialog(this);
        master = findViewById(R.id.master);
        db = new DBHandler(this);
        dbs = db.getWritableDatabase();
        loginTitle = findViewById(R.id.login_title);
        userIdtitle = findViewById(R.id.userid_title);
        passTitle = findViewById(R.id.password_title);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
       /* editor.putString("language", "en");
        editor.commit();*/

        mySharePreference = MySharePreference.getInstance(LoginActivity.this);
        signBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   finish();\
                if (isNetworkAvailable()) {
                    if (userIdtxt.getText().toString().isEmpty()) {
                        Toast.makeText(LoginActivity.this, "Please Enter UserId details", Toast.LENGTH_SHORT).show();
                    } else if (passwdtxt.getText().toString().isEmpty()) {
                        Toast.makeText(LoginActivity.this, "Please Enter Password details", Toast.LENGTH_SHORT).show();
                    } else {
                        setLogin();
                    }
                } else {
                    setOfflineLogin();
                }
            }
        });
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan = mySharePreference.getLanguage();
                Log.e("lan", lan);
                if (lan.equals("en")) {
                    Constants.setLanguage(LoginActivity.this, "ta", language_icon, 1);
                    // signBtn.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,12 );
                } else {
                    Constants.setLanguage(LoginActivity.this, "en", language_icon, 1);
                    // signBtn.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
                }
                //refresh();
            }
        });
        String lan = mySharePreference.getLanguage();
        if (lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
            //  signBtn.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
            // forgotPasswd.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,16 );
            //  setPasswd.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,16 );
        } else {
            //signBtn.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,15 );
            // language_icon.setImageResource(R.drawable.english);
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
           /* forgotPasswd.setTextSize(TypedValue.COMPLEX_UNIT_SP ,14 );
            setPasswd.setTextSize(TypedValue.COMPLEX_UNIT_SP ,14 );
            passTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
            userIdtitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,14 );
            loginTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP ,18 );
            guestusertxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,18 );*/
        }
        setPasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ResetActivity.class);
                intent.putExtra("userId", "");
                startActivity(intent);
            }
        });
        guestusertxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("login", "2");
                    editor.putString("AuthKey", Constants.AuthKey);
                    editor.commit();
                    Intent i = new Intent(getApplicationContext(), Dashboard.class);
                    startActivity(i);
                    finish();
                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("login", "2");
                    editor.putString("AuthKey", Constants.AuthKey);
                    editor.commit();
                    Intent i = new Intent(getApplicationContext(), Dashboard.class);
                    startActivity(i);
                    finish();
                }
            }
        });
        forgotPasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivityForResult(intent, 500);
            }
        });

        CheckPermissions();


    }

    private void CheckPermissions() {
        List<String> permission = new ArrayList<>();
        permission.add(EasyPermissionList.ACCESS_FINE_LOCATION);
        permission.add(EasyPermissionList.READ_EXTERNAL_STORAGE);
        permission.add(EasyPermissionList.WRITE_EXTERNAL_STORAGE);
        new EasyPermissionInit(LoginActivity.this, permission);
    }


    public void refresh() {
        finish();
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void setLogin() {
        String url = Constants.Url + "/api/kiosk_login/KioskLogin?instance=ta&In_user_code=" + userIdtxt.getText().toString() + "&In_user_pwd=" + passwdtxt.getText().toString();
        VolleyUtils.getVolleyResult(LoginActivity.this, Request.Method.GET, url, true, null, false, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }

            @Override
            public void onResponse(JSONObject response) {
                Log.e("onResponse", response.toString());
                JSONObject sys = null;
                try {
                    String rs = response.getString("In_Reponse");
                    if (rs.equals("success")) {
                        String un = response.getString("farmer_name");
                        String uc = response.getString("farmer_code");
                        String rc = response.getString("farmer_village");
                        String fvillagecode = response.getString("village_code");
                        String talukname = response.getString("farmer_taluk");
                        String dist_name = response.getString("farmer_dist");
                        String state_name = response.getString("farmer_state");
                        String isReset = response.getString("Password_reset");
                        String kamaraj_village_id = response.getString("Kamaraj_village_id");
                        String kamaraj_district_id = response.getString("Kamaraj_district_id");
                        int farmerGid = response.getInt("farmer_gid");
                        String farmer_villageII = response.getString("farmer_villageII");
                        String farmer_talukII = response.getString("farmer_talukII");
                        String farmer_distII = response.getString("farmer_distII");
                        String farmer_stateII = response.getString("farmer_stateII");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("fname", un);
                        editor.putString("fcode", uc);

                        editor.putString("fvillage", rc);
                        editor.putString("fvillage_code", fvillagecode);
                        editor.putString("ftaluk", talukname);
                        editor.putString("fdistname", dist_name);
                        editor.putString("state_name", state_name);
                        editor.putString("fmobno", response.getString("Mobile_no"));

                        editor.putString("lafarmervillage",farmer_villageII);
                        editor.putString("lafarmertaluk",farmer_talukII);
                        editor.putString("lafarmerdist",farmer_distII);
                        editor.putString("lafarmerstate",farmer_stateII);

                        editor.putString("farmerGid", String.valueOf(farmerGid));
                        editor.putString("login", "1");
                        editor.putString("off", "1");
                        editor.putString("off", "1");
                        editor.putString("username", userIdtxt.getText().toString());
                        editor.putString("kvillageid",kamaraj_village_id);
                        editor.putString("kdistrictid",kamaraj_district_id);
                        editor.commit();
                        if (isReset.equals("N")) {

                            Toast.makeText(LoginActivity.this, "Kindly Reset password!..", Toast.LENGTH_SHORT).show();
                            Intent newin = new Intent(LoginActivity.this, ResetActivity.class);
                            newin.putExtra("userId", userIdtxt.getText().toString());
                            startActivity(newin);
                            userIdtxt.setText("");
                            passwdtxt.setText("");
                        } else {
                            Toast.makeText(LoginActivity.this, rs, Toast.LENGTH_SHORT).show();
                            getAuthKey();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Invalid Login", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setOfflineLogin() {
        String status_code = "A";
        Boolean isValid = true;
        String username = userIdtxt.getText().toString();
        String password = passwdtxt.getText().toString();
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk", Context.MODE_PRIVATE, null);
        Cursor cr = db.rawQuery("select\n" +
                "      farmer_rowid as farmer_gid,\n" +
                "      farmer_code,\n" +
                "      farmer_name,\t \n" +
                "      farmer_village as village_code,\n" +
                "      g.master_name as farmer_village,  \n" +
                "\t\tb.user_code as username,\t  \n" +
                "\t\tb.user_pwd as passwords,\n" +
                "      farmer_panchayat as panchayat_code,    \n" +
                "      farmer_taluk as taluk_code,\n" +
                "\t  h.master_name as farmer_taluk,         \n" +
                "      farmer_dist as dist_code,\n" +
                "      i.master_name as farmer_dist, \n" +
                "      farmer_state as state_code,    \n" +
                "      j.master_name as farmer_state,\n" +
                "        'success'  as In_Reponse,\n" +
                "       '' as instance,\n" +
                "       b.contact_no as Mobile_no,\n" +
                "       password_reset as password_reset\n" +
                "      from core_mst_tfarmer a\n" +
                "      inner join core_mst_tuser b on a.farmer_rowid=b.orgn_code\n" +
                "\t inner join core_mst_tmastertranslate g  on  farmer_village=g.mastertranslate_master_code and g.mastertranslate_lang_code=\"en_US\"\n" +
                "\t  inner join core_mst_tmastertranslate h  on  farmer_taluk=h.mastertranslate_master_code and h.mastertranslate_lang_code=\"en_US\"\n" +
                "\t  inner join core_mst_tmastertranslate i  on  farmer_dist=i.mastertranslate_master_code and i.mastertranslate_lang_code=\"en_US\"\n" +
                "\t  inner join core_mst_tmastertranslate j  on  farmer_state=j.mastertranslate_master_code and j.mastertranslate_lang_code=\"en_US\"\n" +
                "      where a.status_code='A'", new String[]{});

        while(cr.moveToNext()){
            if(cr.getCount() > 0){
                if((cr.getString(5).equals(username)) && cr.getString(6).equals(password)
                        && cr.getString(14).equals("success")) {
                    String un = cr.getString(2);
                    String uc = cr.getString(1);
                    String rc = cr.getString(4);
                    String fvillagecode = cr.getString(3);
                    String talukname = cr.getString(9);
                    String dist_name = cr.getString(11);
                    String state_name = cr.getString(13);
                    String isReset = cr.getString(17);
                    int farmerGid = cr.getInt(0);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("fname", un);
                    editor.putString("fcode", uc);

                    editor.putString("fvillage", rc);
                    editor.putString("fvillage_code", fvillagecode);
                    editor.putString("ftaluk", talukname);
                    editor.putString("fdistname", dist_name);
                    editor.putString("state_name", state_name);
                    editor.putString("fmobno", cr.getString(16));

                    editor.putString("farmerGid", String.valueOf(farmerGid));
                    editor.putString("login", "1");
                    editor.putString("off", "1");
                    editor.putString("off", "1");
                    editor.putString("username", userIdtxt.getText().toString());
                    editor.commit();
                    Toast.makeText(LoginActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                    startActivity(intent);
                    finish();
                }
            }else {
                Toast.makeText(LoginActivity.this, "Invalid Login", Toast.LENGTH_SHORT).show();
            }
        }


    }




    private void getAuthKey() {
     JSONObject jsonParam = null;
        try {
            jsonParam = new JSONObject();
            jsonParam.put("Username","test");
            jsonParam.put("Password","test");
        }catch (Exception e){
            e.printStackTrace();
        }

        final String url = Constants.Url+"/Users/authenticate";
        Log.e("URL",""+url);
        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.POST, url, jsonParam,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.e("Response", response.toString());
                        JSONObject sys  = null;
                        try {
                            String key = response.getString("token");
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("AuthKey", key);
                            editor.putString("login", "1");
                            editor.commit();
                            Toast.makeText(LoginActivity.this, "Welcome Admin!.." , Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getApplicationContext(), Dashboard.class);
                            startActivity(i);
                            finish();
                            //   overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                        } catch (JSONException e) {
                            Toast.makeText(LoginActivity.this, "Invalid Login", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Error:"+error.getMessage(), Toast.LENGTH_LONG).show();

                        Log.d("Error.Response", String.valueOf(error));
                    }
                }
        );

// add it to the RequestQueue
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(getRequest);

    }



   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("onActivityResult",resultCode+"--"+requestCode);
        if(requestCode==500){
            if(resultCode == RESULT_OK) {
                if (data != null) {
                    String val = data.getStringExtra("ischanged");
                    if (val.equals("1")) {
                        refresh();
                    }
                }
            }
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case EasyPermissionConstants.INTENT_CODE:

                if (data != null) {
                    boolean isGotAllPermissions =
                            data.getBooleanExtra(EasyPermissionConstants.IS_GOT_ALL_PERMISSION, false);

                    if (data.hasExtra(EasyPermissionConstants.IS_GOT_ALL_PERMISSION)) {
                       /* if (isGotAllPermissions) {
                            Toast.makeText(this, "All Permissions Granted", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(this, "All permission not Granted", Toast.LENGTH_SHORT).show();
                        }}*/

                        // if you want to know which are the denied permissions.
                        if (data.getSerializableExtra(EasyPermissionConstants.DENIED_PERMISSION_LIST) != null) {

                            deniedPermissions = new ArrayList<>();

                            deniedPermissions.addAll((Collection<? extends String>) data.getSerializableExtra(
                                    EasyPermissionConstants.DENIED_PERMISSION_LIST));

                            if (deniedPermissions.size() > 0) {
                                for (int i = 0; i < deniedPermissions.size(); i++) {
                                    switch (deniedPermissions.get(i)) {

                                        case EasyPermissionList.READ_EXTERNAL_STORAGE:

                                            Toast.makeText(this, "Storage Permission not granted", Toast.LENGTH_SHORT)
                                                    .show();

                                            break;

                                        case EasyPermissionList.ACCESS_FINE_LOCATION:

                                            Toast.makeText(this, "Location Permission not granted", Toast.LENGTH_SHORT)
                                                    .show();

                                            break;

                                        case EasyPermissionList.WRITE_EXTERNAL_STORAGE:

                                            Toast.makeText(this, "Storage Permission not granted", Toast.LENGTH_SHORT)
                                                    .show();

                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
        }
    }

    }