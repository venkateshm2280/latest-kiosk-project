package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.codesgood.views.JustifiedTextView;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class FaqsViewActivity extends BaseActivity {

    AppCompatImageView logout,home,backBtn;
    String viewId="";
    @SuppressLint("SetJavaScriptEnabled")
    String Url="http://www.africau.edu/images/default/sample.pdf";
    //AppCompatTextView questionstxt;
    AppCompatTextView title;
    AppCompatImageView language_icon;
    JustifiedTextView desctxt;
    LinearLayout descContainer;
    AppCompatTextView refUrltxt,reffiletxt;
    LinearLayout RefUrlcontainer, RefFilecontainer;
    AppCompatTextView refImgtxt,refVideotxt,refurl;
    LinearLayout RefImgcontainer, RefVideocontainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_faqs_view);
        viewId=getIntent().getStringExtra("out_faqscategory_Id");
        logout=findViewById(R.id.logouttxt);
        descContainer=findViewById(R.id.descContainer);
        home=findViewById(R.id.hometxt);
        backBtn=findViewById(R.id.backBtn);
        title=findViewById(R.id.title);
        desctxt=findViewById(R.id.desctxt);
        //questionstxt=findViewById(R.id.questionstxt);
        RefUrlcontainer=findViewById(R.id.refurlContainer);
        RefFilecontainer=findViewById(R.id.reffileContainer);
        RefFilecontainer.setVisibility(View.VISIBLE);
        RefUrlcontainer.setVisibility(View.VISIBLE);
        refUrltxt=findViewById(R.id.refurltxt);
        reffiletxt=findViewById(R.id.reffiletxt);
        refurl=findViewById(R.id.refurl);

        //image and video
        RefImgcontainer=findViewById(R.id.refImgContainer);
        RefVideocontainer=findViewById(R.id.refVideoContainer);
        refImgtxt=findViewById(R.id.refImgtxt);
        refVideotxt=findViewById(R.id.refVideotxt);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        refUrltxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!refUrltxt.getText().toString().equals("null")&&!refUrltxt.getText().toString().isEmpty()) {
                    openweb(refUrltxt.getText().toString());
                }
            }
        });
        reffiletxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!reffiletxt.getText().toString().equals("null")&&!reffiletxt.getText().toString().isEmpty()) {
                    openDefault(reffiletxt.getText().toString());
                }
            }
        });
        refImgtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!refImgtxt.getText().toString().equals("null")&&!refImgtxt.getText().toString().isEmpty()) {
                    openweb(refImgtxt.getText().toString());
                }
            }
        });
        refVideotxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!refVideotxt.getText().toString().equals("null")&&!refVideotxt.getText().toString().isEmpty()) {
                    Intent i=new Intent(FaqsViewActivity.this,VideoPlayActivity.class);
                    i.putExtra("filename",refImgtxt.getText().toString());
                    startActivity(i);
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    setLogout(FaqsViewActivity.this);
                }else {
                    setLogout(FaqsViewActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setHomepage(FaqsViewActivity.this);
                } else {
                    setHomepage(FaqsViewActivity.this);
                }
            }
        });
        WebView urlWebView = (WebView)findViewById(R.id.docview);
       /* urlWebView.setWebViewClient(new AppWebViewClients());
        urlWebView.getSettings().setJavaScriptEnabled(true);
        urlWebView.getSettings().setUseWideViewPort(true);
        urlWebView.loadUrl("http://docs.google.com/gview?embedded=true&url="
                + Url);*/
        if(isNetworkAvailable()) {
            getFaqanswers();
        } else {
            getOfflineFaqanswers();
        }

        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(FaqsViewActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(FaqsViewActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
    }
    public void openDefault(String url) {
        Log.e("openWebPage",url);
        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    public void openweb(String url){
        if (!url.startsWith("https://") && !url.startsWith("http://")){
            url = "http://" + url;
        }
        Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(openUrlIntent);

    }
    public Boolean isNetworkAvailable(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void getOfflineFaqanswers() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext().
                openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cursor = db.rawQuery("select * from kisok_trn_tfaq where faq_gid = ?"
                ,new String[] {viewId});
        while (cursor.moveToNext()) {
            if(cursor.getCount() > 0) {
                Log.e("Data1",cursor.getString(3));
                if (lan.equals("en_US")) {
                    title.setText(cursor.getString(3));
                    //questionstxt.setText(cursor.getString(3));
                    desctxt.setText(cursor.getString(4));
                    if(cursor.getString(10).isEmpty()){
                        refUrltxt.setText("NA");
                        refUrltxt.setBackground(null);
                    }else{
                        refUrltxt.setText(cursor.getString(10));
                    }
                    if(cursor.getString(9).isEmpty()){
                        reffiletxt.setText("NA");
                        reffiletxt.setBackground(null);
                    }else{
                        reffiletxt.setText(cursor.getString(9));
                    }
                    if(cursor.getString(11).isEmpty()) {
                        RefImgcontainer.setVisibility(View.GONE);
                        refImgtxt.setText("NA");
                        refImgtxt.setBackground(null);
                    }else{
                        RefImgcontainer.setVisibility(View.VISIBLE);
                        refImgtxt.setText(cursor.getString(11));
                    }
                    if(cursor.getString(12).isEmpty()) {
                        RefVideocontainer.setVisibility(View.GONE);
                        refVideotxt.setText("NA");
                        refVideotxt.setBackground(null);
                    }else{
                        RefVideocontainer.setVisibility(View.VISIBLE);
                        refVideotxt.setText(cursor.getString(12));
                    }
                } else {
                    title.setText(cursor.getString(6));
                    //questionstxt.setText(cursor.getString(6));
                    desctxt.setText(cursor.getString(7));
                    if(cursor.getString(10).isEmpty()){
                        refUrltxt.setText("NA");
                        refUrltxt.setBackground(null);
                    }else{
                        refUrltxt.setText(cursor.getString(10));
                    }
                    if(cursor.getString(9).isEmpty()){
                        reffiletxt.setText("NA");
                        reffiletxt.setBackground(null);
                    }else{
                        reffiletxt.setText(cursor.getString(9));
                    }
                    if(cursor.getString(11).isEmpty()) {
                        RefImgcontainer.setVisibility(View.GONE);
                        refImgtxt.setText("NA");
                        refImgtxt.setBackground(null);
                    }else{
                        RefImgcontainer.setVisibility(View.VISIBLE);
                        refImgtxt.setText(cursor.getString(11));
                    }
                    if(cursor.getString(12).isEmpty()) {
                        RefVideocontainer.setVisibility(View.GONE);
                        refVideotxt.setText("NA");
                        refVideotxt.setBackground(null);
                    }else{
                        RefVideocontainer.setVisibility(View.VISIBLE);
                        refVideotxt.setText(cursor.getString(12));
                    }
                }
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Kiosk");
                builder.setMessage("No Records Found");
                builder.show();
            }
        }
    }
    private void getFaqanswers() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){

            lan="en_US";
        }else{

            lan="ta_IN";
        }

        String url= Constants.Url+"/api/Kiosk_FAQS/FAQSAnswers?org=flexi&locn=TA&user=kiosk&lang="+lan+"&In_faq_gid="+viewId;
        VolleyUtils.getVolleyResult(FaqsViewActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getFaqanswers",response.toString());
                JSONArray array  = null;
                JSONObject object=null;
                try {
                    object=response.getJSONObject("answers");
                    title.setText(object.getString("out_faq_question"));
                    //questionstxt.setText(object.getString("out_faq_question"));
                    desctxt.setText(object.getString("out_faq_answer"));
                    if(object.getString("in_faq_urltype").isEmpty()){
                        refUrltxt.setText("NA");
                        refUrltxt.setBackground(null);
                    }else{
                        refUrltxt.setText(object.getString("in_faq_urltype"));
                    }
                    if(object.getString("in_faq_ans_upload").isEmpty()){
                        reffiletxt.setText("NA");
                        reffiletxt.setBackground(null);
                    }else{
                        reffiletxt.setText(object.getString("in_faq_ans_upload"));
                    }
                    //
                    if(object.getString("in_faq_ans_upload").isEmpty()){
                        RefImgcontainer.setVisibility(View.GONE);
                        refImgtxt.setText("NA");
                        refImgtxt.setBackground(null);
                    }else{
                        RefImgcontainer.setVisibility(View.VISIBLE);
                        refImgtxt.setText(object.getString("in_faq_ans_upload"));
                    }
                    if(object.getString("in_video_filepathf").equals("null") || object.getString("in_video_filepathf").isEmpty()){
                        RefVideocontainer.setVisibility(View.GONE);
                        refVideotxt.setText("NA");
                        refVideotxt.setBackground(null);
                    }else{
                        RefVideocontainer.setVisibility(View.VISIBLE);
                        refVideotxt.setText(object.getString("in_video_filepathf"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class AppWebViewClients extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

        }
    }

}