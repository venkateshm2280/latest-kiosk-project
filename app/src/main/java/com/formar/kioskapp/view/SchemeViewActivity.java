package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.codesgood.views.JustifiedTextView;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class SchemeViewActivity extends BaseActivity {
    AppCompatImageView logout,home,backBtn;
    String viewId="";
    @SuppressLint("SetJavaScriptEnabled")
    String Url="http://www.africau.edu/images/default/sample.pdf";
    String webpage="https://farmer.gov.in";
    AppCompatTextView questionstxt,title;
    AppCompatImageView language_icon;
    JustifiedTextView desctxt;
    AppCompatTextView refUrltxt,reffiletxt;
    LinearLayout RefUrlcontainer, RefFilecontainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_faqs_view);
        viewId=getIntent().getStringExtra("out_schemecategory_Id");
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        backBtn=findViewById(R.id.backBtn);
        desctxt=findViewById(R.id.desctxt);
       // questionstxt=findViewById(R.id.questionstxt);
        RefUrlcontainer=findViewById(R.id.refurlContainer);
        RefFilecontainer=findViewById(R.id.reffileContainer);
        RefFilecontainer.setVisibility(View.VISIBLE);
        RefUrlcontainer.setVisibility(View.VISIBLE);
        refUrltxt=findViewById(R.id.refurltxt);
        reffiletxt=findViewById(R.id.reffiletxt);
        title=findViewById(R.id.title);
        //title.setText(getResources().getString(R.string.govsview_title));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        refUrltxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!refUrltxt.getText().toString().equals("null")&&!refUrltxt.getText().toString().isEmpty()) {
                    openweb(refUrltxt.getText().toString());
                }
            }
        }); reffiletxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!reffiletxt.getText().toString().equals("null")&&!reffiletxt.getText().toString().isEmpty()) {
                    openDefault(reffiletxt.getText().toString());
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(SchemeViewActivity.this);
                } else {
                    setLogout(SchemeViewActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    setHomepage(SchemeViewActivity.this);
                } else {
                    setHomepage(SchemeViewActivity.this);
                }
            }
        });
        WebView urlWebView = (WebView)findViewById(R.id.docview);
        /*urlWebView.setWebViewClient(new AppWebViewClients());
        urlWebView.getSettings().setJavaScriptEnabled(true);
        urlWebView.getSettings().setUseWideViewPort(true);
        urlWebView.loadUrl("http://docs.google.com/gview?embedded=true&url="
                + Url);*/
        if(isNetworkAvailable()) {
            getFaqanswers();
        } else {
            getOfflineSchemeAnswers();
        }

        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(SchemeViewActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(SchemeViewActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
    }
    public void openDefault(String url) {
        Log.e("openWebPage",url);
        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    public void openweb(String url){
        if (!url.startsWith("https://") && !url.startsWith("http://")){
            url = "http://" + url;
        }
        Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(openUrlIntent);

    }

    public Boolean isNetworkAvailable(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    private void getOfflineSchemeAnswers(){
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kiosk_trn_tscheme where scheme_gid = ?",
                new String[] {viewId});
        if(cr.getCount() == 0) {
            Toast.makeText(SchemeViewActivity.this,"No Records Found",Toast.LENGTH_LONG).show();
        }
        while (cr.moveToNext()) {
            if(cr.getCount() > 0) {
                Log.e("Data1",cr.getString(4));
                if (lan.equals("en_US")) {
                    title.setText(cr.getString(4));
                    //questionstxt.setText(cr.getString(4));
                    desctxt.setText(cr.getString(5));
                } else {
                    title.setText(cr.getString(13));
                    //questionstxt.setText(cr.getString(13));
                    desctxt.setText(cr.getString(7));
                }

               if(cr.getString(9).isEmpty()){
                    refUrltxt.setText("NA");
                    refUrltxt.setBackground(null);
                }else{
                    refUrltxt.setText(cr.getString(9));
                }
                if(cr.getString(10).isEmpty()){
                    reffiletxt.setText("NA");
                    reffiletxt.setBackground(null);
                }else{
                    reffiletxt.setText(cr.getString(10));
                }
            }
        }
    }
    private void getFaqanswers() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        String url= Constants.Url+"/api/Kiosk_Scheme/GetSchemadata?org=flexi&locn=TA&user=kiosk&lang="+lan+"&In_scheme_gid="+viewId;
        VolleyUtils.getVolleyResult(SchemeViewActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getFaqanswers",response.toString());
                JSONArray array  = null;
                JSONObject object=null;
                try {
                    object=response.getJSONObject("schemedata");
                    title.setText(object.getString("out_scheme_name"));
                   // questionstxt.setText(object.getString("out_scheme_name"));
                    desctxt.setText(object.getString("out_scheme_description"));
                    if(object.getString("scheme_url").isEmpty()){
                        refUrltxt.setText("NA");
                        refUrltxt.setBackground(null);
                    }else{
                        refUrltxt.setText(object.getString("scheme_url"));
                    }
                    if(object.getString("upload_path").isEmpty()){
                        reffiletxt.setText("NA");
                        reffiletxt.setBackground(null);
                    }else{
                        reffiletxt.setText(object.getString("upload_path"));
                    }
                   // reffiletxt.setText(object.getString("upload_path"));
                   /* refUrltxt.setText(webpage);
                    reffiletxt.setText(Url);*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class AppWebViewClients extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

        }
    }
}