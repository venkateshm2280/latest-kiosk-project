package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.model.VideosTitleItems;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class VideosSubtitleActivity extends BaseActivity {
    AppCompatImageView logout,home;
    List<VideosTitleItems> titleItems=new ArrayList<>();
    String catId="";
    String catname="";
    AppCompatTextView title;
    LinearLayout dynamicviewcontainer;
    AppCompatImageView backBtn;
    private LinearLayout.LayoutParams llLP=null;
    AppCompatImageView language_icon;
    AppCompatEditText searchTxt;
    AppCompatImageView searchBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_videos_subtitle);
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        title=findViewById(R.id.title);
        dynamicviewcontainer=findViewById(R.id.dynamicviewcontainer);
        catId=getIntent().getStringExtra("catIds");
        catname=getIntent().getStringExtra("catname");
        title.setText(getResources().getString(R.string.trainfor_title)+" "+catname);
        llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                LinearLayout.LayoutParams.MATCH_PARENT,
                //android:layout_height='wrap_content'
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llLP.setMargins(0, Constants.dpToPx(VideosSubtitleActivity.this,20),0,0);
        llLP.gravity= Gravity.CENTER;
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(VideosSubtitleActivity.this);
                } else {
                    setLogout(VideosSubtitleActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setHomepage(VideosSubtitleActivity.this);
                } else {
                    setHomepage(VideosSubtitleActivity.this);
                }
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    finish();
                } else {
                    finish();
                }
            }
        });
        searchTxt=findViewById(R.id.searchTxt);
        searchBtn=findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!searchTxt.getText().toString().isEmpty()){
                    if(searchTxt.length()>2) {
                        if(isNetworkAvailable()) {
                            searchResults(searchTxt.getText().toString());
                        } else {
                            searchOfflineResults(searchTxt.getText().toString());
                        }

                    }
                }else{
                    Toast.makeText(VideosSubtitleActivity.this, "Kindly type keyword", Toast.LENGTH_SHORT).show();
                }
            }
        });
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==0){
                    getSubCategoryList();
                }
            }
        });
        if(isNetworkAvailable()) {
            getSubCategoryList();
        } else {
            getOfflineSubCategoryList();
        }

        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(VideosSubtitleActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(VideosSubtitleActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
    }
    private void searchOfflineResults(String keyword) {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        dynamicviewcontainer.removeAllViews();

        String lan2;
        if(lan.equals("en")){
            lan2="en_US";
        }else{
            lan2="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kiosk_trn_tvideo where video_keywords LIKE ?",
                new String[]{keyword+"%"});
        //Log.e("res",catId);
        Log.e("res",keyword);
        if(cr.getCount() == 0) {
            Toast.makeText(VideosSubtitleActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
        }
        if(cr.moveToFirst()) {
            do {
                Log.e("res",keyword);
                Log.e("res", cr.getString(0));
                Log.e("res", cr.getString(3));
                if (keyword.equalsIgnoreCase(cr.getString(3))) {
                    titleItems.add(new VideosTitleItems(cr.getString(3),"#0069D9",cr.getString(0)));
                }
                bindViews();
            }while(cr.moveToNext());
        }else{
            dynamicviewcontainer.removeAllViews();
            Toast.makeText(VideosSubtitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
            LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
            dynamicviewcontainer.addView(child);
        }
    }
    private void searchResults(String keyword) {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        dynamicviewcontainer.removeAllViews();

        String lan2;
        if(lan.equals("en")){
            lan2="en_US";
        }else{
            lan2="ta_IN";
        }
        String url= Constants.Url+"/api/Kiosk_Video/VideoTiltleList?org=flexi&locn=TA&user=kiosk&lang="+lan2+"&In_video_gid="+catId+"&keyword="+keyword;

    //    String url= Constants.Url+"/api/Kiosk_Video/VideoCategoryList?org=keyword&locn=TA&lang=en_US&user="+keyword;
        VolleyUtils.getVolleyResult(VideosSubtitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("searchResults",response.toString());
                JSONArray array  = null;
                titleItems.clear();
                try {
                    array=response.getJSONArray("title");
                    if(array.length()>0){
                        titleItems.clear();
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            titleItems.add(new VideosTitleItems(object.getString("out_title"),"#0069D9",object.getString("out_file_Name")));

                        }
                        bindViews();
                    }else{
                        dynamicviewcontainer.removeAllViews();
                        Toast.makeText(VideosSubtitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    private void getOfflineSubCategoryList() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kiosk_trn_tvideo where video_category = ?",
                new String[]{catId});
        if(cr.getCount() == 0) {
            Toast.makeText(VideosSubtitleActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
        }
        while (cr.moveToNext()) {
            if(cr.getCount() > 0) {
                if (lan.equals("en_US")) {
                    Log.e("video",cr.getString(3));
                    titleItems.add(new VideosTitleItems(cr.getString(3),"#0069D9",cr.getString(0)));
                } else {
                    titleItems.add(new VideosTitleItems(cr.getString(3),"#0069D9",cr.getString(0)));
                }

                bindViews();
            }else{
                dynamicviewcontainer.removeAllViews();
                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                dynamicviewcontainer.addView(child);
            }
        }
    }
    private void getSubCategoryList() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        dynamicviewcontainer.removeAllViews();
     //   http://localhost:49462/api/Kiosk_Video/VideoTiltleList?org=flexi&locn=TA&user=kiosk&In_video_gid=1092&lang=ta_IN&keyword=
        String url= Constants.Url+"/api/Kiosk_Video/VideoTiltleList?org=flexi&locn=TA&user=kiosk&lang="+lan+"&In_video_gid="+catId+"&keyword=";

        Log.e("Check",""+url);
        VolleyUtils.getVolleyResult(VideosSubtitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getSubCategoryList",response.toString());

                JSONArray array  = null;
                try {
                    array=response.getJSONArray("title");
                    if(array.length()>0){
                        titleItems.clear();
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            String lan= mySharePreference.getLanguage();
                            Log.e("language12",lan);
                            if(lan.equalsIgnoreCase("en")){
                                titleItems.add(new VideosTitleItems(object.getString("out_title"),"#0069D9",object.getString("out_file_Name")));
                            }else{
                                titleItems.add(new VideosTitleItems(object.getString("out_titleII"),"#0069D9",object.getString("out_file_Name")));
                            }

                        }
                        bindViews();
                    }else{
                        dynamicviewcontainer.removeAllViews();
                        Toast.makeText(VideosSubtitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void bindViews() {
        dynamicviewcontainer.removeAllViews();
        List<List<VideosTitleItems>> smallerLists = splittedbyList(titleItems,2);
        for (int k=0;k<smallerLists.size();k++){
            List<VideosTitleItems> sublist= smallerLists.get(k);
            LinearLayout linearLayout=new LinearLayout(VideosSubtitleActivity.this);
            linearLayout.removeAllViews();
            linearLayout.setLayoutParams(llLP);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);

            if(sublist.size()==1) {
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int DeviceTotalWidth = metrics.widthPixels;
                int px = Constants.dpToPx(VideosSubtitleActivity.this, 100);
                px = DeviceTotalWidth - px;
                int val = px / 2;
                Log.e("POS0",val+"--"+px+"--"+DeviceTotalWidth);
                LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                        //android:layout_width='match_parent' an in xml
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        //android:layout_height='wrap_content'
                        Constants.dpToPx(VideosSubtitleActivity.this,50));
                //LinearLayout innerlayout=new LinearLayout(this);
               // innerlayout.setOrientation(LinearLayout.HORIZONTAL);
                LayoutInflater inflater = (LayoutInflater)this.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.vidotitle_itemlayouts, linearLayout,false);
                AppCompatTextView subtitle= child.findViewById(R.id.subTitle);
                subtitle.setText(sublist.get(0).getTitle());
                subtitle.setMovementMethod(new ScrollingMovementMethod());

                //innerlayout.addView(child);
                LinearLayout titleContainer= child.findViewById(R.id.titleContainer);
                AppCompatImageView img= child.findViewById(R.id.correctimg);
                subtitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        img.performClick();
                    }
                });


                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                            Intent i=new Intent(VideosSubtitleActivity.this,VideoPlayActivity.class);
                            i.putExtra("filename",sublist.get(0).getId());
                            startActivity(i);


                    }
                });
               linearLayout.addView(child);

            }else {
                for (int j = 0; j < sublist.size(); j++) {
                    VideosTitleItems titleItems = sublist.get(j);
                    LinearLayout innerlayout=new LinearLayout(VideosSubtitleActivity.this);
                    innerlayout.setOrientation(LinearLayout.HORIZONTAL);
                    innerlayout.setLayoutParams(getLayoutParams(0));
                    LayoutInflater inflater = (LayoutInflater)this.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                    View child = inflater.inflate(R.layout.vidotitle_itemlayouts, innerlayout,false);
                    AppCompatTextView subtitle= child.findViewById(R.id.subTitle);
                    subtitle.setText(titleItems.getTitle());
                    subtitle.setMovementMethod(new ScrollingMovementMethod());

                    innerlayout.addView(child);
                    linearLayout.addView(innerlayout);
                    LinearLayout titleContainer= child.findViewById(R.id.titleContainer);
                    AppCompatImageView img= child.findViewById(R.id.correctimg);
                    subtitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            img.performClick();
                        }
                    });
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(isNetworkAvailable()) {
                                Intent i=new Intent(VideosSubtitleActivity.this,VideoPlayActivity.class);
                                i.putExtra("filename",titleItems.getId());
                                startActivity(i);
                            } else {
                                SharedPreferences preferences;
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                preferences = getSharedPreferences("Videoinfo",0);
                                File pdfFolder = new File(Environment.getExternalStorageDirectory(), "Kvideo");
                                String path = preferences.getString("video",String.valueOf(pdfFolder)+"/"+titleItems.getTitle());
                                intent.setDataAndType(Uri.fromFile(new File(path)), "video/*");
                                Log.e("vurl",path);
                                startActivity(intent);
                            }
                        }
                    });
                }
            }
            dynamicviewcontainer.addView(linearLayout);

        }
    }
    public LinearLayout.LayoutParams getLayoutParams(int pos){
        LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                0,
                //android:layout_height='wrap_content'
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llLP.weight = 1;
        if(pos==0)
        {
            llLP.setMargins(0, 0, 0, 0);
        }else{
            llLP.setMargins(Constants.dpToPx(VideosSubtitleActivity.this, 30), 0, 0, 0);
        }

        return llLP;
    }
    static <VideosTitleItems> List<List<VideosTitleItems>> splittedbyList(List<VideosTitleItems> list, final int L) {
        List<List<VideosTitleItems>> parts = new ArrayList<List<VideosTitleItems>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<VideosTitleItems>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
}