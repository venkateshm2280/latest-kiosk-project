package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.model.VideosTitleItems;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class VideosTitleActivity extends BaseActivity {

    AppCompatImageView logout,home,backBtn;
    int val=8;
    LinearLayout.LayoutParams llLP;
    List<VideosTitleItems> titleItems=new ArrayList<>();
    LinearLayout dynamicviewcontainer;
    AppCompatEditText searchTxt;
    AppCompatImageView searchBtn;
    AppCompatImageView language_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_videos);
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        searchTxt=findViewById(R.id.searchTxt);
        searchBtn=findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!searchTxt.getText().toString().isEmpty()){
                    if(searchTxt.length()>2) {
                        searchResults(searchTxt.getText().toString());
                    }
                }else{
                    Toast.makeText(VideosTitleActivity.this, "Kindly type keyword", Toast.LENGTH_SHORT).show();
                }
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        dynamicviewcontainer=findViewById(R.id.dynamicviewcontainer);
        llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                LinearLayout.LayoutParams.MATCH_PARENT,
                //android:layout_height='wrap_content'
                Constants.dpToPx(VideosTitleActivity.this,90));
        llLP.setMargins(0, Constants.dpToPx(VideosTitleActivity.this,30),0,0);
        llLP.gravity= Gravity.CENTER;
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(VideosTitleActivity.this);
                } else {
                    setLogout(VideosTitleActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    setHomepage(VideosTitleActivity.this);
                } else {
                    setHomepage(VideosTitleActivity.this);
                }
            }
        });

        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==0){
                    getCategoryList();
                }
            }
        });
        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(VideosTitleActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(VideosTitleActivity.this,"en",language_icon,2);
                }
            }
        });
         String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        if(isNetworkAvailable()) {
            getCategoryList();
        } else {
            getOfflineCategoryList();
        }

    }

    private void searchResults(String keyword) {

        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        String url= Constants.Url+"/api/Kiosk_Video/VideoCategoryList?org=keyword&locn=TA&lang="+lan+"&user="+keyword;
        VolleyUtils.getVolleyResult(VideosTitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("searchResults",response.toString());
                JSONArray array  = null;
                titleItems.clear();
                try {
                    array=response.getJSONArray("category");
                    if(array.length()>0){
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            titleItems.add(new VideosTitleItems(object.getString("out_category"),"#0069D9",object.getString("out_categoryId")));
                        }
                        bindViews();
                    }else{
                        dynamicviewcontainer.removeAllViews();
                        Toast.makeText(VideosTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void getOfflineCategoryList() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext().
                openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select a.master_rowid,b.master_name from core_mst_tmaster a " +
                "join core_mst_tmastertranslate b on a.parent_code = b.mastertranslate_parent_code " +
                "and a.master_code = b.mastertranslate_master_code " +
                "where b.mastertranslate_parent_code = 'QCD_TRAINING_VIDEOS' and " +
                "b.mastertranslate_lang_code = ? ",new String[]{lan});
        if (cr.getCount() == 0) {
            Log.e("Data",cr.getString(0));
            Toast.makeText(VideosTitleActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
        }
        while (cr.moveToNext()) {
            if(cr.getCount() > 0) {
                titleItems.add(new VideosTitleItems(cr.getString(1),"#0069D9",cr.getString(0)));
                bindViews();
            }else{
                dynamicviewcontainer.removeAllViews();
                LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                dynamicviewcontainer.addView(child);
            }
        }
    }
    private void getCategoryList() {

        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }

        String url= Constants.Url+"/api/Kiosk_Video/VideoCategoryList?org=flexi&locn=TA&user=kiosk&lang="+lan;
        VolleyUtils.getVolleyResult(VideosTitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getCategoryList",response.toString());
                JSONArray array  = null;
                titleItems.clear();
                try {
                    array=response.getJSONArray("category");
                    if(array.length()>0){
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            titleItems.add(new VideosTitleItems(object.getString("out_category"),"#0069D9",object.getString("out_categoryId")));
                        }
                        bindViews();
                    }else{
                        dynamicviewcontainer.removeAllViews();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindViews() {
        dynamicviewcontainer.removeAllViews();
        List<List<VideosTitleItems>> smallerLists = splittedbyList(titleItems,3);
        Log.e("smallerLists",smallerLists.size()+"");
        for (int k=0;k<smallerLists.size();k++){
            List<VideosTitleItems> sublist= smallerLists.get(k);
                LinearLayout linearLayout=new LinearLayout(this);
                linearLayout.setLayoutParams(llLP);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);

                if(sublist.size()==1) {

                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    int DeviceTotalWidth = metrics.widthPixels;
                    int px = Constants.dpToPx(VideosTitleActivity.this, 100);
                    px = DeviceTotalWidth - px;
                    int val = px / 3;
                    Log.e("POS0",val+"--"+px+"--"+DeviceTotalWidth);
                    Button bt = new Button(this);
                    LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                            //android:layout_width='match_parent' an in xml
                            val,
                            //android:layout_height='wrap_content'
                            Constants.dpToPx(VideosTitleActivity.this,90));
                    bt.setLayoutParams(llLP);
                    int textsize = getResources().getDimensionPixelSize(R.dimen._12ssp);
                    bt.setTextSize(TypedValue.COMPLEX_UNIT_PX, textsize);
                    bt.setTypeface(bt.getTypeface(), Typeface.BOLD);
                    bt.setTextColor(getResources().getColor(R.color.white));

                    GradientDrawable shape = new GradientDrawable();
                    shape.setShape(GradientDrawable.RECTANGLE);
                    shape.setColor(Color.parseColor(sublist.get(0).getColorCode()));
                    shape.setCornerRadius(25f);
                    bt.setBackground(shape);
                   // bt.setBackground(getResources().getDrawable(R.drawable.leftedge_rec));
                    bt.setAllCaps(false);
                    bt.setText(sublist.get(0).getTitle());
                    float density = getResources().getDisplayMetrics().density;
                    int paddingPixel = (int)(5 * density);
                    bt.setPadding(paddingPixel,0,paddingPixel,0);
                    linearLayout.addView(bt);
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            moveActivity(sublist.get(0));
                        }
                    });

                }

                else if(sublist.size()==2) {
                    Log.e("POS2",val+"--");
                    DisplayMetrics metrics = getResources().getDisplayMetrics();
                    int DeviceTotalWidth = metrics.widthPixels;
                    int px = Constants.dpToPx(VideosTitleActivity.this, 100);
                    px = DeviceTotalWidth - px;
                    int val = px / 3;
                    Button bt = new Button(this);
                    //Log.e("sublist",sublist.size()+"=="+val);

                    LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                            //android:layout_width='match_parent' an in xml
                            val,
                            //android:layout_height='wrap_content'
                            Constants.dpToPx(VideosTitleActivity.this,90));
                  //  llLP.weight=1;
                    llLP.setMargins(0, 0, 0, 0);
                    bt.setLayoutParams(llLP);
                    int textsize = getResources().getDimensionPixelSize(R.dimen._12ssp);
                    bt.setTextSize(TypedValue.COMPLEX_UNIT_PX,textsize);
                    bt.setTypeface(bt.getTypeface(), Typeface.BOLD);
                    bt.setTextColor(getResources().getColor(R.color.white));

                    GradientDrawable shape = new GradientDrawable();
                    shape.setShape(GradientDrawable.RECTANGLE);
                    shape.setColor(Color.parseColor(sublist.get(0).getColorCode()));
                    shape.setCornerRadius(25f);
                    bt.setBackground(shape);
                   // bt.setBackground(getResources().getDrawable(R.drawable.leftedge_rec));

                    bt.setAllCaps(false);
                    bt.setText(sublist.get(0).getTitle());
                    float density = getResources().getDisplayMetrics().density;
                    int paddingPixel = (int)(5 * density);
                    bt.setPadding(paddingPixel,0,paddingPixel,0);
                    linearLayout.addView(bt);


                    Button bt2 = new Button(this);
                    LinearLayout.LayoutParams   llLP1 = new LinearLayout.LayoutParams(
                            //android:layout_width='match_parent' an in xml
                            val,
                            //android:layout_height='wrap_content'
                            Constants.dpToPx(VideosTitleActivity.this,90));
                  //  llLP.weight=1;
                    llLP1.setMargins(Constants.dpToPx(VideosTitleActivity.this, 30), 0, 0, 0);
                    bt2.setLayoutParams(llLP1);
                    bt2.setTextSize(TypedValue.COMPLEX_UNIT_PX,textsize);
                  //  bt2.setTypeface(bt.getTypeface(), Typeface.BOLD);
                    bt2.setTextColor(getResources().getColor(R.color.white));

                    GradientDrawable shape2 = new GradientDrawable();
                    shape2.setShape(GradientDrawable.RECTANGLE);
                    shape2.setColor(Color.parseColor(sublist.get(1).getColorCode()));
                    shape2.setCornerRadius(25f);
                    bt2.setBackground(shape2);
                   // bt2.setBackground(getResources().getDrawable(R.drawable.leftedge_rec));
                    bt2.setAllCaps(false);
                    bt2.setText(sublist.get(1).getTitle());
                    bt2.setPadding(paddingPixel,0,paddingPixel,0);
                    linearLayout.addView(bt2);
                    bt2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            moveActivity(sublist.get(1));
                        }
                    });
                    bt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            moveActivity(sublist.get(0));
                        }
                    });
                }
                else {
                    for (int j = 0; j < sublist.size(); j++) {
                        VideosTitleItems titleItems = sublist.get(j);
                        Button bt = new Button(this);
                        bt.setLayoutParams(getLayoutParams(j));
                        int textsize = getResources().getDimensionPixelSize(R.dimen._12ssp);
                        bt.setTextSize(TypedValue.COMPLEX_UNIT_PX,textsize);
                        bt.setTypeface(bt.getTypeface(), Typeface.BOLD);
                        bt.setTextColor(getResources().getColor(R.color.white));

                        GradientDrawable shape = new GradientDrawable();
                        shape.setShape(GradientDrawable.RECTANGLE);
                        shape.setColor(Color.parseColor(titleItems.getColorCode()));
                        shape.setCornerRadius(25f);
                        bt.setBackground(shape);
                       // bt.setBackground(getResources().getDrawable(R.drawable.leftedge_rec));
                        bt.setAllCaps(false);
                        bt.setText(titleItems.getTitle());
                        float density = getResources().getDisplayMetrics().density;
                        int paddingPixel = (int) (5 * density);
                        bt.setPadding(paddingPixel, 0, paddingPixel, 0);
                        linearLayout.addView(bt);
                        bt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                moveActivity(titleItems);
                            }
                        });
                    }
                }
                dynamicviewcontainer.addView(linearLayout);

        }

       // Log.e("DisplayMetrics",DeviceTotalWidth+"--"+val+"--"+px);


    }
    public LinearLayout.LayoutParams getLayoutParams(int pos){
        LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                0,
                //android:layout_height='wrap_content'
                LinearLayout.LayoutParams.MATCH_PARENT);
        llLP.weight = 1;
        if(pos==0)
    {
        llLP.setMargins(0, 0, 0, 0);
    }else{
            llLP.setMargins(Constants.dpToPx(VideosTitleActivity.this, 30), 0, 0, 0);
        }

        return llLP;
    }

    static <VideosTitleItems> List<List<VideosTitleItems>> splittedbyList(List<VideosTitleItems> list, final int L) {
        List<List<VideosTitleItems>> parts = new ArrayList<List<VideosTitleItems>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<VideosTitleItems>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
    public void moveActivity(VideosTitleItems items){
        Intent i=new Intent(VideosTitleActivity.this,VideosSubtitleActivity.class);
        i.putExtra("catIds",items.getId());
        i.putExtra("catname",items.getTitle());
        startActivity(i);
    }

}