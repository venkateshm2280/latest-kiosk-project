package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.adapter.VillageListAdapter;
import com.formar.kioskapp.model.VillageModel;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class WeatherActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    AppCompatImageView logout,home,backBtn;
    private GoogleMap mMap;
    String villagename = "";
    String lavillagecode = "";
    // private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));
    public Marker markerPerth;

    private static final LatLngBounds TAMILNADU = new LatLngBounds( new LatLng(8.189742, 77.399068),new LatLng(12.940322, 80.101697));
   /* private static final LatLngBounds TAMILNADU = new LatLngBounds(
            new LatLng(6.4626999, 68.1097),
            new LatLng(35.513327, 97.39535869999999)
    );*/

    private static final CameraPosition TAMILNADU_CAMERA = new CameraPosition.Builder()
            .target(new LatLng(8.189742, 77.399068)).zoom(18.0f).bearing(0).tilt(0).build();
    AppCompatImageView language_icon;
    AppCompatEditText villagetxt,taluktxt,districtxt,statetxt,datetxt,periodFromtxt,periodtoTxt;
    AppCompatButton submitbtn;
    List<VillageModel> villageModelList=new ArrayList<>();
    List<VillageModel> sampleList=new ArrayList<>();
    private VillageListAdapter adapter;
    public String loginType="";
    public String kvillageId="";
    public  String kdistrictId="";
    String villageode="";
    public String villagesname="";
    public String vnames="";
    public String kvid="";
    public String kdid="";
    public String kamarajvillageid="";
    AppCompatSpinner wtypespinner;
    ArrayList<String> wlist = new ArrayList<>();
    private int mYear, mMonth, mDay, mHour, mMinute;
    AppCompatTextView weathertxthint;
    private LinearLayout ll_hint_spinner;
    private String wtypess="";
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public String viewType="1";
    AppCompatTextView Wtitle;
    LinearLayout dateContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_weather);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        loginType=sharedpreferences.getString("login","");
        kvillageId=sharedpreferences.getString("kvillageid","");
        kdistrictId=sharedpreferences.getString("kdistrictid","");
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        villagetxt=findViewById(R.id.villagetxt);
        wtypespinner=findViewById(R.id.wtypetxt);
        datetxt=findViewById(R.id.datetxt);
        periodFromtxt=findViewById(R.id.periodfromtxt);
        periodtoTxt=findViewById(R.id.periodtotxt);
        taluktxt=findViewById(R.id.taluktxt);
        districtxt=findViewById(R.id.disticttxt);
        statetxt=findViewById(R.id.statetxt);
        submitbtn=findViewById(R.id.submitbtn);
        weathertxthint=findViewById(R.id.weathertxthint);
        ll_hint_spinner = findViewById(R.id.ll_hint_spinner);
        dateContainer = findViewById(R.id.dates_container);
        Wtitle = findViewById(R.id.w_title);
        viewType=getIntent().getStringExtra("viewType");
        wlist.add("Forecast");
        wlist.add("Past");
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(isNetworkAvailable()) {
            if(viewType.equals("2")){
                Log.e("viewtype",viewType);
                Wtitle.setText(getResources().getString(R.string.cropadvis_title));
                dateContainer.setVisibility(View.VISIBLE);
            }else{
                Wtitle.setText(getResources().getString(R.string.weather_title));
                dateContainer.setVisibility(View.GONE);
            }
        } else {
            if(viewType.equals("2")){
                Wtitle.setText(getResources().getString(R.string.cropadvis_title));
                dateContainer.setVisibility(View.VISIBLE);
            }else{
                Wtitle.setText(getResources().getString(R.string.weather_title));
                dateContainer.setVisibility(View.GONE);
            }
        }
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, wlist);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
        wtypespinner.setAdapter(adapter);
       /* wtypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                Log.e("onItemSelected",wlist.get(pos));
                wtypespinner.setSelection(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);
        formattedDate= getPreviousDate(formattedDate);
        datetxt.setText(formattedDate);
        wtypess="1";


        periodFromtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    showPeriodfromialog();
                } else {
                    showOfflineperiodfromialog();
                }
            }
        }); periodtoTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    showPeriodtoialog();
                } else {
                    showOfflinePeriodtoialog();
                }
            }
        });

        datetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showDatedialog();
            }
        });
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(datetxt.getText().toString().isEmpty()){
                    Toast.makeText(WeatherActivity.this, "Kindly select date!.", Toast.LENGTH_SHORT).show();
                }else*/
                if(isNetworkAvailable()) {
                    if(viewType.equals("2")) {
                        if (periodFromtxt.getText().toString().isEmpty() || periodtoTxt.getText().toString().isEmpty()) {
                            Toast.makeText(WeatherActivity.this, "Kindly select from and to Date!.", Toast.LENGTH_SHORT).show();
                        }else if(villageode.isEmpty()){
                            Toast.makeText(WeatherActivity.this, "Kindly select village!.", Toast.LENGTH_SHORT).show();
                        }else{
                            kamarajvillageid = sharedpreferences.getString("kvillageid","");
                            Intent i = new Intent(WeatherActivity.this, WeathersViewActivity.class);
                            i.putExtra("VillageId",villageode);
                            i.putExtra("kvillageid",kamarajvillageid);
                            Log.e("vill",kvid);
                            Log.e("vill",villageode);
                            i.putExtra("date",datetxt.getText().toString());
                            i.putExtra("wtype",wtypess);
                            i.putExtra("viewtype",viewType);
                            i.putExtra("fromdates",periodFromtxt.getText().toString());
                            i.putExtra("todates",periodtoTxt.getText().toString());
                            startActivity(i);

                            // Log.e("vill",datetxt.getText().toString());
                        }
                    }else{
                        if(villageode.isEmpty()){
                            Toast.makeText(WeatherActivity.this, "Kindly select village!.", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            kamarajvillageid = sharedpreferences.getString("kvillageid","");
                            Intent i = new Intent(WeatherActivity.this, WeathersViewActivity.class);
                            i.putExtra("VillageId",villageode);
                            i.putExtra("kamvillage",kamarajvillageid);
                            Log.e("villa",kvid);
                            Log.e("villa",villageode);
                            i.putExtra("date",datetxt.getText().toString());
                            i.putExtra("wtype",wtypess);
                            i.putExtra("viewtype",viewType);
                            i.putExtra("fromdates",periodFromtxt.getText().toString());
                            i.putExtra("todates",periodtoTxt.getText().toString());
                            startActivity(i);
                            // Log.e("vill",kvid);
                        }


                    }
                } else {
                    if(viewType.equals("2") && loginType.equals("2")) {
                        if (periodFromtxt.getText().toString().isEmpty() || periodtoTxt.getText().toString().isEmpty()) {
                            Toast.makeText(WeatherActivity.this, "Kindly select from and to Date!.", Toast.LENGTH_SHORT).show();
                        }else if(villageode.isEmpty()){
                            Toast.makeText(WeatherActivity.this, "Kindly select village!.", Toast.LENGTH_SHORT).show();
                        }else{
                            Intent i = new Intent(WeatherActivity.this, WeathersViewActivity.class);
                            i.putExtra("VillageId",villageode);

                            i.putExtra("kvillageid",kvid);
                            Log.e("vill",kvid);
                            Log.e("vill",villageode);
                            i.putExtra("date",datetxt.getText().toString());
                            i.putExtra("wtype",wtypess);
                            i.putExtra("viewtype",viewType);
                            i.putExtra("fromdates",periodFromtxt.getText().toString());
                            i.putExtra("todates",periodtoTxt.getText().toString());
                            startActivity(i);
                        }
                    }else{
                        if(villageode.isEmpty()){
                            Toast.makeText(WeatherActivity.this, "Kindly select village!.", Toast.LENGTH_SHORT).show();
                        }else {
                            Intent i = new Intent(WeatherActivity.this, WeathersViewActivity.class);
                            i.putExtra("VillageId",villageode);
                            i.putExtra("kvillageid",kvid);
                            Log.e("vill",kvid);
                            Log.e("vill",villageode);
                            i.putExtra("date",datetxt.getText().toString());
                            //Log.e("codes",datetxt.getText().toString());
                            i.putExtra("wtype",wtypess);
                            i.putExtra("viewtype",viewType);
                            i.putExtra("fromdates",periodFromtxt.getText().toString());
                            i.putExtra("todates",periodtoTxt.getText().toString());
                            startActivity(i);
                        }


                    }
                }


            }
        });

        villagetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    showVillageDialog();
                } else {
                    showOfflineVillageDialog();
                }

            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(WeatherActivity.this);
                } else {
                    setLogout(WeatherActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setHomepage(WeatherActivity.this);
                } else {
                    setHomepage(WeatherActivity.this);
                }
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(WeatherActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(WeatherActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        if(isNetworkAvailable()) {
            Log.e("Language",lan);
            getVillageDetails();
        } else {
            getOfflineVillageDetails();
        }

        wtypespinner.setSelected(false);
        wtypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                wtypespinner.setSelection(position);
                setLinearVisibility(false);
                ((TextView) view).setTextColor(
                        getResources().getColorStateList(R.color.weather_bg_color)
                );
                //  ((TextView) view).setTypeface(fontStyle);
                ((TextView) view).setGravity(Gravity.CENTER);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLinearVisibility(true);
            }
        });
        if(isNetworkAvailable()) {
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String loginType= sharedpreferences.getString("login","");
            if(!loginType.equals("2")){
                SharedPreferences.Editor editor = sharedpreferences.edit();
                villageode=sharedpreferences.getString("fvillage_code","");
                villagename = sharedpreferences.getString("fvillage","");
                vnames = sharedpreferences.getString("fvillage","");
                kamarajvillageid = sharedpreferences.getString("kvillageid","");
                kdid = sharedpreferences.getString("kamdistid","");
                editor.putString("villnames",vnames);
                editor.putString("kvillageid",kamarajvillageid);
                editor.putString("kamdistid",kdid);
                editor.commit();
                Log.e("vills", vnames);
                Log.e("vills", kamarajvillageid);
                Log.e("vills", kdid);
                if(lan.equalsIgnoreCase("en")){
                    villagetxt.setText(sharedpreferences.getString("fvillage",""));
                    taluktxt.setText(sharedpreferences.getString("ftaluk",""));
                    districtxt.setText(sharedpreferences.getString("fdistname",""));
                    statetxt.setText(sharedpreferences.getString("state_name",""));
                }else{
                    villagetxt.setText(sharedpreferences.getString("lafarmervillage",""));
                    taluktxt.setText(sharedpreferences.getString("lafarmertaluk",""));
                    districtxt.setText(sharedpreferences.getString("lafarmerdist",""));
                    statetxt.setText(sharedpreferences.getString("lafarmerstate",""));
                }

            } else {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                villageode=sharedpreferences.getString("fvillage_code","");
                //villagetxt.setText(sharedpreferences.getString("fvillage",""));
                vnames = sharedpreferences.getString("fvillage","");
                kamarajvillageid = sharedpreferences.getString("kvillageid","");
                kdid = sharedpreferences.getString("kamdistid","");
                editor.putString("villnames",vnames);
                editor.putString("kvillageid",kamarajvillageid);
                editor.putString("kamdistid",kdid);
                editor.commit();
                Log.e("vills", vnames);
                Log.e("vills", kamarajvillageid);
                Log.e("vills", kdid);
                if(lan.equalsIgnoreCase("en")){
                    villagetxt.setText(sharedpreferences.getString("fvillage",""));
                    taluktxt.setText(sharedpreferences.getString("ftaluk",""));
                    districtxt.setText(sharedpreferences.getString("fdistname",""));
                    statetxt.setText(sharedpreferences.getString("state_name",""));
                }else{
                    villagetxt.setText(sharedpreferences.getString("lafarmervillage",""));
                    taluktxt.setText(sharedpreferences.getString("lafarmertaluk",""));
                    districtxt.setText(sharedpreferences.getString("lafarmerdist",""));
                    statetxt.setText(sharedpreferences.getString("lafarmerstate",""));
                }
            }
        } else {
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String loginType= sharedpreferences.getString("login","");
            if(!loginType.equals("2")){
                SharedPreferences.Editor editor = sharedpreferences.edit();
                villageode=sharedpreferences.getString("fvillage_code","");
                //villagetxt.setText(sharedpreferences.getString("fvillage",""));
                vnames = sharedpreferences.getString("fvillage","");
                kamarajvillageid = sharedpreferences.getString("kvillageid","");
                kdid = sharedpreferences.getString("kamdistid","");
                editor.putString("villnames",vnames);
                editor.putString("kvillageid",kamarajvillageid);
                editor.putString("kamdistid",kdid);
                editor.commit();
                Log.e("vills", vnames);
                Log.e("vills", kamarajvillageid);
                Log.e("vills", kdid);
                if(lan.equalsIgnoreCase("en")){
                    villagetxt.setText(sharedpreferences.getString("fvillage",""));
                    taluktxt.setText(sharedpreferences.getString("ftaluk",""));
                    districtxt.setText(sharedpreferences.getString("fdistname",""));
                    statetxt.setText(sharedpreferences.getString("state_name",""));
                }else{
                    villagetxt.setText(sharedpreferences.getString("lafarmervillage",""));
                    taluktxt.setText(sharedpreferences.getString("lafarmertaluk",""));
                    districtxt.setText(sharedpreferences.getString("lafarmerdist",""));
                    statetxt.setText(sharedpreferences.getString("lafarmerstate",""));
                }
            } else {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                villageode=sharedpreferences.getString("fvillage_code","");
                //villagetxt.setText(sharedpreferences.getString("fvillage",""));
                vnames = sharedpreferences.getString("fvillage","");
                kamarajvillageid = sharedpreferences.getString("kvillageid","");
                kdid = sharedpreferences.getString("kamdistid","");
                editor.putString("villnames",vnames);
                editor.putString("kvillageid",kamarajvillageid);
                editor.putString("kamdistid",kdid);
                editor.commit();
                Log.e("vills", vnames);
                Log.e("vills", kamarajvillageid);
                Log.e("vills", kdid);
                if(lan.equalsIgnoreCase("en")){
                    villagetxt.setText(sharedpreferences.getString("fvillage",""));
                    taluktxt.setText(sharedpreferences.getString("ftaluk",""));
                    districtxt.setText(sharedpreferences.getString("fdistname",""));
                    statetxt.setText(sharedpreferences.getString("state_name",""));
                }else{
                    villagetxt.setText(sharedpreferences.getString("lafarmervillage",""));
                    taluktxt.setText(sharedpreferences.getString("lafarmertaluk",""));
                    districtxt.setText(sharedpreferences.getString("lafarmerdist",""));
                    statetxt.setText(sharedpreferences.getString("lafarmerstate",""));
                }
            }
        }

    }
    public void setLinearVisibility(boolean visible) {
        if (visible) {
            ll_hint_spinner.setVisibility(View.VISIBLE);
        } else {
            ll_hint_spinner.setVisibility(View.INVISIBLE);
        }
    }

    private String getPreviousDate(String inputDate){
        if(isNetworkAvailable()) {
            SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(inputDate);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.DATE, -2);
                inputDate = format.format(c.getTime());
                Log.d("asd", "selected date : "+inputDate);

                System.out.println(date);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                inputDate ="";
            }
            return inputDate;
        } else {
            SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(inputDate);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.DATE, -2);
                inputDate = format.format(c.getTime());
                Log.d("asd", "selected date : "+inputDate);

                System.out.println(date);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                inputDate ="";
            }
            return inputDate;
        }
    }
    public static boolean isDateAfter(String startDate,String endDate)
    {
        try
        {
            String myFormatString = "yyyy-MM-dd"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            if (date1.after(startingDate))
                return true;
            else
                return false;
        }
        catch (Exception e)
        {

            return false;
        }
    }

    private void showOfflineperiodfromialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String month="",days = "";
                        if(monthOfYear < 10){
                            month = "0" + String.valueOf((monthOfYear + 1));
                        }else{
                            month=String.valueOf((monthOfYear + 1));
                        }
                        if(dayOfMonth < 10){
                            days  = "0" +  String.valueOf(dayOfMonth);
                        }else{
                            days=String.valueOf(dayOfMonth);
                        }
                        String selecteddate= days+ "-" + month + "-" + year;
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                        Date sdate= null;
                        String fromdate="";
                        try {
                            sdate = sdf.parse(selecteddate);
                            fromdate=dateFormat2.format(sdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        periodFromtxt.setText(fromdate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    private void showPeriodfromialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String month="",days = "";
                        if(monthOfYear < 10){
                            month = "0" + String.valueOf((monthOfYear + 1));
                        }else{
                            month=String.valueOf((monthOfYear + 1));
                        }
                        if(dayOfMonth < 10){
                            days  = "0" +  String.valueOf(dayOfMonth);
                        }else{
                            days=String.valueOf(dayOfMonth);
                        }
                        String selecteddate= days+ "-" + month + "-" + year;
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                        Date sdate= null;
                        String fromdate="";
                        try {
                            sdate = sdf.parse(selecteddate);
                            fromdate=dateFormat2.format(sdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        periodFromtxt.setText(fromdate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    private void showOfflinePeriodtoialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String month="",days = "";
                        if(monthOfYear < 10){
                            month = "0" + String.valueOf((monthOfYear + 1));
                        }else{
                            month=String.valueOf((monthOfYear + 1));
                        }
                        if(dayOfMonth < 10){
                            days  = "0" +  String.valueOf(dayOfMonth);
                        }else{
                            days=String.valueOf(dayOfMonth);
                        }
                        String selecteddate= days+ "-" + month + "-" + year;
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                        Date sdate= null;
                        String todate="";
                        try {
                            sdate = sdf.parse(selecteddate);
                            todate=dateFormat2.format(sdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if(periodFromtxt.getText().toString().isEmpty()){
                            Toast.makeText(WeatherActivity.this, "Kindly select from date!..", Toast.LENGTH_SHORT).show();
                        }else{
                            CheckDates(todate,periodFromtxt.getText().toString());
                            if(wtypess.equals("0")){
                                periodtoTxt.setText("");
                                Toast.makeText(WeatherActivity.this, "To Date not valid!..", Toast.LENGTH_SHORT).show();
                            }else {
                                periodtoTxt.setText(todate);
                            }

                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    private void showPeriodtoialog() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String month="",days = "";
                        if(monthOfYear < 10){
                            month = "0" + String.valueOf((monthOfYear + 1));
                        }else{
                            month=String.valueOf((monthOfYear + 1));
                        }
                        if(dayOfMonth < 10){
                            days  = "0" +  String.valueOf(dayOfMonth);
                        }else{
                            days=String.valueOf(dayOfMonth);
                        }
                        String selecteddate= days+ "-" + month + "-" + year;
                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                        Date sdate= null;
                        String todate="";
                        try {
                            sdate = sdf.parse(selecteddate);
                            todate=dateFormat2.format(sdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if(periodFromtxt.getText().toString().isEmpty()){
                            Toast.makeText(WeatherActivity.this, "Kindly select from date!..", Toast.LENGTH_SHORT).show();
                        }else{
                            CheckDates(todate,periodFromtxt.getText().toString());
                            if(wtypess.equals("0")){
                                periodtoTxt.setText("");
                                Toast.makeText(WeatherActivity.this, "To Date not valid!..", Toast.LENGTH_SHORT).show();
                            }else {
                                periodtoTxt.setText(todate);
                            }

                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    public boolean CheckDates(String startDate, String endDate) {
        Log.e("CheckDates",startDate+"\n"+endDate);
        SimpleDateFormat dfDate = new SimpleDateFormat("dd-MM-yyyy");

        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
                Log.e("CheckDates","isBefore");
                wtypess="0";
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
                Log.e("CheckDates","equal");
                wtypess="1";
            } else {
                b = false; // If start date is after the end date.
                Log.e("CheckDates","after");
                wtypess="2";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return b;
    }

    private void showOfflineVillageDialog () {
        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        View customView = inflater.inflate(R.layout.village_dialog_layout, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(WeatherActivity.this);
        builder.setView(customView);
        builder.setTitle("Village List");
        AppCompatEditText searchtxt=customView.findViewById(R.id.searchtxt);
        RecyclerView recyclerView=customView.findViewById(R.id.searchRecyclerview);
        sampleList=villageModelList;
        Log.e("showVillageDialog",sampleList.size()+"");
        adapter=new VillageListAdapter(WeatherActivity.this,sampleList);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(WeatherActivity.this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        AlertDialog alertDialog = builder.show();

        adapter.setCallback(new VillageListAdapter.VillageItemclick() {
            @Override
            public void setItemClick(int pos) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                villageode=sampleList.get(pos).getOutMasterCode();
                villagetxt.setText(sampleList.get(pos).getOutMasterDescription());
                vnames = sampleList.get(pos).getOutMasterDescription();
                kvid = sampleList.get(pos).getOutKamarajVillageId();
                kdid = sampleList.get(pos).getOutKamarajDistrictId();
                editor.putString("villnames",vnames);
                editor.putString("kvillageid",kvid);
                editor.putString("kamdistid",kdid);
                editor.commit();
                Log.e("code1",villageode);
                Log.e("code1",vnames);
                Log.e("code1",kvid);
                Log.e("code1",kdid);
                getOfflineStateDetails(villageode);
                alertDialog.cancel();
            }
        });
    }

    private void showVillageDialog() {
        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        View customView = inflater.inflate(R.layout.village_dialog_layout, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(WeatherActivity.this);
        builder.setView(customView);
        builder.setTitle("Village List");
        AppCompatEditText searchtxt=customView.findViewById(R.id.searchtxt);
        RecyclerView recyclerView=customView.findViewById(R.id.searchRecyclerview);
        sampleList=villageModelList;
        Log.e("showVillageDialog",sampleList.size()+"");
        adapter=new VillageListAdapter(WeatherActivity.this,sampleList);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(WeatherActivity.this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        AlertDialog alertDialog = builder.show();

        adapter.setCallback(new VillageListAdapter.VillageItemclick() {
            @Override
            public void setItemClick(int pos) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                villageode=sampleList.get(pos).getOutMasterCode();
                String lan= mySharePreference.getLanguage();
                if(lan.equalsIgnoreCase("en")){
                    villagetxt.setText(sampleList.get(pos).getOutMasterDescription());
                    villagename = sampleList.get(pos).getOutMasterDescription();
                    vnames = sampleList.get(pos).getOutMasterDescription();
                    kvid = sampleList.get(pos).getOutKamarajVillageId();
                    kdid = sampleList.get(pos).getOutKamarajDistrictId();
                } else {
                    villagetxt.setText(sampleList.get(pos).getOutMasterLlDescription());
                    villagename = sampleList.get(pos).getOutMasterLlDescription();
                    vnames = sampleList.get(pos).getOutMasterLlDescription();
                    kvid = sampleList.get(pos).getOutKamarajVillageId();
                    kdid = sampleList.get(pos).getOutKamarajDistrictId();
                }
                editor.putString("villnames",vnames);
                editor.putString("kvillageid",kvid);
                editor.putString("kamdistid",kdid);
                editor.commit();
                Log.e("code",villageode);
                Log.e("code",vnames);
                Log.e("code",kvid);
                Log.e("code",kdid);
                getStateDetails(villageode);
                alertDialog.cancel();
            }

        });


    }

    private void getOfflineStateDetails (String id) {
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kiosk_mst_syncmaster",null);
        Log.e("state", id);
        if(cr.getCount() == 0) {
            Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_LONG).show();
            return;
        }
        while (cr.moveToNext()) {
            if (cr.getCount() > 0) {
                if(id.equals(cr.getString(1))) {
                    taluktxt.setText(cr.getString(8));
                    districtxt.setText(cr.getString(6));
                    statetxt.setText("Tamil Nadu");
                }
            }
        }
    }
    private void getStateDetails(String dependCode) {
        String lan= mySharePreference.getLanguage();
        String lang;
        Log.e("Language",lan);
        if(lan.equalsIgnoreCase("en")){
            lang = "en_US";
        } else {
            lang = "ta_IN";
        }
        String url= Constants.Url+"/api/Web_KioskSetup/Kioskonchange?org=test&locn=TA&user=admin&lang="+lang+"&Depend_Code="+dependCode;
        VolleyUtils.getVolleyResult(WeatherActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getStateDetails",response.toString());
                try {
                    JSONObject newObj=response.getJSONObject("context");
                    JSONArray array=newObj.getJSONArray("list");
                    if(array.length()>0){
                        for (int k=0;k<array.length();k++) {
                            JSONObject object = array.getJSONObject(k);
                            String lan= mySharePreference.getLanguage();
                            if(k==0)
                            {
                                if(lan.equalsIgnoreCase("en")){
                                    taluktxt.setText(object.getString("tk_name"));
                                    districtxt.setText(object.getString("dt_name"));
                                    statetxt.setText(object.getString("st_name"));
                                } else {
                                    taluktxt.setText(object.getString("tk_nameII"));
                                    districtxt.setText(object.getString("dt_nameII"));
                                    statetxt.setText(object.getString("st_nameII"));
                                }

                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getOfflineVillageDetails () {
        String Village = "QCD_UN_VILLAGE";
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        //Cursor cr = db.rawQuery("select * from core_mst_village_details where out_parent_code = ?",
        //new String[]{Village});
        Cursor cr = db.rawQuery("select * from kiosk_mst_syncmaster",null);
        if(cr.getCount() == 0) {
            Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_LONG).show();
            return;
        }
        while(cr.moveToNext()) {
            if(cr.getCount() > 0) {
                Log.e("resu", cr.getString(0));
                villageModelList.add(new VillageModel(cr.getString(1), cr.getString(1),
                        cr.getString(5), cr.getString(5),cr.getString(1),cr.getString(2)));
            }
        }
    }
    private void getVillageDetails() {
        String url= Constants.Url+"/api/Web_MasterDefinition/mastercode_screenid?org=test&locn=TA&user=admin&lang=ta_IN&screen_code=KioskSetup";
        VolleyUtils.getVolleyResult(WeatherActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getVillageDetails",response.toString());
                try {
                    JSONArray array=response.getJSONArray("detail");
                    if(array.length()>0){
                        villageModelList.clear();
                        for (int k=0;k<array.length();k++) {
                            JSONObject object = array.getJSONObject(k);
                            if (object.getString("out_parent_code").equals("QCD_UN_VILLAGE")) {
                                String lan= mySharePreference.getLanguage();
                                if(lan.equalsIgnoreCase("en")){
                                    villageModelList.add(new VillageModel(object.getString("out_parent_code"), object.getString("out_master_code"), object.getString("out_master_description"), object.getString("out_master_ll_description")
                                            ,object.getString("kamaraj_village_id"),object.getString("kamaraj_district_id")));
                                }else{
                                    villageModelList.add(new VillageModel(object.getString("out_parent_code"), object.getString("out_master_code"), object.getString("out_master_ll_description"), object.getString("out_master_ll_description")
                                            ,object.getString("kamaraj_village_id"),object.getString("kamaraj_district_id")));
                                }
                                //Log.e("rest",object.getString("out_master_description"));
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
/*
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

//some code
            float minZoomPreference = 10.0f;
            float maxZoomPreference = 14.0f;
            mMap.setMinZoomPreference(minZoomPreference);
            mMap.setMaxZoomPreference(maxZoomPreference);
            LatLngBounds Bataan = new LatLngBounds(new LatLng(8.189742, 77.399068),new LatLng(12.940322, 80.101697));

            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(Bataan,0));
            mMap.setLatLngBoundsForCameraTarget(Bataan);

        }*/

        // Add a marker in Sydney and move the camera
        //*  mMap.setLatLngBoundsForCameraTarget(TAMILNADU);
        mMap.setMinZoomPreference(7.0f);
        mMap.setMaxZoomPreference(10.0f);
        LatLng sydney = new LatLng(8.189742, 77.399068);
        LatLng mountainView = new LatLng(12.940322, 80.101697);
        mMap.setLatLngBoundsForCameraTarget(TAMILNADU);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(mountainView )      // Sets the center of the map to Mountain View
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        //   mMap.setLatLngBoundsForCameraTarget(TAMILNADU);
    /*  markerPerth = mMap.addMarker(new MarkerOptions()
                .position(mountainView)
                .title("Perth"));
        markerPerth.setTag(0);*/
        //  mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng));
                Toast.makeText(WeatherActivity.this, ""+latLng.latitude+"\n"+latLng.longitude, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        // Retrieve the data from the marker.
        Integer clickCount = (Integer) marker.getTag();

        // Check if a click count was set, then display the click count.
        if (clickCount != null) {
            clickCount = clickCount + 1;
            marker.setTag(clickCount);
            Toast.makeText(this,
                    marker.getTitle() +
                            " has been clicked " + clickCount + " times.",
                    Toast.LENGTH_SHORT).show();
        }

        // Return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    public void isCompareDate(String seleteddate){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String getCurrentDateTime = sdf.format(c.getTime());
        String getMyTime=seleteddate;

        Log.d("getCurrentDateTime",getCurrentDateTime+"--"+getMyTime);

        if (getCurrentDateTime.compareTo(getMyTime) < 0)
        {
            Log.d("Return","greater date of current date ");

        }
        else
        {
            Log.d("Return","getMyTime older than getCurrentDateTime ");
        }

    }
    public Boolean isNetworkAvailable () {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}