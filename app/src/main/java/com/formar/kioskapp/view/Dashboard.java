package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.formar.kioskapp.Irrigationlistactivity;
import com.formar.kioskapp.R;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;

import static com.formar.kioskapp.utils.Constants.setLogout;

public class Dashboard extends BaseActivity implements View.OnClickListener {

    AppCompatButton Soilbtn,Weatherbtn,videobtn,faqbtn,govbtn,istatus;
    AppCompatImageView logoutTxt;
    AppCompatImageView language_icon;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(isNetworkAvailable()) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = this.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.white));
            }
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String loginType= sharedpreferences.getString("login","");
            if(loginType.equals("2")){
                setContentView(R.layout.activity_dashboard2);
            }else{
                setContentView(R.layout.activity_dashboard);
            }
        } else {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = this.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(this.getResources().getColor(R.color.white));
            }
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String loginType= sharedpreferences.getString("login","");
            if(loginType.equals("2")){
                setContentView(R.layout.activity_dashboard2);
            }else{
                setContentView(R.layout.activity_dashboard);
            }
        }
        initViews();
    }

    private void initViews() {
        Soilbtn=findViewById(R.id.soilBtn);
        Weatherbtn=findViewById(R.id.weatherBtn);
        videobtn=findViewById(R.id.videosBtn);
        faqbtn=findViewById(R.id.faqBtn);
        govbtn=findViewById(R.id.govBtn);
        istatus=findViewById(R.id.istatus);
        logoutTxt=findViewById(R.id.logouttxt);
        Soilbtn.setOnClickListener(this);
        Weatherbtn.setOnClickListener(this);
        videobtn.setOnClickListener(this);
        faqbtn.setOnClickListener(this);
        govbtn.setOnClickListener(this);
        logoutTxt.setOnClickListener(this);
        istatus.setOnClickListener(this);
        language_icon=findViewById(R.id.language_icon);

        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(Dashboard.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(Dashboard.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        logoutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setLogout(Dashboard.this);
                } else {
                    setLogout(Dashboard.this);
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.soilBtn:
                Intent soilIntent=new Intent(Dashboard.this,SoilListActivity.class);
                startActivity(soilIntent);
                break;
            case R.id.weatherBtn:
                Intent weaIntent=new Intent(Dashboard.this,WeatherSelectActivity.class);
                startActivity(weaIntent);
                break;
            case R.id.videosBtn:
                Intent videIntent=new Intent(Dashboard.this, VideosTitleActivity.class);
                startActivity(videIntent);
                break;
            case R.id.faqBtn:
                Intent faqIntent=new Intent(Dashboard.this,FaqsActivity.class);
                startActivity(faqIntent);
                break;
            case R.id.govBtn:
                Intent govIntent=new Intent(Dashboard.this,GovSchemesActivity.class);
                startActivity(govIntent);
                break;
            case R.id.istatus:
                Intent isat=new Intent(Dashboard.this, Irrigationlistactivity.class);
                startActivity(isat);
                break;
            case R.id.logouttxt:
               finish();
                break;
        }
    }
    public Boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}