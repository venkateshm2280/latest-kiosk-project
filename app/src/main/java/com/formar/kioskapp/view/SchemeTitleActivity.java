package com.formar.kioskapp.view;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.formar.kioskapp.Interfaces.VolleyListener;
import com.formar.kioskapp.R;
import com.formar.kioskapp.model.VideosTitleItems;
import com.formar.kioskapp.utils.BaseActivity;
import com.formar.kioskapp.utils.Constants;
import com.formar.kioskapp.utils.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.formar.kioskapp.utils.Constants.setHomepage;
import static com.formar.kioskapp.utils.Constants.setLogout;

public class SchemeTitleActivity extends BaseActivity {
    AppCompatImageView logout,home;
    List<VideosTitleItems> titleItems=new ArrayList<>();
    String catId="";
    String catname="";
    AppCompatTextView title;
    LinearLayout dynamicviewcontainer;
    AppCompatImageView backBtn;
    private LinearLayout.LayoutParams llLP=null;
    AppCompatImageView language_icon;
    AppCompatEditText searchTxt;
    AppCompatImageView searchBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.white));
        }
        setContentView(R.layout.activity_scheme_title);
        logout=findViewById(R.id.logouttxt);
        home=findViewById(R.id.hometxt);
        title=findViewById(R.id.title);
        dynamicviewcontainer=findViewById(R.id.dynamicviewcontainer);
        catId=getIntent().getStringExtra("catIds");
        catname=getIntent().getStringExtra("catname");
        title.setText(catname);
        llLP = new LinearLayout.LayoutParams(
                //android:layout_width='match_parent' an in xml
                LinearLayout.LayoutParams.MATCH_PARENT,
                //android:layout_height='wrap_content'
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llLP.setMargins(0, Constants.dpToPx(SchemeTitleActivity.this,20),0,0);
        llLP.gravity= Gravity.CENTER;
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()){
                    setLogout(SchemeTitleActivity.this);
                }else {
                    setLogout(SchemeTitleActivity.this);
                }
            }
        }); home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    setHomepage(SchemeTitleActivity.this);
                }else {
                    setHomepage(SchemeTitleActivity.this);
                }
            }
        });
        backBtn=findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    finish();
                } else {
                    finish();
                }
            }
        });
        if (isNetworkAvailable()) {
            getSubCategoryList();
        } else {
            getOfflineSubCategoryList();
        }

        language_icon=findViewById(R.id.language_icon);
        language_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan= mySharePreference.getLanguage();
                Log.e("lan",lan);
                if(lan.equals("en")){
                    Constants.setLanguage(SchemeTitleActivity.this,"ta",language_icon,2);
                }else{
                    Constants.setLanguage(SchemeTitleActivity.this,"en",language_icon,2);
                }
            }
        });
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")) {
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.tamil));
        }else{
            language_icon.setImageDrawable(getResources().getDrawable(R.drawable.english));
        }
        searchTxt=findViewById(R.id.searchTxt);
        searchBtn=findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!searchTxt.getText().toString().isEmpty()){
                    if(searchTxt.length()>2) {
                        if(isNetworkAvailable()) {
                            getSearchResults(searchTxt.getText().toString());
                        } else {
                            getOfflineSearchResults(searchTxt.getText().toString());
                        }
                    }
                }else{
                    Toast.makeText(SchemeTitleActivity.this, "Kindly type keyword", Toast.LENGTH_SHORT).show();
                }
            }
        });
        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==0){
                    if(isNetworkAvailable()) {
                        getSubCategoryList();
                    } else {
                        getOfflineSubCategoryList();
                    }
                }
            }
        });


    }
    public Boolean isNetworkAvailable(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    private void getOfflineSubCategoryList() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kiosk_trn_tscheme where scheme_category = ?",
                new String[] {catId});
        if(cr.getCount() == 0) {
            Toast.makeText(SchemeTitleActivity.this,"No Records Found",Toast.LENGTH_LONG).show();
        }
        while (cr.moveToNext()) {
            if (cr.getCount() > 0) {
                //for (int k=0;k<cr.getCount();k++){
                Log.e("Data", cr.getString(4));
                Log.e("Data", cr.getString(0));
                if (lan.equals("en_US")) {
                    titleItems.add(new VideosTitleItems(cr.getString(4), "#0069D9", cr.getString(0)));
                } else {
                    titleItems.add(new VideosTitleItems(cr.getString(13), "#0069D9", cr.getString(0)));
                }

                // }
                bindViews();
            } else {
                dynamicviewcontainer.removeAllViews();
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer, false);
                dynamicviewcontainer.addView(child);
            }
        }
    }
    private void getSubCategoryList() {
        String lan= mySharePreference.getLanguage();
        if(lan.equals("en")){
            lan="en_US";
        }else{
            lan="ta_IN";
        }
     //   http://localhost:49462/api/Kiosk_Scheme/Schemelist?org=flexi&locn=TA&lang=en_US&user=Kiosk&In_schemecat_Id=1073&keyword=Test

        String url= Constants.Url+"/api/Kiosk_Scheme/Schemelist?org=flexi&locn=TA&user=kiosk&lang="+lan+"&In_schemecat_Id="+catId+"&keyword=";
        VolleyUtils.getVolleyResult(SchemeTitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getSubCategoryList",response.toString());
                JSONArray array  = null;
                try {
                    array=response.getJSONArray("schemelist");
                    if(array.length()>0){
                        titleItems.clear();
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            titleItems.add(new VideosTitleItems(object.getString("out_scheme_name"),"#0069D9",object.getString("out_schemecategory_Id")));
                        }
                        dynamicviewcontainer.removeAllViews();
                        bindViews();
                    }else{
                        titleItems.clear();
                        dynamicviewcontainer.removeAllViews();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindViews() {
        dynamicviewcontainer.removeAllViews();
        List<List<VideosTitleItems>> smallerLists = splittedbyList(titleItems,1);
        for (int k=0;k<smallerLists.size();k++){
            List<VideosTitleItems> sublist= smallerLists.get(k);
            LinearLayout linearLayout=new LinearLayout(SchemeTitleActivity.this);
            linearLayout.removeAllViews();
            linearLayout.setLayoutParams(llLP);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            for (int j = 0; j < sublist.size(); j++) {
                VideosTitleItems titleItems = sublist.get(j);
                LinearLayout.LayoutParams   llLP = new LinearLayout.LayoutParams(
                        //android:layout_width='match_parent' an in xml
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        //android:layout_height='wrap_content'
                        Constants.dpToPx(SchemeTitleActivity.this,50));
                //LinearLayout innerlayout=new LinearLayout(this);
                // innerlayout.setOrientation(LinearLayout.HORIZONTAL);
                LayoutInflater inflater = (LayoutInflater)this.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.vidotitle_itemlayouts, linearLayout,false);
                AppCompatTextView subtitle= child.findViewById(R.id.subTitle);
                LinearLayout titleContainer= child.findViewById(R.id.titleContainer);
                AppCompatImageView img= child.findViewById(R.id.correctimg);
                img.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_correct));
                subtitle.setText(sublist.get(j).getTitle());
                //innerlayout.addView(child);
                int finalJ = j;
                subtitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        img.performClick();
                    }
                });
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i=new Intent(SchemeTitleActivity.this,SchemeViewActivity.class);
                        i.putExtra("out_schemecategory_Id",sublist.get(finalJ).getId());
                        startActivity(i);
                    }
                });
                linearLayout.addView(child);
            }
            dynamicviewcontainer.addView(linearLayout);
        }
    }
    static <VideosTitleItems> List<List<VideosTitleItems>> splittedbyList(List<VideosTitleItems> list, final int L) {
        List<List<VideosTitleItems>> parts = new ArrayList<List<VideosTitleItems>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<VideosTitleItems>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
    private void getOfflineSearchResults (String key) {
        dynamicviewcontainer.removeAllViews();
        titleItems.clear();
        SQLiteDatabase db = getApplicationContext()
                .openOrCreateDatabase("kiosk",Context.MODE_PRIVATE,null);
        Cursor cr = db.rawQuery("select * from kiosk_trn_tscheme where scheme_category = ?",
                new String[] {catId});
        //Log.e("res",catId);
        Log.e("res",key);
        if(cr.getCount() == 0) {
            Toast.makeText(SchemeTitleActivity.this,"No Results Found",Toast.LENGTH_LONG).show();
        }
        while(cr.moveToNext()) {
            if (cr.getCount() > 0) {
                Log.e("type", key);
                Log.e("type", cr.getString(6));
                if (key.equalsIgnoreCase(cr.getString(6))) {
                    titleItems.add(new VideosTitleItems(cr.getString(4), "#0069D9", cr.getString(0)));
                }
                bindViews();
            } else {
                dynamicviewcontainer.removeAllViews();
                // Toast.makeText(FaqSubTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer, false);
                dynamicviewcontainer.addView(child);
            }
        }
    }
    private void getSearchResults(String key) {
        String url= Constants.Url+"/api/Kiosk_Scheme/Schemelist?org=flexi&locn=TA&user=kiosk&lang=en_US"+"&In_schemecat_Id="+catId+"&keyword="+key;
        dynamicviewcontainer.removeAllViews();
      //  String url= Constants.Url+"/api/Kiosk_Scheme/SchemeCatList?org=keyword&locn=TA&lang=en_US&user="+key;
        VolleyUtils.getVolleyResult(SchemeTitleActivity.this, Request.Method.GET, url, true, null, true, new VolleyListener() {
            @Override
            public void onError(String message) {
                Log.e("onError", message);
            }
            @Override
            public void onResponse(JSONObject response) {
                Log.e("getSearchResults",response.toString());
                JSONArray array  = null;
                titleItems.clear();
                try {
                    array=response.getJSONArray("schemelist");
                    if(array.length()>0){
                        for (int k=0;k<array.length();k++){
                            JSONObject object=array.getJSONObject(k);
                            titleItems.add(new VideosTitleItems(object.getString("out_scheme_name"),"#0069D9",object.getString("out_schemecategory_Id")));
                        }
                        bindViews();
                    }else{
                        dynamicviewcontainer.removeAllViews();
                        Toast.makeText(SchemeTitleActivity.this, "No Records found!", Toast.LENGTH_SHORT).show();
                        LayoutInflater inflater = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);
                        View child = inflater.inflate(R.layout.norecords_items, dynamicviewcontainer,false);
                        dynamicviewcontainer.addView(child);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


}