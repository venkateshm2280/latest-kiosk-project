package com.formar.kioskapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DBHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "kiosk";
    private static final int DATABASE_VERSION = 19;
    private static final String TABLE_USER = "core_mst_tuser";
    private static final String TABLE_MASTER = "core_mst_tmaster";
    private static final String TABLE_FAQ = "kisok_trn_tfaq";
    private static final String TABLE_LAND = "core_mst_tfarmerlanddet";
    private static final String TABLE_HEADER = "kioskheader";
    private static final String TABLE_CONTACT = "kiosk_mst_tsetupcontact";
    private static final String TABLE_ATTACHMENT = "kiosk_tmp_attachments";
    private static final String TABLE_SCHEMES = "kiosk_trn_tscheme";
    private static final String TABLE_SOIL = "kiosk_trn_tsoil";
    private static final String TABLE_SOILPARAM = "kiosk_trn_tsoilparameter";
    private static final String TABLE_VIDEO = "kiosk_trn_tvideo";
    private static final String TABLE_MHEADER = "core_mst_tmastertranslate";
    private static final String TABLE_LOC = "core_mst_tlocalizationtranslate";
    private static final String TABLE_ADV = "kiosk_trn_tadvertisement";
    private static final String TABLE_FARMER = "core_mst_tfarmer";
    private static final String TABLE_LIST_VILLAGE = "core_mst_village_list";
    private static final String TABLE_LIST_WEATHER  = "core_mst_weather_list";
    private static final String TABLE_LIST_FORECAST_WEATHER  = "core_mst_forecast_weather_list";
    private static final String TABLE_VILLAGE_DETAILS  = "core_mst_village_details";
    private static final String TABLE_CROP_ADV  = "core_mst_crop_adv";
    private static final String TABLE_SYNC_MASTER  = "kiosk_mst_syncmaster";

    // Table Structure
    // User Details
    private static final String USER_ROWID  ="user_rowid";
    private static final String ORGN_CODE = "orgn_code";
    private static final String USER_CODE = "user_code";
    private static final String USER_NAME = "user_name";
    private static final String STATUS_CODE = "status_code";
    private static final String ROLE_CODE = "role_code";
    private static final String USER_PWD = "user_pwd";
    private static final String EMAIL_ID = "email_id";
    private static final String CONTACT_NO = "contact_no";
    private static final String PHOTO_USER = "photo_user";
    private static final String VALID_TILL = "valid_till";
    private static final String SECQUESTION_CODE = "secquestion_code";
    private static final String SECQUESTION_ANSWER = "secquestion_answer";
    private static final String CREATED_DATETIME = "created_datetime";
    private static final String PASSWORD = "password";
    private static final String PASSWORD_RESET = "password_reset";

    //Master Details
    private static final String MASTER_ROWID = "master_rowid";
    private static final String PARENT_CODE = "parent_code";
    private static final String PARENT_DESC = "parent_desc";
    private static final String MASTER_CODE = "master_code";
    private static final String MASTER_DESC = "master_desc";
    private static final String DEPEND_CODE = "depend_code";
    private static final String DEPEND_DESC = "depend_desc";
    private static final String LOCALLANG_FLAG = "locallang_flag";
    private static final String MASTER_ROW_SLNO = "master_row_slno";
    private static final String STATUS_CODES = "status_code";

    //FAQ Table
    private static final String FAQ_GID = "faq_gid";
    private static final String FAQ_CATEGORY = "faq_category";
    private static final String FAQ_DATE = "faq_date";
    private static final String FAQ_QUESTION = "faq_question";
    private static final String FAQ_ANSWER = "faq_answer";
    private static final String FAQ_KEYWORDS = "faq_keywords";
    private static final String FAQ_QUES_LOCALLANG = "faq_ques_locallang";
    private static final String FAQ_ANSWER_LOCALLANG = "faq_answer_locallang";
    private static final String FAQ_KEYWORDS_LOCALLANG = "faq_keywords_locallang";
    private static final String FAQ_ANS_UPLOAD = "faq_ans_upload";
    private static final String FAQ_URL = "faq_url";
    private static final String FAQ_VIDEO = "faq_video";
    private static final String FAQ_VIDEOPATH = "faq_videopath";

    //Land Details
    private static final String LAND_GID = "land_gid";
    private static final String FARMER_GID = "farmer_gid";
    private static final String LAND_TYPE = "land_type";
    private static final String LAND_OWNERSHIP = "land_ownership";
    private static final String LAND_SOILTYPE = "land_soiltype";
    private static final String LAND_IRRSOU = "land_irrsou";
    private static final String LAND_NOOFACRES = "land_noofacres";
    private static final String LAND_LONGTITUDE = "land_longtitude";
    private static final String LAND_LATITUDE = "land_latitude";
    private static final String LAND_INSERTEDBY = "land_insertedby";
    private static final String LAND_INSERTEDDATE = "land_inserteddate";
    private static final String LAND_MODIFYBY = "land_modifyby";
    private static final String LAND_MODIFYDATE = "land_modifydate";
    private static final String LAND_ISREMOVED = "land_isremoved";

    //Header Details
    private static final String KIOSK_GID = "kiosk_gid";
    private static final String KIOSK_CODE = "kiosk_code";
    private static final String KIOSK_NAME = "kiosk_name";
    private static final String KIOSK_BILLINGUAL = "kiosk_billingual";
    private static final String KIOSK_VILLAGE = "kiosk_village";
    private static final String KIOSK_TALUK = "kiosk_taluk";
    private static final String KIOSK_DISTRICT = "kiosk_district";
    private static final String KIOSK_STATE = "kiosk_state";
    private static final String KIOSK_PINCODE = "kiosk_pincode";
    private static final String KIOSK_FPO = "kiosk_fpo";
    private static final String KISOK_ADDRESS = "kisok_address";
    private static final String KIOSK_NOTES = "kiosk_notes";
    private static final String KIOSK_STATUS = "kiosk_status";

    //Contact Details
    private static final String CONTACT_GID = "contact_gid";
    private static final String CONTACT_KIOSK_GID = "contact_kiosk_gid";
    private static final String CONTACT_NAME = "contact_name";
    private static final String CONTACT_DESIGNATION = "contact_designation";
    private static final String CONTACT_EMAIL = "contact_email";
    private static final String CONTACT_MOBILE = "contact_mobile";
    private static final String CONTACT_LANDLINE = "contact_landline";

    //Attachment Details
    private static final String ATTACH_GID = "attach_gid";
    private static final String ATTACH_REF_GID = "attach_ref_gid";
    private static final String ATTACH_DOCGROUP_GID = "attach_docgroup_gid";
    private static final String ATTACH_DOCUNAME = "attach_docuname";
    private static final String ATTACH_DESCRIPITION = "attach_descripition";
    private static final String ATTACH_FILENAME = "attach_filename";

    //Schemes Details
    private static final String SCHEME_GID = "scheme_gid";
    private static final String SCHEME_CATEGORY = "scheme_category";
    private static final String SCHEME_DATE = "scheme_date";
    private static final String SCHEME_STATE = "scheme_state";
    private static final String SCHEME_SCHNAME = "scheme_schname";
    private static final String SCHEME_DESCRIPTION = "scheme_description";
    private static final String SCHEME_KEYWORD = "scheme_keyword";
    private static final String SCHEMA_DES_LOCALLANG = "schema_des_locallang";
    private static final String SCHEME_KEYWORD_LOCALLANG = "scheme_keyword_locallang";
    private static final String SCHEME_URL = "scheme_url";
    private static final String SCHEME_UPLOAD = "scheme_upload";
    private static final String SCHEME_STATUS = "scheme_status";
    private static final String SCHEME_NOTES = "scheme_notes";
    private static final String SCHEME_SCHEME_LANG = "scheme_schname_locallang";

    //Soil Details
    private static final String SOIL_GID = "soil_gid";
    private static final String SOIL_FARMER_GID = "soil_farmer_gid";
    private static final String FARMER_COLLDATE = "farmer_colldate";
    private static final String FARMER_TRANID = "farmer_tranid";
    private static final String FARMER_SAMPLESTS = "farmer_samplests";
    private static final String FARMER_SAMPLEID = "farmer_sampleid";
    private static final String FARMER_SAMPLEDRAWN = "farmer_sampledrawn";
    private static final String FARMER_CUSTOMERREF = "farmer_customerref";
    private static final String FARMER_LABREPORTNO = "farmer_labreportno";
    private static final String FARMER_LABID = "farmer_labid";
    private static final String FARMER_SAMPLERECEIVED = "farmer_samplereceived";
    private static final String FARMER_ANALYSTARTED = "farmer_analystarted";
    private static final String FARMER_ANALYCOMPLETED = "farmer_analycompleted";
    private static final String FARMER_TESTMETHOD = "farmer_testmethod";
    private static final String FARMER_SOIL_REJREASON = "farmer_soil_rejreason";
    private static final String FARMER_SOIL_CROPRECOM = "farmer_soil_CropRecom";
    private static final String FAMER_SOIL_NOTES = "famer_soil_notes";
    private static final String FARMER_SOIL_STATUS = "farmer_soil_status";

    //Soil Parameter Details
    private static final String SOILPARAM_GID = "soilparam_gid";
    private static final String SOILPARAM_SOIL_GID = "soilparam_soil_gid";
    private static final String SOIL_PARAMETER = "soil_parameter";
    private static final String SOIL_UOM = "soil_uom";
    private static final String SOIL_RESULTS = "soil_results";

    //Video Details
    private static final String VIDEO_GID = "video_gid";
    private static final String VIDEO_CATEGORY = "video_category";
    private static final String VIDEO_TITLE = "video_title";
    private static final String VIDEO_FILENAME = "video_filename";
    private static final String VIDEO_FILEPATH = "video_filepath";
    private static final String VIDEO_SOURCE = "video_source";
    private static final String VIDEO_KEYWORDS = "video_keywords";
    private static final String VIDEO_ATTRIBUTE = "video_attribute";
    private static final String VIDEO_NOTES = "video_notes";

    //Master Header Details
    private static final String MASTERTRANSLATE_ROWID = "mastertranslate_rowid";
    private static final String MASTERTRANSLATE_MASTER_CODE = "mastertranslate_master_code";
    private static final String MASTERTRANSLATE_PARENT_CODE = "mastertranslate_parent_code";
    private static final String MASTERTRANSLATE_LANG_CODE = "mastertranslate_lang_code";
    private static final String MASTER_NAME = "master_name";

    //Localise Details
    private static final String LOCALIZATIONTRANSLATE_ROWID = "localizationtranslate_rowid";
    private static final String LOCALIZATIONTRANSLATE_ACTIVITY_CODE =
            "localizationtranslate_activity_code";
    private static final String CONTROL_CODE = "control_code";
    private static final String DATA_FIELD = "data_field";
    private static final String LANG_CODE = "lang_code";
    private static final String CONTROL_VALUE = "control_value";
    private static final String LOC_INSERTEDBY = "loc_insertedby";
    private static final String LOC_INSERTEDDATE = "loc_inserteddate";
    private static final String LOC_MODIFYBY = "loc_modifyby";
    private static final String LOC_MODIFYDATE = "loc_modifydate";
    private static final String LOC_ISREMOVED    = "loc_isremoved";

    //Farmer Details
    private static final String FARMER_ROWID = "farmer_rowid";
    private static final String FARMER_CODE = "farmer_code";
    private static final String VERSION_NO = "version_no";
    private static final String FARMER_NAME = "farmer_name";
    private static final String FARMER_DOB = "farmer_dob";
    private static final String FARMER_ADDR1 = "farmer_addr1";
    private static final String FARMER_ADDR2 = "farmer_addr2";
    private static final String FARMER_LL_NAME = "farmer_ll_name";
    private static final String SUR_LL_NAME = "sur_ll_name";
    private static final String FHW_LL_NAME = "fhw_ll_name";
    private static final String FARMER_LL_ADDR1 = "farmer_ll_addr1";
    private static final String FARMER_LL_ADDR2 = "farmer_ll_addr2";
    private static final String FARMER_COUNTRY = "farmer_country";
    private static final String FARMER_STATE = "farmer_state";
    private static final String FARMER_DIST  = "farmer_dist";
    private static final String FARMER_TALUK = "farmer_taluk";
    private static final String FARMER_PANCHAYAT = "farmer_panchayat";
    private static final String FARMER_VILLAGE = "farmer_village";
    private static final String FARMER_PINCODE = "farmer_pincode";
    private static final String MARITAL_STATUS = "marital_status";
    private static final String GENDER_FLAG = "gender_flag";
    private static final String PHOTO_FARMER = "photo_farmer";
    private static final String REG_NOTE = "reg_note";
    private static final String SUR_NAME = "sur_name";
    private static final String FHW_NAME = "fhw_name";
    private static final String FSTATUS_CODE = "status_code";

    //Village List
    private static final String ID = "id";
    private static final String BLOCK_ID = "block_id";
    private static final String BLOCK_NAME = "block_name";
    private static final String DISTRICT_ID = "district_id";
    private static final String DISTRICT_NAME = "district_name";
    private static final String STATE_ID = "state_id";
    private static final String STATE_NAME = "state_name";
    private static final String VILLAGE = "village";
    private static final String CREATEDBY = "createdby";
    private static final String MODIFIEDBY = "modifiedby";
    private static final String CREATEDON = "createdon";
    private static final String MODIFIEDON = "modifiedon";

    //Weather Details
    private static final String DATE = "date";
    private static final String VILLAGE_ID = "village_id";
    private static final String TMAX = "tmax";
    private static final String TMIN = "tmin";
    private static final String RH1 = "rh1";
    private static final String RH2 = "rh2";
    private static final String RH_AVG = "rh_avg";
    private static final String WS1 = "ws1";
    private static final String WS2 = "ws2";
    private static final String WS_AVG = "ws_avg";
    private static final String RF = "rf";

    //Forecast Weather Details
    private static final String FDATE = "date";
    private static final String FVILLAGE_ID = "village_id";
    private static final String FTMAX = "tmax";
    private static final String FTMIN = "tmin";
    private static final String FRH1 = "rh1";
    private static final String FRH2 = "rh2";
    private static final String FRH_AVG = "rh_avg";
    private static final String FWS1 = "ws1";
    private static final String FWS2 = "ws2";
    private static final String FWS_AVG = "ws_avg";
    private static final String FRF = "rf";

    //Village Details
    private static final String OUT_MASTER_ROWID = "out_master_rowid";
    private static final String OUT_ROW_SLNO = "out_row_slno";
    private static final String OUT_PARENT_CODE = "out_parent_code";
    private static final String OUT_PARENT_DESCRIPTION = "out_parent_description";
    private static final String OUT_MASTER_CODE = "out_master_code";
    private static final String OUT_MASTER_DESCRIPTION = "out_master_description";
    private static final String OUT_MASTER_LL_DESCRIPTION = "out_master_ll_description";
    private static final String OUT_DEPEND_CODE = "out_depend_code";
    private static final String OUT_DEPEND_DESC = "out_depend_desc";
    private static final String OUT_LOCALLANG_FLAG = "out_locallang_flag";
    private static final String OUT_STATUS_CODE = "out_status_code";
    private static final String OUT_STATUS_DESC = "out_status_desc";
    private static final String OUT_ROW_TIMESTAMP = "out_row_timestamp";
    private static final String OUT_MODE_FLAG = "out_mode_flag";

    //Crop Adv List
    private static final String CID = "id";
    private static final String CROP_REG_ID = "crop_reg_id";
    private static final String USERID = "userid";
    private static final String MOBILENO = "mobileno";
    private static final String CROPID = "cropid";
    private static final String CROP_NAME = "crop_name";
    private static final String CVILLAGE_ID  = "village_id";
    private static final String MESSAGE_TA = "message_ta";
    private static final String MESSAGE_EN = "message_en";
    private static final String STATUS = "status";
    private static final String NAME = "name";
    private static final String VILLAGE_NAME = "village_name";
    private static final String CBLOCK_ID = "block_id";
    private static final String BLOCK = "block";
    private static final String CDISTRICT_ID = "district_id";
    private static final String DISTRICT = "district";
    private static final String CSTATE_ID = "state_id";
    private static final String STATE = "state";
    private static final String CCREATEDON = "createdon";

    //Sync Master
    private static final String SYNCMASTER_ROWID = "syncmaster_rowid";
    //private static final String MASTER_DATA = "master_data";
    private static final String SYNVILLAGE_ID = "village_id";
    private static final String SYNDISTRICT_ID = "district_id";
    private static final String KIOSK_VILLAGE_GID = "kiosk_village_gid";
    private static final String KIOSK_MASTER_TYPE = "kiosk_Master_type";
    private static final String SYNVILLAGE_NAME  = "village_name";
    private static final String SYNDISTRICT_NAME = "district_name";
    private static final String TALUK_ID = "taluk_id";
    private static final String TALUK_NAME = "taluk_name";
    private static final String KIOSK_TALUK_GID = "kiosk_taluk_gid";
    private static final String KIOSK_PANCHAYAT_GID = "kiosk_panchayat_gid";
    private static final String SOURCE = "source";
    private static final String KIOSK_DISTRICT_GID = "kiosk_district_gid";

    //Creating User Table
    String user = "CREATE TABLE " + TABLE_USER + "("
            + USER_ROWID + " TEXT,"
            + ORGN_CODE + " TEXT,"
            + USER_CODE + " TEXT,"
            + USER_NAME + " TEXT,"
            + STATUS_CODE + " TEXT,"
            + ROLE_CODE + " TEXT,"
            + USER_PWD + " TEXT,"
            + EMAIL_ID + " TEXT,"
            + CONTACT_NO + " TEXT,"
            + PHOTO_USER + " TEXT,"
            + VALID_TILL + " TEXT,"
            + SECQUESTION_CODE + " TEXT,"
            + SECQUESTION_ANSWER + " TEXT,"
            + CREATED_DATETIME + " TEXT,"
            + PASSWORD + " TEXT,"
            + PASSWORD_RESET + " TEXT"
           // + USER_VILLAGE + " TEXT,"
           // + USER_GP + " TEXT,"
           // + USER_DISTRICT + " TEXT"
            + ")";

    //Creating Master Table
    String masterheader = "CREATE TABLE " + TABLE_MASTER + "("
            + MASTER_ROWID + " TEXT,"
            + PARENT_CODE + " TEXT,"
            + MASTER_CODE + " TEXT,"
            + DEPEND_CODE + " TEXT,"
            + LOCALLANG_FLAG + " TEXT,"
            + MASTER_ROW_SLNO + " TEXT,"
            + STATUS_CODES + " TEXT"
            + ")";

    //Creating FAQ Table
    String faq = "CREATE TABLE " + TABLE_FAQ + "("
            + FAQ_GID + " TEXT,"
            + FAQ_CATEGORY + " TEXT,"
            + FAQ_DATE + " TEXT,"
            + FAQ_QUESTION + " TEXT,"
            + FAQ_ANSWER + " TEXT,"
            + FAQ_KEYWORDS + " TEXT,"
            + FAQ_QUES_LOCALLANG + " TEXT,"
            + FAQ_ANSWER_LOCALLANG + " TEXT,"
            + FAQ_KEYWORDS_LOCALLANG + " TEXT,"
            + FAQ_ANS_UPLOAD + " TEXT,"
            + FAQ_URL + " TEXT,"
            + FAQ_VIDEO + " TEXT,"
            + FAQ_VIDEOPATH + " TEXT"
            + ")";

    //Creating Land Table
    String land = "CREATE TABLE " + TABLE_LAND + "("
            + LAND_GID + " TEXT,"
            + FARMER_GID + " TEXT,"
            + LAND_TYPE + " TEXT,"
            + LAND_OWNERSHIP + " TEXT,"
            + LAND_SOILTYPE + " TEXT,"
            + LAND_IRRSOU + " TEXT,"
            + LAND_NOOFACRES + " TEXT,"
            + LAND_LONGTITUDE + " TEXT,"
            + LAND_LATITUDE + " TEXT,"
            + LAND_INSERTEDBY + " TEXT,"
            + LAND_INSERTEDDATE + " TEXT,"
            + LAND_MODIFYBY + " TEXT,"
            + LAND_MODIFYDATE + " TEXT,"
            + LAND_ISREMOVED + " TEXT"
            + ")";

    //Creating Header Table
    String kioskheader = "CREATE TABLE " + TABLE_HEADER + "("
            + KIOSK_GID + " TEXT,"
            + KIOSK_CODE + " TEXT,"
            + KIOSK_NAME + " TEXT,"
            + KIOSK_BILLINGUAL + " TEXT,"
            + KIOSK_VILLAGE + " TEXT,"
            + KIOSK_TALUK + " TEXT,"
            + KIOSK_DISTRICT + " TEXT,"
            + KIOSK_STATE + " TEXT,"
            + KIOSK_PINCODE + " TEXT,"
            + KIOSK_FPO + " TEXT,"
            + KISOK_ADDRESS + " TEXT,"
            + KIOSK_NOTES + " TEXT,"
            + KIOSK_STATUS + " TEXT"
            + ")";

    //Creating Contact Table
    String contactdetails = "CREATE TABLE " + TABLE_CONTACT + "("
            + CONTACT_GID + " TEXT,"
            + CONTACT_KIOSK_GID + " TEXT,"
            + CONTACT_NAME + " TEXT,"
            + CONTACT_DESIGNATION + " TEXT,"
            + CONTACT_EMAIL + " TEXT,"
            + CONTACT_MOBILE + " TEXT,"
            + CONTACT_LANDLINE + " TEXT"
            + ")";

    //Creating Attachment Table
    String attachment = "CREATE TABLE " + TABLE_ATTACHMENT + "("
            + ATTACH_GID + " TEXT,"
            + ATTACH_REF_GID + " TEXT,"
            + ATTACH_DOCGROUP_GID + " TEXT,"
            + ATTACH_DOCUNAME + " TEXT,"
            + ATTACH_DESCRIPITION + " TEXT,"
            + ATTACH_FILENAME + " TEXT"
            + ")";

    //Creating Schemes Table
    String schemes = "CREATE TABLE " + TABLE_SCHEMES + "("
            + SCHEME_GID + " TEXT,"
            + SCHEME_CATEGORY + " TEXT,"
            + SCHEME_DATE + " TEXT,"
            + SCHEME_STATE + " TEXT,"
            + SCHEME_SCHNAME + " TEXT,"
            + SCHEME_DESCRIPTION + " TEXT,"
            + SCHEME_KEYWORD + " TEXT,"
            + SCHEMA_DES_LOCALLANG + " TEXT,"
            + SCHEME_KEYWORD_LOCALLANG + " TEXT,"
            + SCHEME_URL + " TEXT,"
            + SCHEME_UPLOAD + " TEXT,"
            + SCHEME_STATUS + " TEXT,"
            + SCHEME_NOTES + " TEXT,"
            + SCHEME_SCHEME_LANG + " TEXT"
            + ")";

    //Creating Soil Table
    String soil = "CREATE TABLE " + TABLE_SOIL + "("
            + SOIL_GID + " TEXT,"
            + SOIL_FARMER_GID + " TEXT,"
            + FARMER_COLLDATE + " TEXT,"
            + FARMER_TRANID + " TEXT,"
            + FARMER_SAMPLESTS + " TEXT,"
            + FARMER_SAMPLEID + " TEXT,"
            + FARMER_SAMPLEDRAWN + " TEXT,"
            + FARMER_CUSTOMERREF  + " TEXT,"
            + FARMER_LABREPORTNO + " TEXT,"
            + FARMER_LABID + " TEXT,"
            + FARMER_SAMPLERECEIVED + " TEXT,"
            + FARMER_ANALYSTARTED + " TEXT,"
            + FARMER_ANALYCOMPLETED + " TEXT,"
            + FARMER_TESTMETHOD + " TEXT,"
            + FARMER_SOIL_REJREASON + " TEXT,"
            + FARMER_SOIL_CROPRECOM + " TEXT,"
            + FAMER_SOIL_NOTES + " TEXT,"
            + FARMER_SOIL_STATUS + " TEXT"
            + ")";

    //Creating Soil Parameter Table
    String soilparam = "CREATE TABLE " + TABLE_SOILPARAM + "("
            + SOILPARAM_GID + " TEXT,"
            + SOILPARAM_SOIL_GID + " TEXT,"
            + SOIL_PARAMETER + " TEXT,"
            + SOIL_UOM + " TEXT,"
            + SOIL_RESULTS + " TEXT"
            + ")";

    //Creating Video Table
    String video = "CREATE TABLE " + TABLE_VIDEO + "("
            + VIDEO_GID + " TEXT,"
            + VIDEO_CATEGORY + " TEXT,"
            + VIDEO_TITLE + " TEXT,"
            + VIDEO_FILENAME + " TEXT,"
            + VIDEO_FILEPATH + " TEXT,"
            + VIDEO_SOURCE + " TEXT,"
            + VIDEO_KEYWORDS + " TEXT,"
            + VIDEO_ATTRIBUTE + " TEXT,"
            + VIDEO_NOTES + " TEXT"
            + ")";

    //Creating Master Header Table
    String mheader = "CREATE TABLE " + TABLE_MHEADER + "("
            + MASTERTRANSLATE_ROWID + " TEXT,"
            + MASTERTRANSLATE_MASTER_CODE + " TEXT,"
            + MASTERTRANSLATE_PARENT_CODE + " TEXT,"
            + MASTERTRANSLATE_LANG_CODE + " TEXT,"
            + MASTER_NAME + " TEXT"
            + ")";

    //Creating Localise Table
    String localize = "CREATE TABLE " + TABLE_LOC + "("
            + LOCALIZATIONTRANSLATE_ROWID + " TEXT,"
            + LOCALIZATIONTRANSLATE_ACTIVITY_CODE + " TEXT,"
            + CONTROL_CODE + " TEXT,"
            + DATA_FIELD + " TEXT,"
            + LANG_CODE + " TEXT,"
            + CONTROL_VALUE + " TEXT,"
            + LOC_INSERTEDBY + " TEXT,"
            + LOC_INSERTEDDATE  + " TEXT,"
            + LOC_MODIFYBY + " TEXT,"
            + LOC_MODIFYDATE + " TEXT,"
            + LOC_ISREMOVED + " TEXT"
            + ")";

    //Creating Adv Table
    String adv = "CREATE TABLE " + TABLE_ADV + "('')";


    //Creating Farmer Table
    String farmer = "CREATE TABLE " + TABLE_FARMER + "("
            + FARMER_ROWID + " TEXT,"
            + FARMER_CODE + " TEXT,"
            + VERSION_NO + " TEXT,"
            + FARMER_NAME + " TEXT,"
            + FARMER_DOB + " TEXT,"
            + FARMER_ADDR1 + " TEXT,"
            + FARMER_ADDR2 + " TEXT,"
            + FARMER_LL_NAME  + " TEXT,"
            + SUR_LL_NAME + " TEXT,"
            + FHW_LL_NAME + " TEXT,"
            + FARMER_LL_ADDR1 + " TEXT,"
            + FARMER_LL_ADDR2 + " TEXT,"
            + FARMER_COUNTRY + " TEXT,"
            + FARMER_STATE + " TEXT,"
            + FARMER_DIST + " TEXT,"
            + FARMER_TALUK + " TEXT,"
            + FARMER_PANCHAYAT + " TEXT,"
            + FARMER_VILLAGE + " TEXT,"
            + FARMER_PINCODE + " TEXT,"
            + MARITAL_STATUS + " TEXT,"
            + GENDER_FLAG + " TEXT,"
            + PHOTO_FARMER + " TEXT,"
            + REG_NOTE + " TEXT,"
            + SUR_NAME + " TEXT,"
            + FHW_NAME + " TEXT,"
            + STATUS_CODE + " TEXT"
            + ")";

    //Creating Village List Table
    String villagelist = "CREATE TABLE " + TABLE_LIST_VILLAGE + "("
            + ID + " TEXT,"
            + BLOCK_ID + " TEXT,"
            + BLOCK_NAME + " TEXT,"
            + DISTRICT_ID + " TEXT,"
            + DISTRICT_NAME + " TEXT,"
            + STATE_ID + " TEXT,"
            + STATE_NAME + " TEXT,"
            + VILLAGE + " TEXT,"
            + CREATEDBY + " TEXT,"
            + MODIFIEDBY + " TEXT,"
            + CREATEDON + " TEXT,"
            + MODIFIEDON + " TEXT"
            + ")";

    //Creating Weather List Table
    String weatherlist = "CREATE TABLE " + TABLE_LIST_WEATHER + "("
            + DATE + " TEXT,"
            + VILLAGE_ID + " TEXT,"
            + TMAX + " TEXT,"
            + TMIN + " TEXT,"
            + RH1 + " TEXT,"
            + RH2 + " TEXT,"
            + RH_AVG + " TEXT,"
            + WS1 + " TEXT,"
            + WS2 + " TEXT,"
            + WS_AVG + " TEXT,"
            + RF + " TEXT"
            + ")";

    //Creating Forecast Weather List Table
    String forecast_weatherlist = "CREATE TABLE " + TABLE_LIST_FORECAST_WEATHER + "("
            + FDATE + " TEXT,"
            + FVILLAGE_ID + " TEXT,"
            + FTMAX + " TEXT,"
            + FTMIN + " TEXT,"
            + FRH1 + " TEXT,"
            + FRH2 + " TEXT,"
            + FRH_AVG + " TEXT,"
            + FWS1 + " TEXT,"
            + FWS2 + " TEXT,"
            + FWS_AVG + " TEXT,"
            + FRF + " TEXT"
            + ")";

    //Creating Village Details
    String villagedetails = "CREATE TABLE " + TABLE_VILLAGE_DETAILS + "("
            + OUT_MASTER_ROWID + " TEXT,"
            + OUT_ROW_SLNO + " TEXT,"
            + OUT_PARENT_CODE + " TEXT,"
            + OUT_PARENT_DESCRIPTION + " TEXT,"
            + OUT_MASTER_CODE + " TEXT,"
            + OUT_MASTER_DESCRIPTION + " TEXT,"
            + OUT_MASTER_LL_DESCRIPTION + " TEXT,"
            + OUT_DEPEND_CODE  + " TEXT,"
            + OUT_DEPEND_DESC + " TEXT,"
            + OUT_LOCALLANG_FLAG + " TEXT,"
            + OUT_STATUS_CODE + " TEXT,"
            + OUT_STATUS_DESC + " TEXT,"
            + OUT_ROW_TIMESTAMP + " TEXT,"
            + OUT_MODE_FLAG + " TEXT"
            + ")";

    //Creating Crop Adv Table
    String cropadv = "CREATE TABLE " + TABLE_CROP_ADV + "("
            + CID + " TEXT,"
            + CROP_REG_ID + " TEXT,"
            + USERID + " TEXT,"
            + MOBILENO + " TEXT,"
            + CROPID + " TEXT,"
            + CROP_NAME + " TEXT,"
            + CVILLAGE_ID + " TEXT,"
            + MESSAGE_TA + " TEXT,"
            + MESSAGE_EN + " TEXT,"
            + STATUS + " TEXT,"
            + NAME + " TEXT,"
            + VILLAGE_NAME + " TEXT,"
            + CBLOCK_ID + " TEXT,"
            + BLOCK + " TEXT,"
            + CDISTRICT_ID + " TEXT,"
            + DISTRICT + " TEXT,"
             + CSTATE_ID + " TEXT,"
             + STATE + " TEXT,"
             + CCREATEDON + " TEXT"
            + ")";

    // Inserting Sync Details
    String syncmaster = "CREATE TABLE " + TABLE_SYNC_MASTER + "("
            + SYNCMASTER_ROWID + " TEXT,"
           // + MASTER_DATA + " TEXT,"
            + SYNVILLAGE_ID + " TEXT,"
            + SYNDISTRICT_ID + " TEXT,"
            + KIOSK_VILLAGE_GID + " TEXT,"
            + KIOSK_MASTER_TYPE + " TEXT,"
            + SYNVILLAGE_NAME + " TEXT,"
            + SYNDISTRICT_NAME + " TEXT,"
            + TALUK_ID + " TEXT,"
            + TALUK_NAME + " TEXT,"
            + KIOSK_TALUK_GID + " TEXT,"
            + KIOSK_PANCHAYAT_GID + " TEXT,"
            + SOURCE + " TEXT,"
            + KIOSK_DISTRICT_GID + " TEXT"
            + ")";

    //Inserting data manually
    String ins = "INSERT INTO " + TABLE_USER + "("
            + USER_NAME + ","
            + USER_ROWID + ","
            + USER_PWD + ","
            + USER_CODE  + ") VALUES ('flexi','1','123','1','KADIPUR','KADIPUR','KADIPUR'," +
            "'AZAMGARH')";


    public DBHandler(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(user);
        db.execSQL(masterheader);
        db.execSQL(faq);
        db.execSQL(land);
        db.execSQL(kioskheader);
        db.execSQL(contactdetails);
        db.execSQL(attachment);
        db.execSQL(adv);
        db.execSQL(schemes);
        db.execSQL(soil);
        db.execSQL(soilparam);
        db.execSQL(video);
        db.execSQL(mheader);
        db.execSQL(localize);
        db.execSQL(farmer);
        db.execSQL(villagelist);
        db.execSQL(weatherlist);
        db.execSQL(forecast_weatherlist);
        db.execSQL(villagedetails);
        db.execSQL(cropadv);
        db.execSQL(syncmaster);
        db.execSQL(
                "create table ilist " +
                        "(id integer primary key, irrigation_gid text,farmer_gid text,farmer_colldate text,farmer_tranid text,farmer_samplests text,farmer_sampleid text,farmer_sampledrawn text,farmer_customerref text,farmer_labreportno text,farmer_labid text,farmer_samplereceived text,farmer_analystarted text,farmer_analycompleted text,farmer_testmethod text,farmer_irrigation_rejreason text,farmer_irrigation_CropRecom text,famer_irrigation_notes text,farmer_irrigation_status text,farmer_crop_confirm text,farmer_irrigation_confirm text)"


        );

        db.execSQL(
                "create table ipar " +
                        "(id integer primary key, irrigationparam_gid text,irrigation_gid text,irrigation_parameter text,irrigation_uom text,irrigation_results text)"


        );
        



    }



    //Inserting Datas

    public Boolean onUserInsert(String user_rowid, String orgn_code, String user_code,
                                String user_name, String status_code, String role_code,
                                String user_pwd, String email_id, String contact_no, String photo_user,
                                String valid_till, String secquestion_code, String secquestion_answer, String created_datetime,
                                String password, String password_reset) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues =new ContentValues();
        contentValues.put(USER_ROWID, user_rowid);
        contentValues.put(ORGN_CODE, orgn_code);
        contentValues.put(USER_CODE, user_code);
        contentValues.put(USER_NAME, user_name);
        contentValues.put(STATUS_CODE, status_code);
        contentValues.put(ROLE_CODE, role_code);
        contentValues.put(USER_PWD, user_pwd);
        contentValues.put(EMAIL_ID, email_id);
        contentValues.put(CONTACT_NO, contact_no);
        contentValues.put(PHOTO_USER, photo_user);
        contentValues.put(VALID_TILL, valid_till);
        contentValues.put(SECQUESTION_CODE, secquestion_code);
        contentValues.put(SECQUESTION_ANSWER, secquestion_answer);
        contentValues.put(CREATED_DATETIME, created_datetime);
        contentValues.put(PASSWORD, password);
        contentValues.put(PASSWORD_RESET, password_reset);
        long result = db.insert("core_mst_tuser",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onMasterInsert (String master_rowid, String parent_code,
                                   String master_code,  String depend_code,
                                    String locallang_flag, String master_row_slno,
                                   String status_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MASTER_ROWID, master_rowid);
        contentValues.put(PARENT_CODE, parent_code);
        contentValues.put(MASTER_CODE, master_code);
        contentValues.put(DEPEND_CODE, depend_code);
        contentValues.put(LOCALLANG_FLAG, locallang_flag);
        contentValues.put(MASTER_ROW_SLNO, master_row_slno);
        contentValues.put(STATUS_CODE, status_code);
        long result = db.insert("core_mst_tmaster", null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    public Boolean onFaqInsert(String faq_gid, String faq_category, String faq_date,
                               String faq_question, String faq_answer, String faq_keywords,
                               String faq_ques_locallang, String faq_answer_locallang,
                               String faq_keywords_locallang, String faq_ans_upload,
                               String faq_url, String faq_video, String faq_videopath) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FAQ_GID,faq_gid);
        contentValues.put(FAQ_CATEGORY,faq_category);
        contentValues.put(FAQ_DATE,faq_date);
        contentValues.put(FAQ_QUESTION,faq_question);
        contentValues.put(FAQ_ANSWER,faq_answer);
        contentValues.put(FAQ_KEYWORDS,faq_keywords);
        contentValues.put(FAQ_QUES_LOCALLANG,faq_ques_locallang);
        contentValues.put(FAQ_ANSWER_LOCALLANG,faq_answer_locallang);
        contentValues.put(FAQ_KEYWORDS_LOCALLANG,faq_keywords_locallang);
        contentValues.put(FAQ_ANS_UPLOAD,faq_ans_upload);
        contentValues.put(FAQ_URL,faq_url);
        contentValues.put(FAQ_VIDEO,faq_video);
        contentValues.put(FAQ_VIDEOPATH,faq_videopath);


        long result = db.insert("kisok_trn_tfaq",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onLandInsert (String land_gid, String farmer_gid, String land_type,
                                 String land_ownership, String land_soiltype, String land_irrsou,
                                 String land_noofacres, String land_longtitude,
                                 String land_latitude, String land_insertedby,
                                 String land_inserteddate, String land_modifyby, String land_modifydate,
                                 String land_isremoved) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LAND_GID,land_gid);
        contentValues.put(FARMER_GID,farmer_gid);
        contentValues.put(LAND_TYPE,land_type);
        contentValues.put(LAND_OWNERSHIP,land_ownership);
        contentValues.put(LAND_SOILTYPE,land_soiltype);
        contentValues.put(LAND_IRRSOU,land_irrsou);
        contentValues.put(LAND_NOOFACRES,land_noofacres);
        contentValues.put(LAND_LONGTITUDE,land_longtitude);
        contentValues.put(LAND_LATITUDE,land_latitude);
        contentValues.put(LAND_INSERTEDBY,land_insertedby);
        contentValues.put(LAND_INSERTEDDATE,land_inserteddate);
        contentValues.put(LAND_MODIFYBY,land_modifyby);
        contentValues.put(LAND_MODIFYDATE,land_modifydate);
        contentValues.put(LAND_ISREMOVED,land_isremoved);

        long result = db.insert("core_mst_tfarmerlanddet",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onHeaderInsert (String kiosk_gid, String kiosk_code, String kiosk_name,
                                   String kiosk_billingual, String kiosk_village, String kiosk_taluk,
                                   String kiosk_district, String kiosk_state,
                                   String kiosk_pincode, String kiosk_fpo,
                                   String kisok_address, String kiosk_notes, String kiosk_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KIOSK_GID,kiosk_gid);
        contentValues.put(KIOSK_CODE,kiosk_code);
        contentValues.put(KIOSK_NAME,kiosk_name);
        contentValues.put(KIOSK_BILLINGUAL,kiosk_billingual);
        contentValues.put(KIOSK_VILLAGE,kiosk_village);
        contentValues.put(KIOSK_TALUK,kiosk_taluk);
        contentValues.put(KIOSK_DISTRICT,kiosk_district);
        contentValues.put(KIOSK_STATE,kiosk_state);
        contentValues.put(KIOSK_PINCODE,kiosk_pincode);
        contentValues.put(KIOSK_FPO,kiosk_fpo);
        contentValues.put(KISOK_ADDRESS,kisok_address);
        contentValues.put(KIOSK_NOTES,kiosk_notes);
        contentValues.put(KIOSK_STATUS,kiosk_status);

        long result = db.insert("kioskheader",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean iListinsert (String n1, String n2, String n3,
                                String n4, String n5, String n6,
                                String n7, String n8,
                                String n9, String n10,
                                String n11, String n12, String n13,String n14,String n15,String n16,String n17,String n18,String n19,String n20) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("irrigation_gid",n1);
        contentValues.put("farmer_gid",n2);
        contentValues.put("farmer_colldate",n3);
        contentValues.put("farmer_tranid",n4);
        contentValues.put("farmer_samplests",n5);
        contentValues.put("farmer_sampleid",n6);
        contentValues.put("farmer_sampledrawn",n7);
        contentValues.put("farmer_customerref",n8);
        contentValues.put("farmer_labreportno",n9);
        contentValues.put("farmer_labid",n10);
        contentValues.put("farmer_samplereceived",n11);
        contentValues.put("farmer_analystarted",n12);
        contentValues.put("farmer_analycompleted",n13);
        contentValues.put("farmer_testmethod",n14);
        contentValues.put("farmer_irrigation_rejreason",n15);
        contentValues.put("farmer_irrigation_CropRecom",n16);
        contentValues.put("famer_irrigation_notes",n17);
        contentValues.put("farmer_irrigation_status",n18);
        contentValues.put("farmer_crop_confirm",n19);
        contentValues.put("farmer_irrigation_confirm",n20);


        long result = db.insert("ilist",null,contentValues);
        Log.e("ILIST","INSERTED");
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean iParInsert (String n1, String n2, String n3,
                               String n4, String n5) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("irrigationparam_gid",n1);
        contentValues.put("irrigation_gid",n2);
        contentValues.put("irrigation_parameter",n3);
        contentValues.put("irrigation_uom",n4);
        contentValues.put("irrigation_results",n5);



        long result = db.insert("ipar",null,contentValues);
        Log.e("IPAR","INSERTED");
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onContactInsert (String contact_gid, String contact_kiosk_gid, String contact_name,
                                    String contact_designation, String contact_email, String contact_mobile,
                                    String contact_landline) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACT_GID,contact_gid);
        contentValues.put(CONTACT_KIOSK_GID,contact_kiosk_gid);
        contentValues.put(CONTACT_NAME,contact_name);
        contentValues.put(CONTACT_DESIGNATION,contact_designation);
        contentValues.put(CONTACT_EMAIL,contact_email);
        contentValues.put(CONTACT_MOBILE,contact_mobile);
        contentValues.put(CONTACT_LANDLINE,contact_landline);

        long result = db.insert("kiosk_mst_tsetupcontact",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onAttachmentInsert (String attach_gid, String attach_ref_gid, String attach_docgroup_gid,
                                       String attach_docuname, String attach_descripition,
                                       String attach_filename) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ATTACH_GID,attach_gid);
        contentValues.put(ATTACH_REF_GID,attach_ref_gid);
        contentValues.put(ATTACH_DOCGROUP_GID,attach_docgroup_gid);
        contentValues.put(ATTACH_DOCUNAME,attach_docuname);
        contentValues.put(ATTACH_DESCRIPITION,attach_descripition);
        contentValues.put(ATTACH_FILENAME,attach_filename);

        long result = db.insert("kiosk_tmp_attachments",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onSchemeInsert (String scheme_gid, String scheme_category, String scheme_date,
                                   String scheme_state, String scheme_schname, String scheme_description,
                                   String scheme_keyword, String schema_des_locallang,
                                   String scheme_keyword_locallang, String scheme_url,
                                   String scheme_upload, String scheme_status, String scheme_notes,
                                   String scheme_schname_locallang) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SCHEME_GID,scheme_gid);
        contentValues.put(SCHEME_CATEGORY,scheme_category);
        contentValues.put(SCHEME_DATE,scheme_date);
        contentValues.put(SCHEME_STATE,scheme_state);
        contentValues.put(SCHEME_SCHNAME,scheme_schname);
        contentValues.put(SCHEME_DESCRIPTION,scheme_description);
        contentValues.put(SCHEME_KEYWORD,scheme_keyword);
        contentValues.put(SCHEMA_DES_LOCALLANG,schema_des_locallang);
        contentValues.put(SCHEME_KEYWORD_LOCALLANG,scheme_keyword_locallang);
        contentValues.put(SCHEME_URL,scheme_url);
        contentValues.put(SCHEME_UPLOAD,scheme_upload);
        contentValues.put(SCHEME_STATUS,scheme_status);
        contentValues.put(SCHEME_NOTES,scheme_notes);
        contentValues.put(SCHEME_SCHEME_LANG,scheme_schname_locallang);

        long result = db.insert("kiosk_trn_tscheme",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onSoilInsert (String soil_gid, String soil_farmer_gid, String farmer_colldate,
                                 String farmer_tranid, String farmer_samplests, String farmer_sampleid,
                                 String farmer_sampledrawn, String farmer_customerref,
                                 String farmer_labreportno, String farmer_labid,
                                 String farmer_samplereceived, String farmer_analystarted, String farmer_analycompleted,
                                 String farmer_testmethod, String farmer_soil_rejreason, String farmer_soil_CropRecom,
                                 String famer_soil_notes, String farmer_soil_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SOIL_GID,soil_gid);
        contentValues.put(SOIL_FARMER_GID,soil_farmer_gid);
        contentValues.put(FARMER_COLLDATE,farmer_colldate);
        contentValues.put(FARMER_TRANID,farmer_tranid);
        contentValues.put(FARMER_SAMPLESTS,farmer_samplests);
        contentValues.put(FARMER_SAMPLEID,farmer_sampleid);
        contentValues.put(FARMER_SAMPLEDRAWN,farmer_sampledrawn);
        contentValues.put(FARMER_CUSTOMERREF,farmer_customerref);
        contentValues.put(FARMER_LABREPORTNO,farmer_labreportno);
        contentValues.put(FARMER_LABID,farmer_labid);
        contentValues.put(FARMER_SAMPLERECEIVED,farmer_samplereceived);
        contentValues.put(FARMER_ANALYSTARTED,farmer_analystarted);
        contentValues.put(FARMER_ANALYCOMPLETED,farmer_analycompleted);
        contentValues.put(FARMER_TESTMETHOD,farmer_testmethod);
        contentValues.put(FARMER_SOIL_REJREASON,farmer_soil_rejreason);
        contentValues.put(FARMER_SOIL_CROPRECOM,farmer_soil_CropRecom);
        contentValues.put(FAMER_SOIL_NOTES,famer_soil_notes);
        contentValues.put(FARMER_SOIL_STATUS,farmer_soil_status);


        long result = db.insert("kiosk_trn_tsoil",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onSoilParamInsert (String soilparam_gid, String soilparam_soil_gid, String soil_parameter,
                                      String soil_uom, String soil_results) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SOILPARAM_GID,soilparam_gid);
        contentValues.put(SOILPARAM_SOIL_GID,soilparam_soil_gid);
        contentValues.put(SOIL_PARAMETER,soil_parameter);
        contentValues.put(SOIL_UOM,soil_uom);
        contentValues.put(SOIL_RESULTS,soil_results);

        long result = db.insert("kiosk_trn_tsoilparameter",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onVideoInsert (String video_gid, String video_category, String video_title,
                                  String video_filename, String video_filepath, String video_source,
                                  String video_keywords, String video_attribute,
                                  String video_notes) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(VIDEO_GID,video_gid);
        contentValues.put(VIDEO_CATEGORY,video_category);
        contentValues.put(VIDEO_TITLE,video_title);
        contentValues.put(VIDEO_FILENAME,video_filename);
        contentValues.put(VIDEO_FILEPATH,video_filepath);
        contentValues.put(VIDEO_SOURCE,video_source);
        contentValues.put(VIDEO_KEYWORDS,video_keywords);
        contentValues.put(VIDEO_ATTRIBUTE,video_attribute);
        contentValues.put(VIDEO_NOTES,video_notes);

        long result = db.insert("kiosk_trn_tvideo",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onMasTransInsert (String mastertranslate_rowid, String mastertranslate_master_code, String mastertranslate_parent_code,
                                     String mastertranslate_lang_code, String master_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MASTERTRANSLATE_ROWID,mastertranslate_rowid);
        contentValues.put(MASTERTRANSLATE_MASTER_CODE,mastertranslate_master_code);
        contentValues.put(MASTERTRANSLATE_PARENT_CODE,mastertranslate_parent_code);
        contentValues.put(MASTERTRANSLATE_LANG_CODE,mastertranslate_lang_code);
        contentValues.put(MASTER_NAME,master_name);

        long result = db.insert("core_mst_tmastertranslate",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onLocInsert (String localizationtranslate_rowid, String localizationtranslate_activity_code, String control_code,
                                String data_field, String lang_code, String control_value,
                                String loc_insertedby, String loc_inserteddate,
                                String loc_modifyby, String loc_modifydate, String loc_isremoved ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOCALIZATIONTRANSLATE_ROWID,localizationtranslate_rowid);
        contentValues.put(LOCALIZATIONTRANSLATE_ACTIVITY_CODE,localizationtranslate_activity_code);
        contentValues.put(CONTROL_CODE,control_code);
        contentValues.put(DATA_FIELD,data_field);
        contentValues.put(LANG_CODE,lang_code);
        contentValues.put(CONTROL_VALUE,control_value);
        contentValues.put(LOC_INSERTEDBY,loc_insertedby);
        contentValues.put(LOC_INSERTEDDATE,loc_inserteddate);
        contentValues.put(LOC_MODIFYBY,loc_modifyby);
        contentValues.put(LOC_MODIFYDATE,loc_modifydate);
        contentValues.put(LOC_ISREMOVED,loc_isremoved);

        long result = db.insert("core_mst_tlocalizationtranslate",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onFarmerInsert (String farmer_rowid, String farmer_code, String version_no,
                                 String farmer_name, String farmer_dob, String farmer_addr1,
                                 String farmer_addr2, String farmer_ll_name,
                                 String sur_ll_name, String fhw_ll_name,
                                 String farmer_ll_addr1, String farmer_ll_addr2, String farmer_country,
                                 String farmer_state, String farmer_dist, String farmer_taluk,
                                 String farmer_panchayat, String farmer_village,String farmer_pincode,
                                   String marital_status,String gender_flag, String photo_farmer,
                                   String reg_note, String sur_name,String fhw_name,String status_code ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FARMER_ROWID,farmer_rowid);
        contentValues.put(FARMER_CODE,farmer_code);
        contentValues.put(VERSION_NO,version_no);
        contentValues.put(FARMER_NAME,farmer_name);
        contentValues.put(FARMER_DOB,farmer_dob);
        contentValues.put(FARMER_ADDR1,farmer_addr1);
        contentValues.put(FARMER_ADDR2,farmer_addr2);
        contentValues.put(FARMER_LL_NAME,farmer_ll_name);
        contentValues.put(SUR_LL_NAME,sur_ll_name);
        contentValues.put(FHW_LL_NAME,fhw_ll_name);
        contentValues.put(FARMER_LL_ADDR1,farmer_ll_addr1);
        contentValues.put(FARMER_LL_ADDR2,farmer_ll_addr2);
        contentValues.put(FARMER_COUNTRY,farmer_country);
        contentValues.put(FARMER_STATE,farmer_state);
        contentValues.put(FARMER_DIST,farmer_dist);
        contentValues.put(FARMER_TALUK,farmer_taluk);
        contentValues.put(FARMER_PANCHAYAT,farmer_panchayat);
        contentValues.put(FARMER_VILLAGE,farmer_village);
        contentValues.put(FARMER_PINCODE,farmer_pincode);
        contentValues.put(MARITAL_STATUS,marital_status);
        contentValues.put(GENDER_FLAG,gender_flag);
        contentValues.put(PHOTO_FARMER,photo_farmer);
        contentValues.put(REG_NOTE,reg_note);
        contentValues.put(SUR_NAME,sur_name);
        contentValues.put(FHW_NAME,fhw_name);
        contentValues.put(FSTATUS_CODE,status_code);

        long result = db.insert("core_mst_tfarmer",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onVillageListInsert (String id, String block_id, String block_name,
                                 String district_id, String district_name, String state_id,
                                 String state_name, String village,
                                 String createdby, String modifiedby,
                                 String createdon, String modifiedon) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID,id);
        contentValues.put(BLOCK_ID,block_id);
        contentValues.put(BLOCK_NAME,block_name);
        contentValues.put(DISTRICT_ID,district_id);
        contentValues.put(DISTRICT_NAME,district_name);
        contentValues.put(STATE_ID,state_id);
        contentValues.put(STATE_NAME,state_name);
        contentValues.put(VILLAGE,village);
        contentValues.put(CREATEDBY,createdby);
        contentValues.put(MODIFIEDBY,modifiedby);
        contentValues.put(CREATEDON,createdon);
        contentValues.put(MODIFIEDON,modifiedon);

        long result = db.insert("core_mst_village_list",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onWeatherListInsert (String date, String village_id, String tmax,
                                        String tmin, String rh1, String rh2,
                                        String rh_avg, String ws1,
                                        String ws2, String ws_avg,
                                        String rf) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DATE,date);
        contentValues.put(VILLAGE_ID,village_id);
        contentValues.put(TMAX,tmax);
        contentValues.put(TMIN,tmin);
        contentValues.put(RH1,rh1);
        contentValues.put(RH2,rh2);
        contentValues.put(RH_AVG,rh_avg);
        contentValues.put(WS1,ws1);
        contentValues.put(WS2,ws2);
        contentValues.put(WS_AVG,ws_avg);
        contentValues.put(RF,rf);

        long result = db.insert("core_mst_weather_list",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean onForecastWeatherListInsert (String date, String village_id, String tmax,
                                        String tmin, String rh1, String rh2,
                                        String rh_avg, String ws1,
                                        String ws2, String ws_avg,
                                        String rf) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FDATE,date);
        contentValues.put(FVILLAGE_ID,village_id);
        contentValues.put(FTMAX,tmax);
        contentValues.put(FTMIN,tmin);
        contentValues.put(FRH1,rh1);
        contentValues.put(FRH2,rh2);
        contentValues.put(FRH_AVG,rh_avg);
        contentValues.put(FWS1,ws1);
        contentValues.put(FWS2,ws2);
        contentValues.put(FWS_AVG,ws_avg);
        contentValues.put(FRF,rf);

        long result = db.insert("core_mst_forecast_weather_list",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    //Village Details
    public Boolean onVillageDetailsInsert (String out_master_rowid, String out_row_slno, String out_parent_code,
                                 String out_parent_description, String out_master_code, String out_master_description,
                                 String out_master_ll_description, String out_depend_code,
                                 String out_depend_desc, String out_locallang_flag,
                                 String out_status_code, String out_status_desc, String out_row_timestamp,
                                 String out_mode_flag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OUT_MASTER_ROWID,out_master_rowid);
        contentValues.put(OUT_ROW_SLNO,out_row_slno);
        contentValues.put(OUT_PARENT_CODE,out_parent_code);
        contentValues.put(OUT_PARENT_DESCRIPTION,out_parent_description);
        contentValues.put(OUT_MASTER_CODE,out_master_code);
        contentValues.put(OUT_MASTER_DESCRIPTION,out_master_description);
        contentValues.put(OUT_MASTER_LL_DESCRIPTION,out_master_ll_description);
        contentValues.put(OUT_DEPEND_CODE,out_depend_code);
        contentValues.put(OUT_DEPEND_DESC,out_depend_desc);
        contentValues.put(OUT_LOCALLANG_FLAG,out_locallang_flag);
        contentValues.put(OUT_STATUS_CODE,out_status_code);
        contentValues.put(OUT_STATUS_DESC,out_status_desc);
        contentValues.put(OUT_ROW_TIMESTAMP,out_row_timestamp);
        contentValues.put(OUT_MODE_FLAG,out_mode_flag);

        long result = db.insert("core_mst_village_details",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    //Inserting Crop Adv Datas

    public Boolean onCropAdvInsert(String id, String crop_reg_id, String userid,
                                String mobileno, String cropid, String crop_name,
                                String village_id, String message_ta, String message_en, String status,
                                String name, String village_name, String block_id, String block,
                                String district_id, String district, String state_id, String state,
                                   String createdon) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues =new ContentValues();
        contentValues.put(CID, id);
        contentValues.put(CROP_REG_ID, crop_reg_id);
        contentValues.put(USERID, userid);
        contentValues.put(MOBILENO, mobileno);
        contentValues.put(CROPID, cropid);
        contentValues.put(CROP_NAME, crop_name);
        contentValues.put(CVILLAGE_ID, village_id);
        contentValues.put(MESSAGE_TA, message_ta);
        contentValues.put(MESSAGE_EN, message_en);
        contentValues.put(STATUS, status);
        contentValues.put(NAME, name);
        contentValues.put(VILLAGE_NAME, village_name);
        contentValues.put(CBLOCK_ID, block_id);
        contentValues.put(BLOCK, block);
        contentValues.put(CDISTRICT_ID, district_id);
        contentValues.put(DISTRICT, district);
        contentValues.put(CSTATE_ID, state_id);
        contentValues.put(STATE, state);
        contentValues.put(CCREATEDON, createdon);
        long result = db.insert("core_mst_crop_adv",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }


    // Inserting Synmaster Details
    public Boolean onSyncMasterInsert(String syncmaster_rowid, String village_id,
                                      String district_id, String kiosk_village_gid,
                                      String kiosk_Master_type, String village_name, String district_name,
                                      String taluk_id, String taluk_name, String kiosk_taluk_gid,
                                      String kiosk_panchayat_gid, String source, String kiosk_district_gid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues =new ContentValues();
        contentValues.put(SYNCMASTER_ROWID, syncmaster_rowid);
        //contentValues.put(MASTER_DATA, master_data);
        contentValues.put(SYNVILLAGE_ID, village_id);
        contentValues.put(SYNDISTRICT_ID, district_id);
        contentValues.put(KIOSK_VILLAGE_GID, kiosk_village_gid);
        contentValues.put(KIOSK_MASTER_TYPE, kiosk_Master_type);
        contentValues.put(SYNVILLAGE_NAME, village_name);
        contentValues.put(SYNDISTRICT_NAME, district_name);
        contentValues.put(TALUK_ID, taluk_id);
        contentValues.put(TALUK_NAME, taluk_name);
        contentValues.put(KIOSK_TALUK_GID, kiosk_taluk_gid);
        contentValues.put(KIOSK_PANCHAYAT_GID, kiosk_panchayat_gid);
        contentValues.put(SOURCE, source);
        contentValues.put(KIOSK_DISTRICT_GID, kiosk_district_gid);
        long result = db.insert("kiosk_mst_syncmaster",null,contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    // Checking Username and Password

    public Boolean checkUsername (String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select *from core_mst_tuser where user_name = ?",
                new String[] {username});
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean checkPassword (String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from core_mst_tuser where password = ?",
                new String[] {password});
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean checkUsernamePassword (String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from core_mst_tuser where " +
                "user_name = ? and password = ?", new String[] {username,password});
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAQ);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LAND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HEADER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTACHMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEMES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOILPARAM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MHEADER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADV);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FARMER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST_VILLAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST_WEATHER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST_FORECAST_WEATHER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VILLAGE_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CROP_ADV);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_MASTER);
        onCreate(db);
    }

}
